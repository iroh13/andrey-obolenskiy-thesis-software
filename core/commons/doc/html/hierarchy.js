var hierarchy =
[
    [ "Common.Time.Clock", "interface_common_1_1_time_1_1_clock.html", [
      [ "Common.Time.VariableIncrementClock", "class_common_1_1_time_1_1_variable_increment_clock.html", null ]
    ] ],
    [ "Comparer", null, [
      [ "Common.Functional.FunctionalComparer< T >", "class_common_1_1_functional_1_1_functional_comparer.html", null ]
    ] ],
    [ "Common.Randomization.Distributions.Distribution", "interface_common_1_1_randomization_1_1_distributions_1_1_distribution.html", [
      [ "Common.Randomization.Distributions.PseudoRandomDistribution", "class_common_1_1_randomization_1_1_distributions_1_1_pseudo_random_distribution.html", [
        [ "Common.Randomization.Distributions.UniformDistribution", "class_common_1_1_randomization_1_1_distributions_1_1_uniform_distribution.html", null ]
      ] ]
    ] ],
    [ "IEnumerable", null, [
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ],
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ]
    ] ],
    [ "Common.Collections.Interfaces.ImmutableCollection", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", [
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ]
    ] ],
    [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", [
      [ "Common.Collections.Interfaces.ImmutableList< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_list.html", [
        [ "Common.Collections.Views.ListView< T >", "class_common_1_1_collections_1_1_views_1_1_list_view.html", null ]
      ] ],
      [ "Common.Collections.Views.CollectionView< T >", "class_common_1_1_collections_1_1_views_1_1_collection_view.html", [
        [ "Common.Collections.Views.ListView< T >", "class_common_1_1_collections_1_1_views_1_1_list_view.html", null ]
      ] ]
    ] ],
    [ "Common.DesignPatterns.ObjectBuilder< T >", "class_common_1_1_design_patterns_1_1_object_builder.html", null ],
    [ "Common.Functional.Options.Option< T >", "interface_common_1_1_functional_1_1_options_1_1_option.html", [
      [ "Common.Functional.Options.None< T >", "struct_common_1_1_functional_1_1_options_1_1_none.html", null ],
      [ "Common.Functional.Options.Some< T >", "struct_common_1_1_functional_1_1_options_1_1_some.html", null ]
    ] ],
    [ "Common.Collections.Pair< TFirst, TSecond >", "struct_common_1_1_collections_1_1_pair.html", null ],
    [ "Common.Time.Timer", "class_common_1_1_time_1_1_timer.html", null ]
];