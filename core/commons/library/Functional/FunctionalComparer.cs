﻿using Common.Validation;
using System;
using System.Collections.Generic;

namespace Common.Functional
{
    /// <summary>
    /// This class represents a <see cref="Comparer{T}"/> that can be built
    /// from lambda expressions.
    /// </summary>
    /// <typeparam name="T">The type of the objects being compared.</typeparam>
    public sealed class FunctionalComparer<T>
        : Comparer<T>
    {
        /// <summary>
        /// Creates a new comparer.
        /// </summary>
        /// <param name="function">
        /// The function to use for comparison.
        /// </param>
        /// <remarks>
        /// <paramref name="function"/> must not be null.
        /// </remarks>
        public FunctionalComparer(Func<T, T, int> function)
        {
            Require.IsNotNull(function);

            Function = function;
        }

        /// <summary>
        /// Gets the function used for comparison.
        /// </summary>
        public Func<T, T, int> Function { get; private set; }

        /// <summary>
        /// Performs a comparison of two objects of the same type and returns a
        /// value indicating whether one object is less than, equal to, or 
        /// greater than the other.
        /// </summary>
        /// <param name="a">The first object.</param>
        /// <param name="b">The second object.</param>
        /// <returns>
        /// A value less than zero if <paramref name="a"/> is less than 
        /// <paramref name="b"/>, 
        /// a value of zero if <paramref name="a"/> equals 
        /// <paramref name="b"/>, and
        /// a value greater than zero if <paramref name="a"/> is greater than
        /// <paramref name="b"/>.
        /// </returns>
        public override int Compare(T a, T b)
        {
            return Function.Invoke(a, b);
        }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as FunctionalComparer<T>;

            if (ReferenceEquals(other, null)) { return false; }
            if (ReferenceEquals(other, this)) { return true; }

            return other.Function.Equals(Function);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return Function.GetHashCode();
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(FunctionalComparer<T>)}(" +
                   $"{nameof(Function)} = {Function})";
        }
    }
    /// <summary>
    /// Convenience methods for the creation of a functional comparer.
    /// </summary>
    public static class FunctionalComparer
    {
        /// <summary>
        /// Creates a new comparer.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the objects being compared.
        /// </typeparam>
        /// <param name="function">
        /// The function to use for comparison.
        /// </param>
        /// <remarks>
        /// <paramref name="function"/> must not be null.
        /// </remarks>
        /// <returns>A new comparer.</returns>
        public static FunctionalComparer<T> Create<T>
        (
            Func<T, T, int> function
        )
        {
            Require.IsNotNull(function);

            return new FunctionalComparer<T>(function);
        }
    }
}
