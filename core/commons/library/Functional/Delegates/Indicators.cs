﻿namespace Common.Functional.Delegates
{
    /// <summary>
    /// Indicates if the specified item belongs to the indicated set.
    /// </summary>
    /// <typeparam name="T">The type of the items to act on.</typeparam>
    /// <param name="item">The item to test.</param>
    /// <returns>
    /// True if the specified item belongs to the indicated set, otherwise 
    /// false.
    /// </returns>
    public delegate bool Indicator<T>(T item);
    /// <summary>
    /// Convenience methods for the creation of common indicators.
    /// </summary>
    /// <typeparam name="T">The type of the items to act on.</typeparam>
    public static class Indicators<T>
    {
        /// <summary>
        /// The indicator accepting any item.
        /// </summary>
        public static readonly Indicator<T> Any = item => true;
        /// <summary>
        /// The indicator accepting no items.
        /// </summary>
        public static readonly Indicator<T> None = item => false;
    }
}
