﻿namespace Common.Functional.Options
{
    /// <summary>
    /// This structure represents the "some" state of an option.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the value held by the option.
    /// </typeparam>
    public struct Some<T>
        : Option<T>
    {
        /// <summary>
        /// Creates a new option holding some value.
        /// </summary>
        /// <param name="value">The value to hold.</param>
        public Some(T value)
        {
            Value = value;
        }

        /// <summary>
        /// Gets the value held by this option.
        /// </summary>
        public T Value { get; private set; }

        /// <summary>
        /// Determines whether this option contains the specified value.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <returns>
        /// True if this option contains the specified value, otherwise false.
        /// </returns>
        public bool Contains(T value)
        {
            return Equals(Value, value);
        }

        /// <summary>
        /// Casts this option to the specified type.
        /// </summary>
        /// <typeparam name="TResult">The type to cast to.</typeparam>
        /// <returns>
        /// A new option containing the cast value of this one.
        /// </returns>
        public Option<TResult> Cast<TResult>()
        {
            return new Some<TResult>((TResult)(object)Value);
        }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Some<T>)) { return false; }

            var other = (Some<T>)obj;

            return Equals(other.Value, Value);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Some<T>)}(" +
                   $"{nameof(Value)} = {Value})";
        }
    }
}
