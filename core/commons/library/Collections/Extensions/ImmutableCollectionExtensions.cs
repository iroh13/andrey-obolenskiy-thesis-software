﻿using Common.Collections.Interfaces;
using Common.Validation;

namespace Common.Collections.Extensions
{
    /// <summary>
    /// A collection of extension methods for immutable collections.
    /// </summary>
    public static class ImmutableCollectionExtensions
    {
        /// <summary>
        /// Determines whether the specified collection is empty.
        /// </summary>
        /// <typeparam name="T">The type of the collection's items.</typeparam>
        /// <param name="collection">The collection to test.</param>
        /// <returns>
        /// True if the specified collection is empty, otherwise false.
        /// </returns>
        /// <remarks>
        /// <paramref name="collection"/> must not be null.
        /// </remarks>
        public static bool IsEmpty<T>(this ImmutableCollection<T> collection)
        {
            Require.IsNotNull(collection);

            return collection.Count == 0;
        }
    }
}
