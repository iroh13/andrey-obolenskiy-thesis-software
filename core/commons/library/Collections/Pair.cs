﻿using Common.Hashing;

namespace Common.Collections
{
    /// <summary>
    /// This structure represents a generic pair.
    /// </summary>
    /// <typeparam name="TFirst">The type of the first item.</typeparam>
    /// <typeparam name="TSecond">The type of the second item.</typeparam>
    public struct Pair<TFirst, TSecond>
    {
        /// <summary>
        /// Creates a new pair.
        /// </summary>
        /// <param name="first">The first item.</param>
        /// <param name="second">The second item.</param>
        public Pair(TFirst first, TSecond second)
        {
            First = first;
            Second = second;
        }

        /// <summary>
        /// Gets the first item.
        /// </summary>
        public TFirst First { get; private set; }
        /// <summary>
        /// Gets the second item.
        /// </summary>
        public TSecond Second { get; private set; }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Pair<TFirst, TSecond>)) { return false; }

            var other = (Pair<TFirst, TSecond>)obj;

            return other.First.Equals(First) &&
                   other.Second.Equals(Second);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hash = HashCombiner.Initialize();
            hash = HashCombiner.Hash(hash, First);
            hash = HashCombiner.Hash(hash, Second);

            return hash;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Pair<TFirst, TSecond>)}(" +
                   $"{nameof(First)} = {First}, " +
                   $"{nameof(Second)} = {Second})";
        }
    }
    /// <summary>
    /// Convenience methods for the creation of pairs.
    /// </summary>
    public static class Pair
    {
        /// <summary>
        /// Creates a new pair.
        /// </summary>
        /// <typeparam name="TFirst">The type of the first item.</typeparam>
        /// <typeparam name="TSecond">The type of the second item.</typeparam>
        /// <param name="first">The first item.</param>
        /// <param name="second">The second item.</param>
        /// <returns>A new pair.</returns>
        public static Pair<TFirst, TSecond> Create<TFirst, TSecond>
        (
            TFirst first,
            TSecond second
        )
        {
            return new Pair<TFirst, TSecond>(first, second);
        }
    }
}
