﻿using System.Collections;
using System.Collections.Generic;

namespace Common.Collections.Interfaces
{
    /// <summary>
    /// This interface represents an immutable collection. 
    /// </summary>
    public interface ImmutableCollection
        : IEnumerable
    {
        /// <summary>
        /// Gets the item count.
        /// </summary>
        int Count { get; }
    }
    /// <summary>
    /// This interface represents a generic immutable collection. 
    /// </summary>
    /// <typeparam name="T">The type of the collection's items.</typeparam>
    public interface ImmutableCollection<T>
        : ImmutableCollection, IEnumerable<T>
    {
        /// <summary>
        /// Determines whether this collection contains the specified item.
        /// </summary>
        /// <param name="item">The item to search for.</param>
        /// <returns>
        /// True if this collection contains the specified item, otherwise 
        /// false.
        /// </returns>
        bool Contains(T item);
    }
}
