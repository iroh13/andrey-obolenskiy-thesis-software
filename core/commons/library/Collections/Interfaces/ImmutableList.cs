﻿namespace Common.Collections.Interfaces
{
    /// <summary>
    /// This class represents an immutable view of a list.
    /// </summary>
    /// <typeparam name="T">The type of the collection's items.</typeparam>
    public interface ImmutableList<T>
        : ImmutableCollection<T>
    {
        /// <summary>
        /// Gets the item at the specified index.
        /// </summary>
        /// <param name="index">The desired item's index.</param>
        /// <returns>The item at the specified index.</returns>
        T this[int index] { get; }
        /// <summary>
        /// Determines the index of the specified item in this list.
        /// </summary>
        /// <param name="item">The item to index.</param>
        /// <returns>The item's index if found, otherwise -1.</returns>
        /// <remarks>
        /// If the list contains multiple occurrences of the item, this method
        /// returns the index of the first one.
        /// </remarks>
        int IndexOf(T item);
    }
}
