﻿using Common.Validation;
using System;

namespace Common.Randomization.Distributions
{
    /// <summary>
    /// This class is a base for pseudo-random distributions using the 
    /// <see cref="Random"/> class as a generator.
    /// </summary>
    public abstract class PseudoRandomDistribution
        : Distribution
    {
        /// <summary>
        /// Creates a new distribution powered by the specified random number
        /// generator.
        /// </summary>
        /// <param name="generator">
        /// The random number generator to sample from.
        /// </param>
        /// <remarks>
        /// <paramref name="generator"/> must not be null.
        /// </remarks>
        public PseudoRandomDistribution(Random generator)
        {
            Require.IsNotNull(generator);

            Generator = generator;
        }
        /// <summary>
        /// Creates a new random distribution.
        /// </summary>
        public PseudoRandomDistribution()
            : this(new Random()) {}

        /// <summary>
        /// Gets the random number generator this distribution samples from.
        /// </summary>
        public Random Generator { get; private set; }

        /// <summary>
        /// Samples the distribution.
        /// </summary>
        /// <returns>
        /// The sampled value.
        /// </returns>
        public abstract double Sample();
    }
}
