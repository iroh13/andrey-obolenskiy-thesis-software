﻿using Common.Functional.Options;
using Common.Functional.Options.Extensions;
using Common.Validation;
using System;

namespace Common.DesignPatterns
{
    /// <summary>
    /// This class represents the builder design-pattern for object creation.
    /// </summary>
    /// <typeparam name="T">The type of the built object.</typeparam>
    public abstract class ObjectBuilder<T>
    {
        /// <summary>
        /// Indicates whether the object is already built.
        /// </summary>
        public bool IsBuilt { get; private set; } = false;
        /// <summary>
        /// Gets the built object.
        /// </summary>
        public Option<T> Object { get; private set; } = Option.CreateNone<T>();

        /// <summary>
        /// Builds the object.
        /// </summary>
        /// <returns>The built object.</returns>
        /// <remarks>
        /// Calling this more than once per instance is invalid.
        /// </remarks>
        public T Build()
        {
            Require.IsFalse<InvalidOperationException>(IsBuilt);

            Object = Option.CreateSome(CreateObject());
            IsBuilt = true;

            return Object.GetValue();
        }

        /// <summary>
        /// Creates the object.
        /// </summary>
        /// <returns>The built object.</returns>
        protected abstract T CreateObject();
    }
}
