﻿using Moq;
using NUnit.Framework;
using System;

namespace Common.Functional.Options.Extensions.Test
{
    [TestFixture]
    public sealed class OptionExtensionsTest
    {
        private static readonly int VALUE = 1;

        private static readonly Option<int> NONE = Option.CreateNone<int>();
        private static readonly Option<int> SOME = Option.CreateSome(VALUE);

        [Test]
        public void Test_TypeVerification()
        {
            Assert.IsTrue(NONE.IsNone());
            Assert.IsFalse(NONE.IsSome());

            Assert.IsFalse(SOME.IsNone());
            Assert.IsTrue(SOME.IsSome());
        }

        [Test]
        public void Test_MatchingSome_WithInvalidArgs()
        {
            var option = new Mock<Option<int>>().Object;

            Assert.Throws<ArgumentNullException>
            (
                () => option.IfIsSome(null)
            );
        }
        [Test]
        public void Test_MatchingSome()
        {
            bool matched_none = 
                NONE.IfIsSome(value => { Assert.Fail(); });
            bool matched_some = 
                SOME.IfIsSome(value => { Assert.AreEqual(VALUE, value); });

            Assert.IsFalse(matched_none);
            Assert.IsTrue(matched_some);
        }
        
        [Test]
        public void Test_MatchingExpressions_WithInvalidArgs()
        {
            var option = new Mock<Option<int>>().Object;

            Assert.Throws<ArgumentNullException>
            (
                () => option.Match
                (
                    none: null,
                    some: (value) => true
                )
            );
            Assert.Throws<ArgumentNullException>
            (
                () => option.Match
                (
                    none: () => true,
                    some: null
                )
            );
        }
        [Test]
        public void Test_MatchingExpressions()
        {
            bool matched_none = NONE.Match
            (
                none: () => true,
                some: (value) => false
            );
            bool matched_some = SOME.Match
            (
                none: () => false,
                some: (value) => value == VALUE
            );

            Assert.IsTrue(matched_none);
            Assert.IsTrue(matched_some);
        }

        [Test]
        public void Test_MatchingAction_WithInvalidArgs()
        {
            var option = new Mock<Option<int>>().Object;

            Assert.Throws<ArgumentNullException>
            (
                () => option.Match
                (
                    none: null,
                    some: (value) => { }
                )
            );
            Assert.Throws<ArgumentNullException>
            (
                () => option.Match
                (
                    none: () => { },
                    some: null
                )
            );
        }
        [Test]
        public void Test_MatchingActions()
        {
            NONE.Match
            (
                none: () => { },
                some: (value) => { Assert.Fail(); }
            );
            SOME.Match
            (
                none: () => { Assert.Fail(); },
                some: (value) => { Assert.AreEqual(VALUE, value); }
            );
        }

        [Test]
        public void Test_ValueAccess()
        {
            Assert.Throws<InvalidOperationException>
            (
                () => NONE.GetValue()
            );
            Assert.AreEqual(VALUE, SOME.GetValue());
        }
    }
}
