﻿using NUnit.Framework;

namespace Common.Functional.Options.Test
{
    [TestFixture]
    public sealed class OptionTest
    {
        private static readonly int VALUE = 1;

        [Test]
        public void Test_StaticNoneConstructor()
        {
            var option = Option.CreateNone<int>();

            Assert.IsTrue(option is None<int>);
        }

        [Test]
        public void Test_StaticSomeConstructor()
        {
            var option = Option.CreateSome(VALUE);

            Assert.IsTrue(option is Some<int>);
            Assert.IsTrue(option.Contains(VALUE));
        }
    }
}
