﻿using NUnit.Framework;

namespace Common.Randomization.Test
{
    [TestFixture]
    public sealed class PseudoRandomTest
    {
        [Test]
        public void Test_StaticGenerator()
        {
            Assert.IsNotNull(PseudoRandom.Generator);
        }

        [Test]
        public void Test_Seeding()
        {
            int a0, b0, c0;
            int a1, b1, c1;
            int seed = 1;

            PseudoRandom.Seed(seed);
            a0 = PseudoRandom.Generator.Next();
            b0 = PseudoRandom.Generator.Next();
            c0 = PseudoRandom.Generator.Next();

            PseudoRandom.Seed(seed);
            a1 = PseudoRandom.Generator.Next();
            b1 = PseudoRandom.Generator.Next();
            c1 = PseudoRandom.Generator.Next();

            Assert.AreEqual(a0, a1);
            Assert.AreEqual(b0, b1);    
            Assert.AreEqual(c0, c1);
        }
    }
}
