﻿using NUnit.Framework;
using System;

namespace Common.Randomization.Distributions.Test
{
    [TestFixture]
    public sealed class UniformDistributionTest
    {
        private static readonly Random GENERATOR = new Random();
        private static readonly float LOWER_BOUND = 1;
        private static readonly float UPPER_BOUND = 2;

        [Test]
        public void Test_ConstructorWithGeneratorAndBounds_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new UniformDistribution
                (
                    null, 
                    LOWER_BOUND, 
                    UPPER_BOUND
                )
            );
            Assert.Throws<ArgumentException>
            (
                () => new UniformDistribution
                (
                    GENERATOR, 
                    UPPER_BOUND, 
                    LOWER_BOUND
                )
            );
            Assert.DoesNotThrow
            (
                () => new UniformDistribution
                (
                    GENERATOR, 
                    LOWER_BOUND, 
                    LOWER_BOUND
                )
            );
        }
        [Test]
        public void Test_ConstructorWithGeneratorAndBounds()
        {
            var distribution = new UniformDistribution
            (
                GENERATOR, 
                LOWER_BOUND, 
                UPPER_BOUND
            );

            Assert.AreSame(GENERATOR, distribution.Generator);
            Assert.AreEqual(LOWER_BOUND, distribution.LowerBound);
            Assert.AreEqual(UPPER_BOUND, distribution.UpperBound);
        }

        [Test]
        public void Test_ConstructorWithBounds_WithInvalidArgs()
        {
            Assert.Throws<ArgumentException>
            (
                () => new UniformDistribution(UPPER_BOUND, LOWER_BOUND)
            );
            Assert.DoesNotThrow
            (
                () => new UniformDistribution(LOWER_BOUND, LOWER_BOUND)
            );
        }
        [Test]
        public void Test_ConstructorWithBounds()
        {
            var distribution = new UniformDistribution
            (
                LOWER_BOUND,
                UPPER_BOUND
            );

            Assert.IsNotNull(distribution.Generator);
            Assert.AreEqual(LOWER_BOUND, distribution.LowerBound);
            Assert.AreEqual(UPPER_BOUND, distribution.UpperBound);
        }

        [Test]
        public void Test_ConstructorWithGenerator_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new UniformDistribution(null)
            );
        }
        [Test]
        public void Test_ConstructorWithGenerator()
        {
            var distribution = new UniformDistribution(GENERATOR);

            Assert.AreSame(GENERATOR, distribution.Generator);
            Assert.AreEqual(0, distribution.LowerBound);
            Assert.AreEqual(1, distribution.UpperBound);
        }

        [Test]
        public void Test_DefaultConstructor()
        {
            var distribution = new UniformDistribution();

            Assert.IsNotNull(distribution.Generator);
            Assert.AreEqual(0, distribution.LowerBound);
            Assert.AreEqual(1, distribution.UpperBound);
        }

        [Test]
        public void Test_Equality()
        {
            var original = new UniformDistribution(LOWER_BOUND, UPPER_BOUND);
            var good_copy = new UniformDistribution(LOWER_BOUND, UPPER_BOUND);
            var flawed_lower_bound_copy = new UniformDistribution
            (
                LOWER_BOUND - 1,
                UPPER_BOUND
            );
            var flawed_upper_bound_copy = new UniformDistribution
            (
                LOWER_BOUND,
                UPPER_BOUND + 1
            );

            Assert.AreNotEqual(original, null);
            Assert.AreNotEqual(original, "incompatible type");
            Assert.AreNotEqual(original, flawed_lower_bound_copy);
            Assert.AreNotEqual(original, flawed_upper_bound_copy);

            Assert.AreEqual(original, original);
            Assert.AreEqual(original, good_copy);
        }
    }
}
