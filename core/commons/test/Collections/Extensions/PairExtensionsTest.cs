﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Common.Collections.Extensions.Test
{
    [TestFixture]
    public sealed class PairExtensionsTest
    {
        [Test]
        public void Test_Enumeration()
        {
            var pair = Pair.Create(1, 2);
            var items = pair.AsEnumerable();

            Assert.AreEqual(2, items.Count());
            Assert.AreEqual(pair.First, items.ElementAt(0));
            Assert.AreEqual(pair.Second, items.ElementAt(1));
        }

        [Test]
        public void Test_Mapping()
        {
            var original = Pair.Create(1, 2);
            var image = original.Map(x => 2 * x);

            Assert.AreEqual(2, image.First);
            Assert.AreEqual(4, image.Second);
        }

        [Test]
        public void Test_ConversionToArray()
        {
            var pair = Pair.Create(1, 2);
            int[] array = pair.ToArray();

            Assert.AreEqual(2, array.Length);

            Assert.AreEqual(pair.First, array[0]);
            Assert.AreEqual(pair.Second, array[1]);
        }

        [Test]
        public void Test_ConversionToList()
        {
            var pair = Pair.Create(1, 2);
            List<int> list = pair.ToList();

            Assert.AreEqual(2, list.Count);

            Assert.AreEqual(pair.First, list[0]);
            Assert.AreEqual(pair.Second, list[1]);
        }
    }
}
