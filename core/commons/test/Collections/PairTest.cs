﻿using NUnit.Framework;

namespace Common.Collections.Test
{
    [TestFixture]
    public sealed class PairTest
    {
        private sealed class Foo { }
        private sealed class Bar { }

        private static readonly int INT_ITEM = 0;
        private static readonly string STRING_ITEM = "hello";

        private Pair<int, string> _pair;

        [SetUp]
        public void Setup()
        {
            _pair = new Pair<int, string>(INT_ITEM, STRING_ITEM);
        }

        [Test]
        public void Test_Constructor()
        {
            Assert.AreEqual(INT_ITEM, _pair.First);
            Assert.AreEqual(STRING_ITEM, _pair.Second);
        }

        [Test]
        public void Test_StaticConstructor()
        {
            _pair = Pair.Create(INT_ITEM, STRING_ITEM);

            Assert.AreEqual(INT_ITEM, _pair.First);
            Assert.AreEqual(STRING_ITEM, _pair.Second);
        }

        [Test]
        public void Test_Equality()
        {
            var original = _pair;
            var good_copy = new Pair<int, string>
            (
                _pair.First, 
                _pair.Second
            );
            var flawed_first_item_copy = new Pair<int, string>
            (
                _pair.First + 1, 
                _pair.Second
            );
            var flawed_second_item_copy = new Pair<int, string>
            (
                _pair.First,
                _pair.Second + "foo"
            );

            Assert.AreNotEqual(original, null);
            Assert.AreNotEqual(original, "incompatible type");
            Assert.AreNotEqual(original, flawed_first_item_copy);
            Assert.AreNotEqual(original, flawed_second_item_copy);

            Assert.AreEqual(original, original);
            Assert.AreEqual(original, good_copy);
        }
    }
}
