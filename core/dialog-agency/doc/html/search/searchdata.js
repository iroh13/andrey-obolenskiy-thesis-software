var indexSectionsWithContent =
{
  0: "abcdefgiklmnoprstuvw",
  1: "abcdfilmnoprstuv",
  2: "cd",
  3: "abcdefgilprstuw",
  4: "i",
  5: "a",
  6: "acip",
  7: "acdefgiklmoprstuv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties"
};

