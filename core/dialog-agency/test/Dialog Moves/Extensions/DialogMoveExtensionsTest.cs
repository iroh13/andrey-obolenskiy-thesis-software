﻿using Common.Functional.Options.Extensions;
using Dialog.Agency.Scene;
using Moq;
using NUnit.Framework;
using System;

namespace Dialog.Agency.Dialog_Moves.Extensions.Test
{
    [TestFixture]
    public sealed class DialogMoveExtensionsTest
    {
        private class ActorFoo : Actor { }
        private sealed class ActorFooDerived : ActorFoo { }
        private sealed class ActorBar : Actor { }

        private sealed class MoveKind
            : DialogMoveKind
        {
            public static readonly MoveKind Dummy =
                new MoveKind();

            private MoveKind()
                : base("dummy")
            { }
        }
        private DialogMove<TActor> CreateDummy<TActor>(TActor actor)
            where TActor : Actor
        {
            return new DialogMove<TActor>(MoveKind.Dummy, actor);
        }

        [Test]
        public void Test_IdleCheck_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => ((DialogMove)null).IsIdle()
            );
        }
        [Test]
        public void Test_IdleCheck()
        {
            var non_idle_move = new Mock<DialogMove>().Object;
            var idle_move = IdleMove.Instance;

            Assert.IsFalse(non_idle_move.IsIdle());
            Assert.IsTrue(idle_move.IsIdle());
        }

        [Test]
        public void Test_Casting_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => ((DialogMove)null).Cast<ActorFoo>()
            );
        }
        [Test]
        public void Test_Casting_ToInvalidType()
        {
            Assert.Throws<InvalidCastException>
            (
                () => CreateDummy(new ActorFoo()).Cast<ActorBar>()
            );
        }
        [Test]
        public void Test_Casting_Up()
        {
            var source = CreateDummy<ActorFooDerived>(new ActorFooDerived());
            var cast = source.Cast<ActorFoo>();

            Assert.AreSame(cast.Target.GetValue(), source.Target.GetValue());
        }
        [Test]
        public void Test_Casting_Down()
        {
            var source = CreateDummy<ActorFoo>(new ActorFooDerived());
            var cast = source.Cast<ActorFooDerived>();

            Assert.AreSame(cast.Target.GetValue(), source.Target.GetValue());
        }
    }
}
