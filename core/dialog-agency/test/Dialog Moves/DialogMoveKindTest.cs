﻿using NUnit.Framework;
using System;

namespace Dialog.Agency.Dialog_Moves.Test
{
    [TestFixture]
    public sealed class DialogMoveKindTest
    {
        private static readonly string LABEL = "A";

        private sealed class MockMoveKind
            : DialogMoveKind
        {
            public MockMoveKind(string label)
                : base(label)
            { }
        }

        private MockMoveKind _kind;

        [SetUp]
        public void Setup()
        {
            _kind = new MockMoveKind(LABEL);
        }

        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentException>
            (
                () => new MockMoveKind(null)
            );
            Assert.Throws<ArgumentException>
            (
                () => new MockMoveKind("")
            );
            Assert.Throws<ArgumentException>
            (
                () => new MockMoveKind(" ")
            );
        }
        [Test]
        public void Test_Constructor()
        {
            Assert.AreEqual(LABEL, _kind.Label);
        }
    }
}
