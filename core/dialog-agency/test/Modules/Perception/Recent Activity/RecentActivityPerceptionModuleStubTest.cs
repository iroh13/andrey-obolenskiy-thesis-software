﻿using Dialog.Agency.State;
using Moq;
using NUnit.Framework;

namespace Dialog.Agency.Modules.Perception.Test
{
    [TestFixture]
    public sealed class RecentActivityPerceptionModuleStubTest
    {
        private static readonly ImmutableState
            STATE = new Mock<ImmutableState>().Object;

        private RecentActivityPerceptionModuleStub _stub;

        [SetUp]
        public void Setup()
        {
            _stub = new RecentActivityPerceptionModuleStub();
            _stub.Initialize(STATE);
        }

        [Test]
        public void Test_ActivityPerception()
        {
            RecentActivityReport report = _stub.PerceiveActivity();

            Assert.AreEqual(0, report.Events.Count);
        }
    }
}
