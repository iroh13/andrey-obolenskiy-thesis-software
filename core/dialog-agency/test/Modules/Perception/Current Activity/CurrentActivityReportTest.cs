﻿using Dialog.Agency.Scene;
using Moq;
using NUnit.Framework;
using System;

namespace Dialog.Agency.Modules.Perception.Test
{
    [TestFixture]
    public sealed class CurrentActivityReportTest
    {
        private static readonly Actor 
            ALICE = new Mock<Actor>().Object;
        private static readonly Actor 
            BOB = new Mock<Actor>().Object;

        private CurrentActivityReport _report;

        [SetUp]
        public void Setup()
        {
            _report = new CurrentActivityReport();
        }

        [Test]
        public void Test_InitialState()
        {
            Assert.AreEqual(0, _report.Actors.Count);
            Assert.AreEqual(0, _report.PassiveActors.Count);
            Assert.AreEqual(0, _report.ActiveActors.Count);
        }

        [Test]
        public void Test_PassiveActivityAddition_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => _report.AddAsPassive(null)
            );
        }
        [Test]
        public void Test_PassiveActivityAddition_OfExistingPassiveActor()
        {
            _report.AddAsPassive(ALICE);
            bool success = _report.AddAsPassive(ALICE);

            Assert.IsFalse(success);
        }
        [Test]
        public void Test_PassiveActivityAddition_OfExistingActiveActor()
        {
            _report.AddAsActive(ALICE);
            bool success = _report.AddAsPassive(ALICE);

            Assert.IsFalse(success);
        }
        [Test]
        public void Test_PassiveActivityAddition()
        {
            bool success = _report.AddAsPassive(ALICE);

            Assert.IsTrue(success);
            Assert.AreEqual(1, _report.Actors.Count);
            Assert.AreEqual(1, _report.PassiveActors.Count);
            Assert.IsTrue(_report.Actors.Contains(ALICE));
            Assert.IsTrue(_report.PassiveActors.Contains(ALICE));
        }

        [Test]
        public void Test_ActiveActivityAddition_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => _report.AddAsActive(null)
            );
        }
        [Test]
        public void Test_ActiveActivityAddition_OfExistingPassiveActor()
        {
            _report.AddAsPassive(ALICE);
            bool success = _report.AddAsActive(ALICE);

            Assert.IsFalse(success);
        }
        [Test]
        public void Test_ActiveActivityAddition_OfExistingActiveActor()
        {
            _report.AddAsActive(ALICE);
            bool success = _report.AddAsActive(ALICE);

            Assert.IsFalse(success);
        }
        [Test]
        public void Test_ActiveActivityAddition()
        {
            bool success = _report.AddAsActive(ALICE);

            Assert.IsTrue(success);
            Assert.AreEqual(1, _report.Actors.Count);
            Assert.AreEqual(1, _report.ActiveActors.Count);
            Assert.IsTrue(_report.Actors.Contains(ALICE));
            Assert.IsTrue(_report.ActiveActors.Contains(ALICE));
        }

        [Test]
        public void Test_ActivityStatusCheck_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => _report.GetStatus(null)
            );
            Assert.Throws<ArgumentException>
            (
                () => _report.GetStatus(ALICE)
            );
        }
        [Test]
        public void Test_ActivityStatusCheck()
        {
            _report.AddAsPassive(ALICE);
            _report.AddAsActive(BOB);

            Assert.AreEqual
            (
                ActorActivityStatus.Passive, 
                _report.GetStatus(ALICE)
            );
            Assert.AreEqual
            (
                ActorActivityStatus.Active,
                _report.GetStatus(BOB)
            );
        }
    }
}
