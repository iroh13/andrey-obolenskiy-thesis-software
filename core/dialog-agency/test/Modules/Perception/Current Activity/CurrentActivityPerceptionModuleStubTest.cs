﻿using Dialog.Agency.State;
using Moq;
using NUnit.Framework;

namespace Dialog.Agency.Modules.Perception.Test
{
    [TestFixture]
    public sealed class CurrentActivityPerceptionModuleStubTest
    {
        private static readonly ImmutableState 
            STATE = new Mock<ImmutableState>().Object;

        private CurrentActivityPerceptionModuleStub _stub;

        [SetUp]
        public void Setup()
        {
            _stub = new CurrentActivityPerceptionModuleStub();
            _stub.Initialize(STATE);
        }

        [Test]
        public void Test_ActivityPerception()
        {
            CurrentActivityReport report = _stub.PerceiveActivity();

            Assert.AreEqual(0, report.Actors.Count);
        }
    }
}
