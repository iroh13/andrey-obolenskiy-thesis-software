﻿using Dialog.Agency.Dialog_Moves;
using Moq;
using NUnit.Framework;
using System;

namespace Dialog.Agency.System.Test
{
    [TestFixture]
    public sealed class SystemActivitySnapshotTest
    {
        private static readonly DialogMove
            RECENT_MOVE = new Mock<DialogMove>().Object;
        private static readonly DialogMove
            TARGET_MOVE = new Mock<DialogMove>().Object;
        private static readonly DialogMove
            ACTUAL_MOVE = new Mock<DialogMove>().Object;

        private SystemActivitySnapshot _system_activity;

        [SetUp]
        public void Setup()
        {
            _system_activity = new SystemActivitySnapshot
            (
                RECENT_MOVE,
                TARGET_MOVE,
                ACTUAL_MOVE
            );
        }

        [Test]
        public void Test_Construcor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new SystemActivitySnapshot
                (
                    null, 
                    TARGET_MOVE, 
                    ACTUAL_MOVE
                )
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new SystemActivitySnapshot
                (
                    RECENT_MOVE,
                    null,
                    ACTUAL_MOVE
                )
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new SystemActivitySnapshot
                (
                    RECENT_MOVE,
                    TARGET_MOVE,
                    null
                )
            );
        }
        [Test]
        public void Test_Constructor()
        {
            Assert.AreEqual(RECENT_MOVE, _system_activity.RecentMove);
            Assert.AreEqual(TARGET_MOVE, _system_activity.TargetMove);
            Assert.AreEqual(ACTUAL_MOVE, _system_activity.ActualMove);
        }

        [Test]
        public void Test_Equality()
        {
            var original = _system_activity;
            var good_copy = new SystemActivitySnapshot
            (
                original.RecentMove, 
                original.TargetMove,
                original.ActualMove
            );
            var flawed_recent_move_copy = new SystemActivitySnapshot
            (
                new Mock<DialogMove>().Object,
                original.TargetMove,
                original.ActualMove
            );
            var flawed_target_move_copy = new SystemActivitySnapshot
            (
                original.RecentMove,
                new Mock<DialogMove>().Object,
                original.ActualMove
            );
            var flawed_actual_move_copy = new SystemActivitySnapshot
            (
                original.RecentMove,
                original.TargetMove,
                new Mock<DialogMove>().Object
            );

            Assert.AreNotEqual(original, null);
            Assert.AreNotEqual(original, "incompatible type");
            Assert.AreNotEqual(original, flawed_recent_move_copy);
            Assert.AreNotEqual(original, flawed_target_move_copy);
            Assert.AreNotEqual(original, flawed_actual_move_copy);

            Assert.AreEqual(original, original);
            Assert.AreEqual(original, good_copy);
        }
    }
}
