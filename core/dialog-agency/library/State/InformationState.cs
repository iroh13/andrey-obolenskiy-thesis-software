﻿using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.DesignPatterns;
using Common.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dialog.Agency.State
{
    /// <summary>
    /// The information state is an object that is shared among all operation
    /// modules. It is to be used as a container for any data that is either:
    /// 1. used by more than one module, or 2. is generated based on data 
    /// produced by the perception modules.
    /// 
    /// The state organizes its data into "components". Each component is 
    /// associated with one object of one type, and the state holds at most 
    /// one component per type. Which components are available on a given state 
    /// object is specified during its construction, and can be queried through 
    /// the <see cref="SupportedComponents"/> property.
    /// </summary>
    public sealed class InformationState
        : MutableState
    {
        /// <summary>
        /// This class is used to build instances of 
        /// <see cref="InformationState"/>. 
        /// </summary>
        public sealed class Builder
            : ObjectBuilder<InformationState>
        {
            /// <summary>
            /// Specifies that the state should include a component for the 
            /// specified data type.
            /// </summary>
            /// <typeparam name="T">
            /// The type of data the component holds.
            /// </typeparam>
            /// <param name="initial_value">
            /// The component's initial value.
            /// </param>
            /// <returns>This instance (for method call-chaining).</returns>
            /// <remarks>
            /// Calling this on a built object is invalid.
            /// Calling this with the same type parameter on the same builder
            /// more than once is invalid.
            /// <paramref name="initial_value"/> must not be null.
            /// </remarks>
            public Builder Support<T>(T initial_value)
            {
                Type type = typeof(T);

                Require.IsFalse<InvalidOperationException>(IsBuilt);
                Require.IsFalse<InvalidOperationException>
                (
                    _container_by_data_type.ContainsKey(type)
                );
                Require.IsNotNull(initial_value);

                _container_by_data_type.Add
                (
                    type, 
                    new StateComponent<T>(initial_value)
                );

                return this;
            }

            /// <summary>
            /// Creates the object.
            /// </summary>
            /// <returns>
            /// The built object.
            /// </returns>
            protected override InformationState CreateObject()
            {
                return new InformationState(_container_by_data_type.Values);
            }

            private readonly 
                Dictionary<Type, StateComponent>
                _container_by_data_type = 
                new Dictionary<Type, StateComponent>();
        }

        /// <summary>
        /// Creates a new state from the specified components.
        /// </summary>
        /// <param name="components">The components to hold.</param>
        private InformationState(IEnumerable<StateComponent> components)
        {
            foreach (var component in components)
            {
                _component_by_data_type.Add(component.DataType, component);
            }
            SupportedComponents = 
                new CollectionView<Type>(_component_by_data_type.Keys);
        }

        /// <summary>
        /// Gets the collection of supported data types.
        /// </summary>
        public ImmutableCollection<Type> SupportedComponents
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the data corresponding to the specified type.
        /// </summary>
        /// <typeparam name="T">The type of data to get.</typeparam>
        /// <returns>
        /// The data corresponding to the specified type.
        /// </returns>
        /// <remarks>
        /// Attempting to retrieve data from a non-existent container is 
        /// invalid.
        /// </remarks>
        public T Get<T>()
        {
            Require.IsTrue(SupportedComponents.Contains(typeof(T)));

            return 
                ((StateComponent<T>)_component_by_data_type[typeof(T)]).Value;
        }
        /// <summary>
        /// Sets the data corresponding to the specified type.
        /// </summary>
        /// <typeparam name="T">The type of data to set.</typeparam>
        /// <param name="value">The value to assign.</param>
        /// <remarks>
        /// Attempting to set data of a non-existent container is invalid.
        /// <paramref name="value"/> must not be null.
        /// </remarks>
        public void Set<T>(T value)
        {
            Require.IsTrue(SupportedComponents.Contains(typeof(T)));
            Require.IsNotNull(value);

            _component_by_data_type[typeof(T)] = new StateComponent<T>(value);
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            string types = string.Join
            (
                ", ",
                SupportedComponents
                    .Select(item => item.ToString())
                    .ToArray()
            );
            return $"{nameof(InformationState)}(" +
                   $"{nameof(SupportedComponents)} = [{types}])";
        }

        private readonly Dictionary<Type, StateComponent> 
            _component_by_data_type = new Dictionary<Type, StateComponent>();
    }
}
