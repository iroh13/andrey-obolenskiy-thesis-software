﻿using Common.Collections.Interfaces;
using System;

namespace Dialog.Agency.State
{
    /// <summary>
    /// This interface represents an immutable information state.
    /// </summary>
    public interface ImmutableState
    {
        /// <summary>
        /// Gets the collection of supported components.
        /// </summary>
        ImmutableCollection<Type> SupportedComponents { get; }

        /// <summary>
        /// Gets the component corresponding to the specified type.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <returns>
        /// The component corresponding to the specified type.
        /// </returns>
        T Get<T>();
    }
}
