﻿using Dialog.Agency.Scene;

namespace Dialog.Agency.Dialog_Moves
{
    /// <summary>
    /// This class represents the special idle dialog move, indicating 
    /// inaction.
    /// </summary>
    public sealed class IdleMove
        : DialogMove<Actor>
    {
        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        public static IdleMove Instance { get; private set; }

        /// <summary>
        /// Initializes static properties.
        /// </summary>
        static IdleMove()
        {
            Instance = new IdleMove();
        }

        /// <summary>
        /// Creates a new idle move.
        /// </summary>
        private IdleMove()
            : base(DialogMoveKind.Idle)
        { }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(IdleMove)}()";
        }
    }
}
