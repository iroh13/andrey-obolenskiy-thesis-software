﻿using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.Validation;
using Dialog.Agency.Scene;
using System.Collections.Generic;

namespace Dialog.Agency.Modules.Perception
{
    /// <summary>
    /// This class is the medium through which the 
    /// <see cref="CurrentActivityPerceptionModule"/> reports its output.
    /// </summary>
    public sealed class CurrentActivityReport
    {
        /// <summary>
        /// Creates a new report.
        /// </summary>
        public CurrentActivityReport()
        {
            Actors = new CollectionView<Actor>(_perceived_actors);
            PassiveActors = new CollectionView<Actor>(_passive_actors);
            ActiveActors = new CollectionView<Actor>(_active_actors);
        }

        /// <summary>
        /// Gets the collection of all reported actors.
        /// </summary>
        public ImmutableCollection<Actor> Actors { get; private set; }
        /// <summary>
        /// Gets the collection of passive actors.
        /// </summary>
        public ImmutableCollection<Actor> PassiveActors { get; private set; }
        /// <summary>
        /// Gets the collection of active actors.
        /// </summary>
        public ImmutableCollection<Actor> ActiveActors { get; private set; }

        /// <summary>
        /// Gets the activity status of the specified actor.
        /// </summary>
        /// <param name="actor">The actor to query.</param>
        /// <returns>
        /// <see cref="ActorActivityStatus.Passive"/> if the specified actor is 
        /// present in <see cref="PassiveActors"/>, otherwise
        /// <see cref="ActorActivityStatus.Active"/>.  
        /// </returns>
        /// <remarks>
        /// <paramref name="actor"/> must be one of <see cref="Actors"/>. 
        /// </remarks>
        public ActorActivityStatus GetStatus(Actor actor)
        {
            Require.IsNotNull(actor);
            Require.IsTrue(Actors.Contains(actor));
            
            if (PassiveActors.Contains(actor))
            {
                return ActorActivityStatus.Passive;
            }
            else
            {
                return ActorActivityStatus.Active;
            }
        }

        /// <summary>
        /// Adds the specified actor to this report, marked as passive.
        /// </summary>
        /// <param name="actor">The actor to add.</param>
        /// <returns>
        /// True if the actor was added successfully, otherwise false.
        /// </returns>
        /// <remarks>
        /// <paramref name="actor"/> must not be null.
        /// An addition is unsuccessful when the actor has already been
        /// submitted before.
        /// </remarks>
        public bool AddAsPassive(Actor actor)
        {
            Require.IsNotNull(actor);
            
            if (Actors.Contains(actor)) { return false; }
            else
            {
                _perceived_actors.Add(actor);
                _passive_actors.Add(actor);

                return true;
            }
        }
        /// <summary>
        /// Adds the specified actor to this submission, marked as active.
        /// </summary>
        /// <param name="actor">The actor to add.</param>
        /// <returns>
        /// True if the actor was added successfully, otherwise false.
        /// </returns>
        /// <remarks>
        /// <paramref name="actor"/> must not be null.
        /// An addition is unsuccessful when the actor has already been
        /// submitted before.
        /// </remarks>
        public bool AddAsActive(Actor actor)
        {
            Require.IsNotNull(actor);

            if (Actors.Contains(actor)) { return false; }
            else
            {
                _perceived_actors.Add(actor);
                _active_actors.Add(actor);

                return true;
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(CurrentActivityReport)}(" +
                   $"{nameof(PassiveActors)} = {PassiveActors}, " + 
                   $"{nameof(ActiveActors)} = {ActiveActors})";
        }

        private readonly HashSet<Actor> 
            _perceived_actors = new HashSet<Actor>();
        private readonly HashSet<Actor>
            _active_actors = new HashSet<Actor>();
        private readonly HashSet<Actor>
            _passive_actors = new HashSet<Actor>();
    }
}
