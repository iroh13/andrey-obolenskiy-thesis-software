﻿using Common.Validation;

namespace Dialog.Agency.Modules.Perception
{
    /// <summary>
    /// This class is a stub implementation of the 
    /// <see cref="CurrentActivityPerceptionModule"/>.
    /// </summary>
    public sealed class CurrentActivityPerceptionModuleStub
        : CurrentActivityPerceptionModule
    {
        /// <summary>
        /// Fills in a blank activity report.
        /// </summary>
        /// <param name="report">The report to fill in.</param>
        /// <remarks>
        /// <paramref name="report"/> must not be null.
        /// </remarks>
        protected override void ComposeActivityReport
        (
            CurrentActivityReport report
        )
        {
            Require.IsNotNull(report);
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(CurrentActivityPerceptionModuleStub)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
