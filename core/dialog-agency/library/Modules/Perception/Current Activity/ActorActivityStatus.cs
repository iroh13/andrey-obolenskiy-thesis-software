﻿namespace Dialog.Agency.Modules.Perception
{
    /// <summary>
    /// Enumerates possible values of current-activity status of actors.
    /// </summary>
    public enum ActorActivityStatus : int
    {
        /// <summary>
        /// Referencing an idle actor.
        /// </summary>
        Passive = 0,
        /// <summary>
        /// Referencing a non-idle actor.
        /// </summary>
        Active = 1
    }
}
