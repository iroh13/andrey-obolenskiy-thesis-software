﻿using Common.Hashing;
using Common.Validation;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Scene;

namespace Dialog.Agency.Modules.Perception
{
    /// <summary>
    /// This structure describes the perceived event of a dialog move having 
    /// been fully realized.
    /// </summary>
    public sealed class DialogMoveRealizationEvent
    {
        /// <summary>
        /// Creates a new event.
        /// </summary>
        /// <param name="source">The actor who produced the move.</param>
        /// <param name="move">The move that was made.</param>
        /// <remarks>
        /// <paramref name="source"/> must not be null.
        /// <paramref name="move"/> must not be null.
        /// </remarks>
        public DialogMoveRealizationEvent(Actor source, DialogMove move)
        {
            Require.IsNotNull(source);
            Require.IsNotNull(move);

            Source = source;
            Move = move;
        }

        /// <summary>
        /// Gets the source of the move.
        /// </summary>
        public Actor Source { get; private set; }

        /// <summary>
        /// Gets the realized move.
        /// </summary>
        public DialogMove Move { get; private set; }

        /// <summary>
        /// Determines whether the specified object is equal to this instance.
        /// </summary>
        /// <param name="obj">The object to compare with.</param>
        /// <returns>
        /// True if the specified object is equal to this instance; 
        /// otherwise false.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as DialogMoveRealizationEvent;

            if (ReferenceEquals(other, null)) { return false; }
            if (ReferenceEquals(other, this)) { return true; }

            return other.Source.Equals(Source) &&
                   other.Move.Equals(Move);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing 
        /// algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hash = HashCombiner.Initialize();
            hash = HashCombiner.Hash(hash, Source);
            hash = HashCombiner.Hash(hash, Move);

            return hash;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(DialogMoveRealizationEvent)}(" +
                   $"{nameof(Source)} = {Source}, " +
                   $"{nameof(Move)} = {Move})";
        }
    }
}
