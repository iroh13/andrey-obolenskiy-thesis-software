﻿using Dialog.Agency.State;

namespace Dialog.Agency.Modules.Perception
{
    /// <summary>
    /// This module is responsible for perceiving recently occurring dialog 
    /// moves.
    /// </summary>
    public abstract class RecentActivityPerceptionModule
        : OperationModule<ImmutableState>
    {
        /// <summary>
        /// Perceives recent actor activity and compiles it into a
        /// <see cref="RecentActivityReport"/>.
        /// </summary>
        public RecentActivityReport PerceiveActivity()
        {
            var output = new RecentActivityReport();
            ComposeActivityReport(output);
            return output;
        }

        /// <summary>
        /// Fills in an activity report.
        /// </summary>
        /// <param name="report">The report to fill in.</param>
        protected abstract void ComposeActivityReport
        (
            RecentActivityReport report
        );

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(RecentActivityPerceptionModule)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
