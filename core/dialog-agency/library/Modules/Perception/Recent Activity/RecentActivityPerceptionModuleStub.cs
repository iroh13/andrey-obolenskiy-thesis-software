﻿using Common.Validation;

namespace Dialog.Agency.Modules.Perception
{
    /// <summary>
    /// This class is a stub implementation of the 
    /// <see cref="RecentActivityPerceptionModule"/>.
    /// </summary>
    public sealed class RecentActivityPerceptionModuleStub
        : RecentActivityPerceptionModule
    {
        /// <summary>
        /// Fills in a blank activity report.
        /// </summary>
        /// <param name="report">The report to fill in.</param>
        /// <remarks>
        /// <paramref name="report"/> must not be null.
        /// </remarks>
        protected override void ComposeActivityReport
        (
            RecentActivityReport report
        )
        {
            Require.IsNotNull(report);
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(RecentActivityPerceptionModuleStub)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
