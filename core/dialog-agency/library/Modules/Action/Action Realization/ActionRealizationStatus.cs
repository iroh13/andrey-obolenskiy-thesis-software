﻿namespace Dialog.Agency.Modules.Action
{
    /// <summary>
    /// Enumerates possible values of an actions's realization status.
    /// </summary>
    public enum ActionRealizationStatus : int
    {
        /// <summary>
        /// Referring to a completed realization.
        /// </summary>
        Complete = 0,
        /// <summary>
        /// Referring to a realization in progress.
        /// </summary>
        InProgress = 1
    }
}
