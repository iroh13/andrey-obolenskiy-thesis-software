﻿using Common.Validation;
using Dialog.Agency.Dialog_Moves;

namespace Dialog.Agency.Modules.Action
{
    /// <summary>
    /// This class is a stub implementation of the 
    /// <see cref="ActionRealizationModule"/>.
    /// </summary>
    public sealed class ActionRealizationModuleStub
        : ActionRealizationModule
    {
        /// <summary>
        /// Realizes the specified dialog move.
        /// </summary>
        /// <param name="move">The dialog move to realize.</param>
        /// <returns>
        /// Information regarding the status of the realization.
        /// </returns>
        /// <remarks>
        /// <paramref name="move"/> must not be null.
        /// </remarks>
        public override ActionRealizationStatus RealizeMove(DialogMove move)
        {
            Require.IsNotNull(move);

            return ActionRealizationStatus.Complete;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ActionRealizationModuleStub)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
