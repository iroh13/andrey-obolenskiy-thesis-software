﻿using Common.Validation;
using Dialog.Agency.Dialog_Moves;

namespace Dialog.Agency.Modules.Action
{
    /// <summary>
    /// This class is a stub implementation of the 
    /// <see cref="ActionTimingModule"/>.
    /// </summary>
    public sealed class ActionTimingModuleStub
        : ActionTimingModule
    {
        /// <summary>
        /// Determines whether now is a valid time to realize the specified 
        /// dialog move.
        /// </summary>
        /// <param name="move">The desired dialog move to make.</param>
        /// <returns>True.</returns>
        /// <remarks>
        /// <paramref name="move"/> must not be null.
        /// </remarks>
        public override bool IsValidMoveNow(DialogMove move)
        {
            Require.IsNotNull(move);

            return true;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ActionTimingModuleStub)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
