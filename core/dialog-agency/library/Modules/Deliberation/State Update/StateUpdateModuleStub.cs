﻿using Common.Validation;
using Dialog.Agency.Modules.Perception;
using Dialog.Agency.System;

namespace Dialog.Agency.Modules.Deliberation
{
    /// <summary>
    /// This class is a stub implementation of the 
    /// <see cref="StateUpdateModule"/>.
    /// </summary>
    public sealed class StateUpdateModuleStub
        : StateUpdateModule
    {
        /// <summary>
        /// Updates the information state based on recent perceived activity.
        /// </summary>
        /// <param name="recent_activity">
        /// The recent activity report, as was generated by 
        /// <see cref="RecentActivityPerceptionModule"/>.
        /// </param>
        /// <param name="current_activity">
        /// The current activity report, as was generated by
        /// <see cref="CurrentActivityPerceptionModule"/>.
        /// </param>
        /// <param name="system_activity">
        /// A snapshot the system's current activity.
        /// </param>
        /// <remarks>
        /// <paramref name="recent_activity"/> must not be null.
        /// <paramref name="current_activity"/> must not be null.
        /// <paramref name="system_activity"/> must not be null.
        /// </remarks>
        public override void PerformUpdate
        (
            RecentActivityReport recent_activity, 
            CurrentActivityReport current_activity, 
            SystemActivitySnapshot system_activity
        )
        {
            Require.IsNotNull(recent_activity);
            Require.IsNotNull(current_activity);
            Require.IsNotNull(system_activity);
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(StateUpdateModuleStub)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
