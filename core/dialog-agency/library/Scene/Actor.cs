﻿namespace Dialog.Agency.Scene
{
    /// <summary>
    /// This interface represents a participant in a conversation.
    /// </summary>
    public interface Actor { }
}
