﻿using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.Validation;
using Dialog.Communication.Packets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dialog.Communication.Management
{
    public sealed partial class CommunicationManager
    {
        /// <summary>
        /// This class represents an actor's data subscription preferences.
        /// </summary>
        public sealed class DataSubscription
        {
            /// <summary>
            /// The data handler delegate associated with the specified type.
            /// </summary>
            /// <typeparam name="T">The type of data handled.</typeparam>
            public delegate void Handler<T>
            (
                CommunicationManager manager,
                IEnumerable<DataPacket<T>> packets
            );

            /// <summary>
            /// Creates a new subscription with the specified manager.
            /// </summary>
            /// <param name="manager">The subscription's manager.</param>
            /// <remarks>
            /// <paramref name="manager"/> must not be null.
            /// </remarks>
            internal DataSubscription(CommunicationManager manager)
            {
                Require.IsNotNull(manager);

                Manager = manager;
                DataTypes = new CollectionView<Type>(_handler_by_type.Keys);
            }

            /// <summary>
            /// Gets this subscription's manager.
            /// </summary>
            public CommunicationManager Manager { get; private set; }

            /// <summary>
            /// Gets the included data types.
            /// </summary>
            public ImmutableCollection<Type> DataTypes { get; private set; }

            /// <summary>
            /// Indicates whether this subscription is active. That is, whether
            /// it is associated with an engaged actor.
            /// </summary>
            public bool IsActive
            {
                get { return Manager.ActiveDataSubscriptions.Contains(this); }
            }

            /// <summary>
            /// Includes the specified data type in the subscription.
            /// </summary>
            /// <typeparam name="T">The type of data to include.</typeparam>
            /// <param name="handler">
            /// The delegate to invoke for handling data of the specified type.
            /// </param>
            /// <remarks>
            /// <typeparamref name="T"/> must be supported by the manager.
            /// <paramref name="handler"/> must not be null.
            /// </remarks>
            public void Include<T>(Handler<T> handler)
            {
                Require.IsNotNull(handler);
                Require.IsTrue(Manager.DataTypes.Contains(typeof(T)));

                _handler_by_type[typeof(T)] = () =>
                {
                    var packets = Manager._channels.Get<T>().Packets;
                    handler.Invoke(Manager, packets);
                };
            }

            /// <summary>
            /// Excludes the specified type from the subscription.
            /// </summary>
            /// <typeparam name="T">The type of data to exclude.</typeparam>
            public void Exclude<T>()
            {
                Exclude(typeof(T));
            }
            /// <summary>
            /// Excludes the specified type from the subscription.
            /// </summary>
            /// <param name="type">The type of data to exclude.</param>
            /// <remarks>
            /// <paramref name="type"/> must not be null.
            /// </remarks>
            public void Exclude(Type type)
            {
                Require.IsNotNull(type);

                _handler_by_type.Remove(type);
            }

            /// <summary>
            /// Returns a string that represents this instance.
            /// </summary>
            /// <returns>
            /// A human-readable string.
            /// </returns>
            public override string ToString()
            {
                string types = string.Join
                (
                    ", ",
                    DataTypes
                        .Select(item => item.ToString())
                        .ToArray()
                );
                return $"{nameof(DataSubscription)}(" +
                       $"{nameof(Manager)} = {Manager}, " +
                       $"{nameof(DataTypes)} = [{types}])";
            }

            /// <summary>
            /// Invokes the handlers of data types included in this 
            /// subscription with the relevant data packets obtained from
            /// the communication manager.
            /// </summary>
            /// <remarks>
            /// This subscription must be active.
            /// </remarks>
            internal void Invoke()
            {
                Require.IsTrue<InvalidOperationException>(IsActive);

                foreach (var handler in _handler_by_type.Values)
                {
                    handler.Invoke();
                }
            }

            private readonly Dictionary<Type, Action>
                _handler_by_type = new Dictionary<Type, Action>();
        }
    }
}
