﻿using Dialog.Communication.Actors;

namespace Dialog.Communication.Packets
{
    /// <summary>
    /// This interface represents a wrapper of data in transit. It is used to 
    /// associate a piece of data with its authoring actor.
    /// </summary>
    public interface DataPacket
    {
        /// <summary>
        /// Gets the packet's author.
        /// </summary>
        DataAuthor Author { get; }
    }
    /// <summary>
    /// This interface represents a wrapper of data in transit. It is used to 
    /// associate a piece of data with its authoring actor.
    /// </summary>
    /// <typeparam name="T">The type of the payload data.</typeparam>
    public interface DataPacket<T>
        : DataPacket
    {
        /// <summary>
        /// Gets the payload data.
        /// </summary>
        T Payload { get; }
    }
}
