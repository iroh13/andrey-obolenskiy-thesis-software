﻿using Common.Collections.Interfaces;
using Common.Validation;
using Dialog.Communication.Packets;
using System;

namespace Dialog.Communication.Channels
{
    /// <summary>
    /// A generic implementation of the 
    /// <see cref="DoubleBufferedDataChannel{T}"/> interface.
    /// </summary>
    /// <typeparam name="T">The type of data the channel carries.</typeparam>
    public sealed class GenericDoubleBufferedChannel<T>
        : DoubleBufferedDataChannel<T>
    {
        /// <summary>
        /// Gets the type of data the channel carries.
        /// </summary>
        public Type DataType { get; private set; } = typeof(T);

        /// <summary>
        /// Gets the packets currently live on the front buffer.
        /// </summary>
        public ImmutableCollection<DataPacket<T>> Packets
        {
            get { return _front_buffer.Packets; }
        }

        /// <summary>
        /// Clears the back buffer.
        /// </summary>
        public void Clear()
        {
            _back_buffer.Clear();
        }

        /// <summary>
        /// Posts the specified packet onto the back buffer.
        /// </summary>
        /// <param name="packet">The packet to post.</param>
        /// <remarks>
        /// <paramref name="packet"/> must not be null.
        /// </remarks>
        public void Post(DataPacket<T> packet)
        {
            Require.IsNotNull(packet);

            _back_buffer.Post(packet);
        }

        /// <summary>
        /// Swaps the front and back buffers.
        /// </summary>
        public void SwapBuffers()
        {
            var formerly_front = _front_buffer;
            _front_buffer = _back_buffer;
            _back_buffer = formerly_front;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(GenericDoubleBufferedChannel<T>)}(" +
                   $"{nameof(DataType)} = {DataType}, " +
                   $"{nameof(Packets)} = {Packets})";
        }

        private GenericChannel<T> _front_buffer = new GenericChannel<T>();
        private GenericChannel<T> _back_buffer = new GenericChannel<T>();
    }
}
