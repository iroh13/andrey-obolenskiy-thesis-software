var hierarchy =
[
    [ "Dialog.Communication.Management.ChannelBatch", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch.html", null ],
    [ "Common.Time.Clock", "interface_common_1_1_time_1_1_clock.html", [
      [ "Common.Time.VariableIncrementClock", "class_common_1_1_time_1_1_variable_increment_clock.html", null ]
    ] ],
    [ "Dialog.Communication.Management.CommunicationManager", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager.html", null ],
    [ "Comparer", null, [
      [ "Common.Functional.FunctionalComparer< T >", "class_common_1_1_functional_1_1_functional_comparer.html", null ]
    ] ],
    [ "Dialog.Communication.Actors.DataAuthor", "interface_dialog_1_1_communication_1_1_actors_1_1_data_author.html", [
      [ "Dialog.Communication.Actors.ManagedActor", "interface_dialog_1_1_communication_1_1_actors_1_1_managed_actor.html", null ]
    ] ],
    [ "Dialog.Communication.Channels.DataChannel", "interface_dialog_1_1_communication_1_1_channels_1_1_data_channel.html", [
      [ "Dialog.Communication.Channels.DataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_data_channel.html", null ],
      [ "Dialog.Communication.Channels.DoubleBufferedDataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_double_buffered_data_channel.html", null ]
    ] ],
    [ "Dialog.Communication.Channels.DataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_data_channel.html", [
      [ "Dialog.Communication.Channels.DoubleBufferedDataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_double_buffered_data_channel.html", null ],
      [ "Dialog.Communication.Channels.GenericChannel< T >", "class_dialog_1_1_communication_1_1_channels_1_1_generic_channel.html", null ]
    ] ],
    [ "Dialog.Communication.Packets.DataPacket", "interface_dialog_1_1_communication_1_1_packets_1_1_data_packet.html", [
      [ "Dialog.Communication.Packets.DataPacket< T >", "interface_dialog_1_1_communication_1_1_packets_1_1_data_packet.html", null ]
    ] ],
    [ "Dialog.Communication.Packets.DataPacket< T >", "interface_dialog_1_1_communication_1_1_packets_1_1_data_packet.html", [
      [ "Dialog.Communication.Packets.GenericPacket< T >", "class_dialog_1_1_communication_1_1_packets_1_1_generic_packet.html", null ]
    ] ],
    [ "Dialog.Communication.Management.CommunicationManager.DataSubmission", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_data_submission.html", null ],
    [ "Dialog.Communication.Management.CommunicationManager.DataSubscription", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_data_subscription.html", null ],
    [ "Common.Randomization.Distributions.Distribution", "interface_common_1_1_randomization_1_1_distributions_1_1_distribution.html", [
      [ "Common.Randomization.Distributions.PseudoRandomDistribution", "class_common_1_1_randomization_1_1_distributions_1_1_pseudo_random_distribution.html", [
        [ "Common.Randomization.Distributions.UniformDistribution", "class_common_1_1_randomization_1_1_distributions_1_1_uniform_distribution.html", null ]
      ] ]
    ] ],
    [ "Dialog.Communication.Channels.DoubleBufferedDataChannel", "interface_dialog_1_1_communication_1_1_channels_1_1_double_buffered_data_channel.html", [
      [ "Dialog.Communication.Channels.DoubleBufferedDataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_double_buffered_data_channel.html", null ]
    ] ],
    [ "Dialog.Communication.Channels.DoubleBufferedDataChannel< T >", "interface_dialog_1_1_communication_1_1_channels_1_1_double_buffered_data_channel.html", [
      [ "Dialog.Communication.Channels.GenericDoubleBufferedChannel< T >", "class_dialog_1_1_communication_1_1_channels_1_1_generic_double_buffered_channel.html", null ]
    ] ],
    [ "IEnumerable", null, [
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ],
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ]
    ] ],
    [ "Common.Collections.Interfaces.ImmutableCollection", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", [
      [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", null ]
    ] ],
    [ "Common.Collections.Interfaces.ImmutableCollection< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_collection.html", [
      [ "Common.Collections.Interfaces.ImmutableList< T >", "interface_common_1_1_collections_1_1_interfaces_1_1_immutable_list.html", [
        [ "Common.Collections.Views.ListView< T >", "class_common_1_1_collections_1_1_views_1_1_list_view.html", null ]
      ] ],
      [ "Common.Collections.Views.CollectionView< T >", "class_common_1_1_collections_1_1_views_1_1_collection_view.html", [
        [ "Common.Collections.Views.ListView< T >", "class_common_1_1_collections_1_1_views_1_1_list_view.html", null ]
      ] ]
    ] ],
    [ "Common.DesignPatterns.ObjectBuilder< T >", "class_common_1_1_design_patterns_1_1_object_builder.html", null ],
    [ "Common.DesignPatterns.ObjectBuilder< ChannelBatch >", "class_common_1_1_design_patterns_1_1_object_builder.html", [
      [ "Dialog.Communication.Management.ChannelBatch.Builder", "class_dialog_1_1_communication_1_1_management_1_1_channel_batch_1_1_builder.html", null ]
    ] ],
    [ "Common.DesignPatterns.ObjectBuilder< CommunicationManager >", "class_common_1_1_design_patterns_1_1_object_builder.html", [
      [ "Dialog.Communication.Management.CommunicationManager.Builder", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_builder.html", null ]
    ] ],
    [ "Common.Functional.Options.Option< T >", "interface_common_1_1_functional_1_1_options_1_1_option.html", [
      [ "Common.Functional.Options.None< T >", "struct_common_1_1_functional_1_1_options_1_1_none.html", null ],
      [ "Common.Functional.Options.Some< T >", "struct_common_1_1_functional_1_1_options_1_1_some.html", null ]
    ] ],
    [ "Common.Collections.Pair< TFirst, TSecond >", "struct_common_1_1_collections_1_1_pair.html", null ],
    [ "Common.Time.Timer", "class_common_1_1_time_1_1_timer.html", null ]
];