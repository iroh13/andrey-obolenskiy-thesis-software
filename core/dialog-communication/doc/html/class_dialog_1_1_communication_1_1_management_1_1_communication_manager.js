var class_dialog_1_1_communication_1_1_management_1_1_communication_manager =
[
    [ "Builder", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_builder.html", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_builder" ],
    [ "DataSubmission", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_data_submission.html", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_data_submission" ],
    [ "DataSubscription", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_data_subscription.html", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_data_subscription" ],
    [ "Disengage", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager.html#a6f96ed3c830b00b3f16a545f88d71a22", null ],
    [ "Engage", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager.html#a25c0bc43d97bfb56c97e858440258e26", null ],
    [ "ToString", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager.html#a66c23f6a181fe0a14cf5455d61303b5c", null ],
    [ "Update", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager.html#afecc90d19768c7eb4f66317ca3b52a76", null ],
    [ "ActiveDataSubmission", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager.html#aa90618b5f7b554afd7749b35a712f7cd", null ],
    [ "ActiveDataSubscriptions", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager.html#aba2f135cb89a63e54fdae34be840097d", null ],
    [ "Actors", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager.html#a18e6c9b8e6fc5f009d2db818a87b3627", null ],
    [ "DataTypes", "class_dialog_1_1_communication_1_1_management_1_1_communication_manager.html#af6a800d3b3d39eb12c1ba5be284269c9", null ]
];