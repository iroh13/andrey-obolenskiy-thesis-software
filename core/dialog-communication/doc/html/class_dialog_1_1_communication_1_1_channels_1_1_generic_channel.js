var class_dialog_1_1_communication_1_1_channels_1_1_generic_channel =
[
    [ "GenericChannel", "class_dialog_1_1_communication_1_1_channels_1_1_generic_channel.html#afd25660f4ee9f5d3ce0175c53aaee45c", null ],
    [ "Clear", "class_dialog_1_1_communication_1_1_channels_1_1_generic_channel.html#a13964278d6416a621bfeaef28b9c87c9", null ],
    [ "Post", "class_dialog_1_1_communication_1_1_channels_1_1_generic_channel.html#a619897f394094b06a3d95ede1e2db3ef", null ],
    [ "ToString", "class_dialog_1_1_communication_1_1_channels_1_1_generic_channel.html#af4ad98933e13ad16bd2db9bc71e66968", null ],
    [ "DataType", "class_dialog_1_1_communication_1_1_channels_1_1_generic_channel.html#aff219b18c46bf797623bbc6c48f4c747", null ],
    [ "Packets", "class_dialog_1_1_communication_1_1_channels_1_1_generic_channel.html#a7e6da681cfd3709e0805a4875c6ac429", null ]
];