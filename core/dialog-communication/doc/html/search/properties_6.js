var searchData=
[
  ['isactive',['IsActive',['../class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_data_submission.html#ae13a9101815362b2e0dca9d8f7f44eb2',1,'Dialog.Communication.Management.CommunicationManager.DataSubmission.IsActive()'],['../class_dialog_1_1_communication_1_1_management_1_1_communication_manager_1_1_data_subscription.html#a48cf9e33d41efd7ba4667bf4d0e8b6e1',1,'Dialog.Communication.Management.CommunicationManager.DataSubscription.IsActive()']]],
  ['isbuilt',['IsBuilt',['../class_common_1_1_design_patterns_1_1_object_builder.html#a435d7c07319936cf4dbdb40ad1b325db',1,'Common::DesignPatterns::ObjectBuilder']]],
  ['isempty',['IsEmpty',['../class_common_1_1_collections_1_1_views_1_1_collection_view.html#a51e673d8942b32117622abfebeb277a5',1,'Common::Collections::Views::CollectionView']]],
  ['isringing',['IsRinging',['../class_common_1_1_time_1_1_timer.html#a38d7f43886b6a1f793647aca7a85a54b',1,'Common::Time::Timer']]],
  ['isticking',['IsTicking',['../class_common_1_1_time_1_1_timer.html#a67519aa11856df2339463fd1044d4574',1,'Common::Time::Timer']]]
];
