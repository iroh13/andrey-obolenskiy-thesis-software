﻿using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Modules.Deliberation;
using Dialog.Agency.Modules.Perception;
using Dialog.SPB.State;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dialog.SPB.Modules
{
    /// <summary>
    /// This class is an implementation of the 
    /// <see cref="ActionSelectionModule"/> that uses social practices to 
    /// derive action.
    /// </summary>
    public sealed class SPBASModule
        : ActionSelectionModule
    {
        /// <summary>
        /// Gets the collection of data types required by this module to be 
        /// available through the information state.
        /// </summary>
        /// <param name="types">The collection of types.</param>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public override 
            void GetRequiredStateComponents(ICollection<Type> types)
        {
            types.Add(typeof(SocialContext));
        }

        /// <summary>
        /// Selects a target dialog move for the system to perform.
        /// </summary>
        /// <returns>
        /// A move derived from the active social practice program.
        /// </returns>
        public override DialogMove SelectMove()
        {
            var context = State.Get<SocialContext>();

            if (context.Program.IsResolved) { return IdleMove.Instance; }

            IEnumerable<DialogMoveRealizationEvent> self_expectations =
                context.Program.FindExpectedEvents
                (
                    @event => @event.Source == context.Self
                )
                .ToList();
            return
                self_expectations.Count() == 0 ?
                IdleMove.Instance :
                self_expectations.First().Move;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(SPBASModule)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
