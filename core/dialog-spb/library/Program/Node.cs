﻿using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.Functional.Delegates;
using Common.Functional.Options;
using Common.Functional.Options.Extensions;
using Common.Validation;
using System.Collections.Generic;
using System.Linq;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

namespace Dialog.SPB.Program
{
    /// <summary>
    /// This class represents a node of a social practice program.
    /// </summary>
    public abstract class Node
    {
        /// <summary>
        /// Occurs when the node's expectation status is assigned a value 
        /// different from <see cref="ExpectationStatus.Pending"/>.
        /// </summary>
        public event EventHandler<Node> Resolved = delegate { };
        /// <summary>
        /// Occurs when the node's expectation status is assigned a value of
        /// <see cref="ExpectationStatus.Failure"/>.
        /// </summary>
        public event EventHandler<Node> Failed = delegate { };
        /// <summary>
        /// Occurs when the node's expectation status is assigned a value of
        /// <see cref="ExpectationStatus.Satisfaction"/>.
        /// </summary>
        public event EventHandler<Node> Satisfied = delegate { };
        
        /// <summary>
        /// Creates a new node with the specified title and children.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="children">The node's children.</param>
        /// <remarks>
        /// <paramref name="title"/> must not be blank.
        /// <paramref name="children"/> must not be null.
        /// </remarks>
        protected Node(string title, IEnumerable<Node> children)
        {
            Require.IsNotBlank(title);
            Require.IsNotNull(children);

            Title = title;
            Children = new ListView<Node>(children.ToList());
        }

        /// <summary>
        /// Gets this node's title.
        /// </summary>
        public string Title { get; private set; }
        /// <summary>
        /// Gets this node's children.
        /// </summary>
        public ImmutableList<Node> Children { get; private set; }

        /// <summary>
        /// Gets the index of this node's active scope child (if it has any).
        /// </summary>
        public virtual Option<int> ActiveScopeChildIndex
            { get; protected set; } = Option.CreateNone<int>();
        
        /// <summary>
        /// Gets this node's expectation status.
        /// </summary>
        public ExpectationStatus ExpectationStatus
            { get; private set; } = ExpectationStatus.Pending;

        /// <summary>
        /// Indicates whether this node's expectations have been resolved in 
        /// one way or another.
        /// </summary>
        public bool IsResolved
        {
            get { return ExpectationStatus != ExpectationStatus.Pending; }
        }

        /// <summary>
        /// Resets this node's expectations to their initial state 
        /// (i.e. pending).
        /// </summary>
        /// <param name="do_recurse">
        /// Indicates whether this node's children should be recursively reset 
        /// as well.
        /// </param>
        public void Reset(bool do_recurse = true)
        {
            ExpectationStatus = ExpectationStatus.Pending;

            if (do_recurse)
            {
                foreach (var child in Children) { child.Reset(); }
            }

            OnReset();
        }
        /// <summary>
        /// Called by <see cref="Reset"/> to reset this node's expectations
        /// back to their initial state.
        /// </summary>
        protected virtual void OnReset() { }

        /// <summary>
        /// Sets this node's expectation status to 
        /// <see cref="ExpectationStatus.Failure"/> and invokes related events.
        /// </summary>
        public void Fail()
        {
            ExpectationStatus = ExpectationStatus.Failure;
            Resolved.Invoke(this);
            Failed.Invoke(this);
        }
        /// <summary>
        /// Sets this node's expectation status to
        /// <see cref="ExpectationStatus.Satisfaction"/> and invokes related 
        /// events.
        /// </summary>
        public void Satisfy()
        {
            ExpectationStatus = ExpectationStatus.Satisfaction;
            Resolved.Invoke(this);
            Satisfied.Invoke(this);
        }

        /// <summary>
        /// Processes the specified dialog move realization event, and updates 
        /// this node's expectations accordingly.
        /// </summary>
        /// <param name="event">The event to process.</param>
        /// <returns>
        /// This node's expectation status.
        /// </returns>
        public ExpectationStatus Witness(MoveEvent @event)
        {
            if (IsResolved) { return ExpectationStatus; }
            else
            {
                switch (OnWitness(@event))
                {
                    case ExpectationStatus.Failure: Fail(); break;
                    case ExpectationStatus.Satisfaction: Satisfy(); break;
                }
                return ExpectationStatus;
            }
        }
        /// <summary>
        /// Called by <see cref="Witness(MoveEvent)"/> to 
        /// update this node's expectations with regard to the witnessed dialog 
        /// move realization event.
        /// </summary>
        /// <param name="event">The event to process.</param>
        /// <returns>
        /// <see cref="ExpectationStatus.Satisfaction"/> if witnessing of this event 
        /// has caused all expectations of this node to be satisfied,
        /// <see cref="ExpectationStatus.Failure"/> if satisfaction of those
        /// expectation is no longer possible, or 
        /// <see cref="ExpectationStatus.Pending"/> otherwise.
        /// </returns>
        protected abstract ExpectationStatus OnWitness(MoveEvent @event);

        /// <summary>
        /// Finds and returns this node's expected dialog move realization 
        /// events that are indicated by the specified function.
        /// </summary>
        /// <param name="indicator">The event indicator function.</param>
        /// <returns>
        /// An enumeration of events indicated by the specified function.
        /// </returns>
        public IEnumerable<MoveEvent> 
            FindExpectedEvents(Indicator<MoveEvent> indicator)
        {
            var result = new List<MoveEvent>();

            FindExpectedEvents(indicator, result);

            return result;
        }
        /// <summary>
        /// Finds and adds to the specified collection this node's expected 
        /// dialog move realization events that are indicated by the specified 
        /// function.
        /// </summary>
        /// <param name="indicator">The event indicator function.</param>
        /// <param name="result">The collection to add events to.</param>
        public abstract void FindExpectedEvents
        (
            Indicator<MoveEvent> indicator,
            ICollection<MoveEvent> result
        );

        /// <summary>
        /// Gets this node's active scope-descendents.
        /// </summary>
        /// <returns>
        /// An enumeration of nodes, ordered from shallow to deep, starting 
        /// with this node itself.
        /// </returns>
        public IEnumerable<Node> GetActiveScopeDescendents()
        {
            var result = new List<Node>();

            GetActiveScopeDescendents(result);

            return result;
        }
        /// <summary>
        /// Adds this node's active scope-descendents to the specified 
        /// collection.
        /// </summary>
        /// <param name="result">The collection to add nodes to.</param>
        /// <remarks>
        /// <paramref name="result"/> must not be null.
        /// </remarks>
        public void GetActiveScopeDescendents(ICollection<Node> result)
        {
            Require.IsNotNull(result);

            ActiveScopeChildIndex.Match
            (
                none: () => result.Add(this),
                some: index =>
                {
                    result.Add(this);
                    Children[index].GetActiveScopeDescendents(result);
                }
            );
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Node)}(" +
                   $"{nameof(Title)} = '{Title}', " +
                   $"{nameof(ExpectationStatus)} = {ExpectationStatus})";
        }
    }
}
