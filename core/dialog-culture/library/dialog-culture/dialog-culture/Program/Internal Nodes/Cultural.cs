using System.Collections.Generic;
using Common.Functional.Delegates;
using Common.Validation;
using Dialog.Agency.Modules.Perception;
using Dialog.SPB.Program;

namespace CulturalModel.CulturalProject
{
    /// <summary>
    /// This class represents an internal node expecting to satisfy an
    /// expectation only while a specified cultural condition holds.
    /// </summary>
    public sealed class Cultural
        : Node
    {
        /// <summary>
        /// Creates a new cultural node with the specified title, cultural norm,
        /// and body.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="norm">The node's cultural norm.</param>
        /// <param name="body">The node's body.</param>
        /// <remarks>
        /// <paramref name="title"/> must not be blank.
        /// <paramref name="norm"/> must not be null.
        /// <paramref name="body"/> must not be null.
        /// </remarks>
        public Cultural(string title, ICulturalNorms norm, Node body, Node bodyAlternative = null)
            : base(title, bodyAlternative == null ? new Node[1] { body }
                : new Node[2] { body, bodyAlternative})
        {
            Require.IsNotBlank(title);
            Require.IsNotNull(norm);
            Require.IsNotNull(body);

            Norm = norm;
            Body = body;
            BodyAlternative = bodyAlternative;

            Reset();
        }

        /// <summary>
        /// Gets this node's cultural norm.
        /// </summary>
        /// <returns></returns>
        public ICulturalNorms Norm { get; private set; }

        /// <summary>
        /// Gets this node's body.
        /// </summary>
        public Node Body { get; private set; }

        /// <summary>
        /// Gets this node's alternative body.
        /// </summary>
        public Node BodyAlternative { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event">The event to process.</param>
        /// <returns></returns>
        protected override ExpectationStatus OnWitness(DialogMoveRealizationEvent @event)
        {
            if (!Norm.IsMovePossible())
            {
                if (BodyAlternative == null)
                {
                    return ExpectationStatus.Satisfaction;
                }
  
                return BodyAlternative.Witness(@event);
            }

            return Body.Witness(@event);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="indicator"></param>
        /// <param name="result"></param>
        public override void FindExpectedEvents
        (
            Indicator<DialogMoveRealizationEvent> indicator,
            ICollection<DialogMoveRealizationEvent> result
        )
        {
            Require.IsFalse(IsResolved);
            Require.IsNotNull(indicator);
            Require.IsNotNull(result);

            if (Norm.IsMovePossible())
            {
                Body.FindExpectedEvents(indicator, result);
            } else if (BodyAlternative != null)
            {
                BodyAlternative.FindExpectedEvents(indicator, result);
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Cultural)}(" +
                   $"{nameof(Title)} = '{Title}', " +
                   $"{nameof(Body)} = {Body})";
        }
    }
}
