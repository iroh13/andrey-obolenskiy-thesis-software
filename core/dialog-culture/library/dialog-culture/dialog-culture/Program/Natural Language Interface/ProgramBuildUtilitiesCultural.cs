﻿using CulturalModel.CulturalProject;
using Dialog.SPB.Program;

namespace dialog_culture.Program.Natural_Language_Interface
{
    /// <summary>
    /// This class contains utility methods to help make program composition
    /// easier via a natural language interface, but for cultural nodes only.
    /// </summary>
    public static class ProgramBuildUtilitiesCultural
    {
        /// <summary>
        /// Creates a cultual norm expectation node.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="norm">The node's cultural norm.</param>
        /// <param name="body">The node's body.</param>
        /// <returns>A repeating expectation.</returns>
        public static Cultural ExpectCultural(
            ICulturalNorms norm,
            Node body,
            Node bodyAlternative = null,
            string title = "untitled"
        )
        {
            return new Cultural(title, norm, body, bodyAlternative);
        }

        /// <summary>
        /// Creates a sequence expectation node.
        /// </summary>
        /// <param name="title">The node's title.</param>
        /// <param name="nodes">The node's children.</param>
        /// <returns>A sequential expectation.</returns>
        public static SkippingSequence ExpectSkippingSequence
        (
            string title,
            params Node[] nodes
        )
        {
            return new SkippingSequence(title, nodes);
        }
    }
}
