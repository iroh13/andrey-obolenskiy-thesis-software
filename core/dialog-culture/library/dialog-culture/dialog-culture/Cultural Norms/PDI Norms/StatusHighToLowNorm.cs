using System;
using System.Collections.Generic;
using System.Text;

namespace CulturalModel.CulturalProject
{
    public class StatusHighToLowNorm : ICulturalNorms
    {
        /// <summary>
        /// Initialization of source actor, target actor, and power distance
        /// dimension score limit value to check whether power status matters
        /// or not.
        /// </summary>
        private readonly ICulturalActor source;
        private readonly ICulturalActor target;
        private readonly int pdiLimit;

        /// <summary>
        /// Status norm constructor.
        /// </summary>
        /// <param name="source">The move's source actor.</param>
        /// <param name="target">The move's target actor.Target actor.</param>
        /// <param name="pdiLimit">Power distance score limit value.</param>
        public StatusHighToLowNorm(ICulturalActor source, ICulturalActor target, 
            int pdiLimit = 50)
        {
            this.source = source;
            this.target = target;
            this.pdiLimit = pdiLimit;
        }

        /// <summary>
        /// Verifies whether this dialogue move can be performed based on
        /// source actor culture's pdi, and whether the status of source actor
        /// is higher than the status of target actor.
        /// </summary>
        /// <returns>A move realization expectation.</returns>
        public bool IsMovePossible()
        {
            if (source.ActorCulture.pdi >= pdiLimit)
            {
                if (source.ActorCulturalParams.status >= target.ActorCulturalParams.status)
                {
                    return true;
                }
            }
            
            return false;
        }
    }
}
