using System;
using System.Collections.Generic;
using System.Text;

namespace CulturalModel.CulturalProject
{
    public class SameGenderNorm : ICulturalNorms
    {
        /// <summary>
        /// Initialization of source actor, target actor, and masculinity
        /// dimension score limit value to check whether group matters or not.
        /// </summary>
        private readonly ICulturalActor source;
        private readonly ICulturalActor target;
        private readonly int masLimit;

        /// <summary>
        /// Group norm constructor.
        /// </summary>
        /// <param name="source">The move's source actor.</param>
        /// <param name="target">The move's target actor.Target actor.</param>
        /// <param name="masLimit">Masculinity score limit value.</param>
        public SameGenderNorm(ICulturalActor source, ICulturalActor target,
            int masLimit = 50)
        {
            this.source = source;
            this.target = target;
            this.masLimit = masLimit;
        }

        /// <summary>
        /// Verifies whether this dialogue move can be performed based on
        /// source actor culture's mas, and whether source and target actors
        /// have the same gender.
        /// </summary>
        /// <returns>A move realization expectation.</returns>
        public bool IsMovePossible()
        {
            if (source.ActorCulture.mas >= masLimit) return false;
            if (source.ActorCulturalParams.gender_id == target.ActorCulturalParams.gender_id)
            {
                return true;
            }

            return false;
        }
    }
}