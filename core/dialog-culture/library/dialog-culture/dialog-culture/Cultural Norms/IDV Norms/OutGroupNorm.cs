﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CulturalModel.CulturalProject
{
    public class OutGroupNorm : ICulturalNorms
    {
        /// <summary>
        /// Initialization of source actor, target actor, and individualism
        /// dimension score limit value to check whether group matters or not.
        /// </summary>
        private readonly ICulturalActor source;
        private readonly ICulturalActor target;
        private readonly int idvLimit;

        /// <summary>
        /// Group norm constructor.
        /// </summary>
        /// <param name="source">The move's source actor.</param>
        /// <param name="target">The move's target actor.Target actor.</param>
        /// <param name="idvLimit">Individualism score limit value.</param>
        public OutGroupNorm(ICulturalActor source, ICulturalActor target,
            int idvLimit = 50)
        {
            this.source = source;
            this.target = target;
            this.idvLimit = idvLimit;
        }

        /// <summary>
        /// Verifies whether this dialogue move can be performed based on
        /// source actor culture's idv, and whether source and target actors
        /// are from different groups.
        /// </summary>
        /// <returns>A move realization expectation.</returns>
        public bool IsMovePossible()
        {
            if (source.ActorCulture.idv < idvLimit)
            {
                if (source.ActorCulturalParams.group_id != target.ActorCulturalParams.group_id)
                {
                    return true;
                }
            }
            
            return false;
        }
    }
}
