﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CulturalModel.CulturalProject
{
    public class LongTermOrientationNorm : ICulturalNorms
    {
        /// <summary>
        /// Initialization of source actor, target actor, and indulgence
        /// dimension score limit value to check whether Nonchalance matters or
        /// not.
        /// </summary>
        private readonly ICulturalActor source;
        private readonly ICulturalActor target;
        private readonly int ltoLimit;

        /// <summary>
        /// Nonchalance norm constructor.
        /// </summary>
        /// <param name="source">The move's source actor.</param>
        /// <param name="target">The move's target actor.Target actor.</param>
        /// <param name="ltoLimit">Long Term Orientation score limit value.</param>
        public LongTermOrientationNorm(ICulturalActor source, ICulturalActor target,
            int ltoLimit = 50)
        {
            this.source = source;
            this.target = target;
            this.ltoLimit = ltoLimit;
        }

        /// <summary>
        /// Verifies whether this dialogue move can be performed based on
        /// source actor culture's ivr, and whether .
        /// </summary>
        /// <returns>A move realization expectation.</returns>
        public bool IsMovePossible()
        {
            if (source.ActorCulture.lto >= ltoLimit) return true;

            return false;
        }
    }
}