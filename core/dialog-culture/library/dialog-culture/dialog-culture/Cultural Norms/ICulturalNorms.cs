using System;
using System.Collections.Generic;
using System.Text;

namespace CulturalModel.CulturalProject
{
    /// <summary>
    /// An interface for all cultural norms. Verifies whether the dialogue
    /// move can be perfomed based on specific conditions
    /// </summary>
    public interface ICulturalNorms
    {
        bool IsMovePossible();
    }
}
