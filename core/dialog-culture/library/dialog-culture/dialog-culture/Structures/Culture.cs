using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using Common.Validation;

namespace CulturalModel.CulturalProject
{
    public struct Culture
    {
        public readonly int pdi;
        public readonly int idv;
        public readonly int mas;
        public readonly int uai;
        public readonly int lto;
        public readonly int ivr;

        /// <summary>
        /// This constructor gets cultural dimensions scores if such are
        /// specified in the actor.
        /// </summary>
        /// <param name="pdi">Power Distance index score</param>
        /// <param name="idv">Individualism index score</param>
        /// <param name="mas">Masculinity index score</param>
        /// <param name="uai">Uncertainty Avoidance index score</param>
        /// <param name="lto">Long Term Orientation index score</param>
        /// <param name="ivr">Indulgence index score</param>
        public Culture(int pdi, int idv, int mas, int uai, int lto, int ivr)
        {
            this.pdi = pdi;
            this.idv = idv;
            this.mas = mas;
            this.uai = uai;
            this.lto = lto;
            this.ivr = ivr;
        }

        /// <summary>
        /// This constructor gets cultural dimensions scores from the xml file
        /// from the specified directory.
        /// </summary>
        /// <param name="culturesDBFilePath">Path to the XML file</param>
        /// <param name="cultureName">Name of the culture</param>
        public Culture(string culturesDBFilePath, string cultureName)
        {
            Require.IsTrue(File.Exists(culturesDBFilePath));

            var database = XElement.Parse(File.ReadAllText(culturesDBFilePath));
            var cultureNode = database.XPathSelectElement($"./culture[@name='{cultureName}']");
            this.pdi = int.Parse(cultureNode.XPathSelectElement("./pdi").Value);
            this.idv = int.Parse(cultureNode.XPathSelectElement("./idv").Value);
            this.mas = int.Parse(cultureNode.XPathSelectElement("./mas").Value);
            this.uai = int.Parse(cultureNode.XPathSelectElement("./uai").Value);
            this.lto = int.Parse(cultureNode.XPathSelectElement("./lto").Value);
            this.ivr = int.Parse(cultureNode.XPathSelectElement("./ivr").Value);

        }
    }
}