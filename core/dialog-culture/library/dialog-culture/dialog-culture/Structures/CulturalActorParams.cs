using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using Common.Validation;

namespace CulturalModel.CulturalProject
{
    public struct CulturalActorParams
    {
        public readonly string name;
        public readonly string cultureName;
        public readonly int status;
        public readonly int group_id;
        public readonly int gender_id;

        /// <summary>
        /// This constructor gets cultural parameters for a specific cultural
        /// actor if such are specified.
        /// </summary>
        /// <param name="status">Power Status, refers to PDI</param>
        /// <param name="group_id">Group ID, refers to IDV</param>
        /// <param name="gender_id">Gender ID, refers to MAS</param>
        public CulturalActorParams(string name, string cultureName, int status,
            int group_id, int gender_id)
        {
            this.name = name;
            this.cultureName = cultureName;
            this.status = status;
            this.group_id = group_id;
            this.gender_id = gender_id;
        }

        /// <summary>
        /// This constructor gets cultural parameters for a specific cultural
        /// actor from the xml file from the specified directory.
        /// </summary>
        /// <param name="culturalActorParamsDBFilePath">Path to the XML file</param>
        /// <param name="culturalActorName">Cultural Actor's name</param>
        public CulturalActorParams(string culturalActorParamsDBFilePath,
            string culturalActorName)
        {
            Require.IsTrue(File.Exists(culturalActorParamsDBFilePath));

            var database = XElement.Parse(File.ReadAllText(culturalActorParamsDBFilePath));
            var cultureNode = database.XPathSelectElement($"./actor[@name='{culturalActorName}']");
            this.name = culturalActorName;
            this.cultureName = cultureNode.XPathSelectElement("./culture").Value;
            this.status = int.Parse(cultureNode.XPathSelectElement("./status").Value);
            this.group_id = int.Parse(cultureNode.XPathSelectElement("./group_id").Value);
            this.gender_id = int.Parse(cultureNode.XPathSelectElement("./gender_id").Value);

        }
    }
}
