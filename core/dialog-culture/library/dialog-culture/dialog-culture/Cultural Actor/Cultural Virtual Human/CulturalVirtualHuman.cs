using System;
using System.IO;
using Dialog.Communication.Management;
using Common.Functional.Options;
using Common.Functional.Options.Extensions;

namespace CulturalModel.CulturalProject
{
    public class CulturalVirtualHuman : ICulturalActor
    {
        public Culture ActorCulture { get; private set; }
        public CulturalActorParams ActorCulturalParams { get; private set; }

        /// <summary>
        /// Gets the actor's name.
        /// </summary>
        public string Name => ActorCulturalParams.name;

        /// <summary>
        /// Gets/sets the action handler for this actor.
        /// </summary>
        public Option<Action<CommunicationManager.DataSubmission>> Action
        {
            get;
            set;
        }

        /// <summary>
        /// CulturalVirtualHuman constructor.
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="culturalParams"></param>
        public CulturalVirtualHuman(Culture culture, CulturalActorParams culturalParams)
        {
            ActorCulture = culture;
            ActorCulturalParams = culturalParams;
        }

        /// <summary>
        /// Forwards action handling to the delegate specified by 
        /// <see cref="Action"/>, if there is any.
        /// </summary>
        /// <param name="submission">The data submission to use.</param>
        public void Act(CommunicationManager.DataSubmission submission)
        {
            Action.IfIsSome(@delegate => @delegate.Invoke(submission));
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(CulturalVirtualHuman)}(" +
                   $"{nameof(Name)} = '{Name}')";
        }
    }
}
