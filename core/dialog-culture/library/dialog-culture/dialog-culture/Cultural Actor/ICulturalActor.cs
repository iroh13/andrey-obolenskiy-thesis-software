using Dialog.Agency.Scene;
using Dialog.Communication.Actors;

namespace CulturalModel.CulturalProject
{
    /// <summary>
    /// This interface represents an actor with cultural parameters that is
    /// managed by the communication management system.
    /// </summary>
    public interface ICulturalActor : ManagedActor, Actor
    {
        /// <summary>
        /// This method takes cultural dimensions scores from the structure.
        /// </summary>
        /// <returns>Cultural dimensions scores</returns>
        Culture ActorCulture
        {
            get;
        }

        /// <summary>
        /// This method takes cultural actor parameters from the structure.
        /// </summary>
        /// <returns>Cultural Actor parameters</returns>
        CulturalActorParams ActorCulturalParams
        {
            get;
        }
    }
}
