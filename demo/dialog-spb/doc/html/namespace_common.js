var namespace_common =
[
    [ "Collections", "namespace_common_1_1_collections.html", "namespace_common_1_1_collections" ],
    [ "DesignPatterns", "namespace_common_1_1_design_patterns.html", "namespace_common_1_1_design_patterns" ],
    [ "Functional", "namespace_common_1_1_functional.html", "namespace_common_1_1_functional" ],
    [ "Randomization", "namespace_common_1_1_randomization.html", "namespace_common_1_1_randomization" ],
    [ "Time", "namespace_common_1_1_time.html", "namespace_common_1_1_time" ]
];