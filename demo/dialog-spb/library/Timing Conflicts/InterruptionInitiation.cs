﻿namespace Dialog.SPB.Timing_Conflicts
{
    /// <summary>
    /// Enumerates interruption resolution strategies.
    /// </summary>
    public enum InterruptionInitiation : int
    {
        /// <summary>
        /// Don't initiate an interruption.
        /// </summary>
        Dont = 0,
        /// <summary>
        /// Do initiate an interruption.
        /// </summary>
        Do = 1
    }
}
