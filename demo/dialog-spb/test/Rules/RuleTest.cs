﻿using Common.Functional.Delegates;
using Dialog.Agency.Scene;
using Moq;
using NUnit.Framework;
using System;

namespace Dialog.SPB.Rules.Test
{
    [TestFixture]
    public sealed class RuleTest
    {
        private static readonly Predicate 
            PRECONDITION = Predicates.Always;
        private static readonly Indicator<Actor> 
            ACTOR_INDICATOR = Indicators<Actor>.Any;
        private static readonly int 
            IMPLICATION = 1;
        private static readonly float
            WEIGHT = 1.0f;

        [Test]
        public void Test_Constructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => new Rule<int>
                (
                    null,
                    ACTOR_INDICATOR,
                    IMPLICATION,
                    WEIGHT
                )
            );
            Assert.Throws<ArgumentNullException>
            (
                () => new Rule<int>
                (
                    PRECONDITION,
                    null,
                    IMPLICATION,
                    WEIGHT
                )
            );
            Assert.Throws<ArgumentException>
            (
                () => new Rule<int>
                (
                    PRECONDITION,
                    ACTOR_INDICATOR,
                    IMPLICATION,
                    -1.0f
                )
            );
        }
        [Test]
        public void Test_Constructor()
        {
            var rule = new Rule<int>
            (
                PRECONDITION,
                ACTOR_INDICATOR,
                IMPLICATION,
                WEIGHT
            );

            Assert.AreEqual(IMPLICATION, rule.Implication);
            Assert.AreEqual(WEIGHT, rule.Weight);
        }

        [Test]
        public void Test_StaticConstructor_WithInvalidArgs()
        {
            Assert.Throws<ArgumentNullException>
            (
                () => Rule.Create
                (
                    null,
                    ACTOR_INDICATOR,
                    IMPLICATION,
                    WEIGHT
                )
            );
            Assert.Throws<ArgumentNullException>
            (
                () => Rule.Create
                (
                    PRECONDITION,
                    null,
                    IMPLICATION,
                    WEIGHT
                )
            );
            Assert.Throws<ArgumentException>
            (
                () => Rule.Create
                (
                    PRECONDITION,
                    ACTOR_INDICATOR,
                    IMPLICATION,
                    -1.0f
                )
            );
        }
        [Test]
        public void Test_StaticConstructor()
        {
            var rule = Rule.Create
            (
                PRECONDITION,
                ACTOR_INDICATOR,
                IMPLICATION,
                WEIGHT
            );

            Assert.AreEqual(IMPLICATION, rule.Implication);
            Assert.AreEqual(WEIGHT, rule.Weight);
        }

        [Test]
        public void Test_PreconditionCheck()
        {
            bool delegate_invoked = false;

            var rule = new Rule<int>
            (
                () => { return delegate_invoked = true; },
                ACTOR_INDICATOR,
                IMPLICATION
            );

            Assert.IsTrue(rule.IsRelevant());
            Assert.IsTrue(delegate_invoked);
        }

        [Test]
        public void Test_AffectedActorCheck()
        {
            var mock_actor = new Mock<Actor>();
            bool delegate_invoked = false;

            var rule = new Rule<int>
            (
                PRECONDITION,
                (actor) => 
                {
                    Assert.AreEqual(mock_actor.Object, actor);
                    return delegate_invoked = true;
                },
                IMPLICATION
            );

            Assert.IsTrue(rule.IsAffecting(mock_actor.Object));
            Assert.IsTrue(delegate_invoked);
        }

        [Test]
        public void Test_Equality()
        {
            var original = new Rule<int>
            (
                PRECONDITION, 
                ACTOR_INDICATOR,
                IMPLICATION, 
                WEIGHT
            );
            var good_copy = new Rule<int>
            (
                PRECONDITION,
                ACTOR_INDICATOR,
                original.Implication,
                original.Weight
            );
            var flawed_precondition_copy = new Rule<int>
            (
                Predicates.Never,
                ACTOR_INDICATOR,
                original.Implication,
                original.Weight
            );
            var flawed_actor_indicator_copy = new Rule<int>
            (
                PRECONDITION,
                Indicators<Actor>.None,
                original.Implication,
                original.Weight
            );
            var flawed_implication_copy = new Rule<int>
            (
                PRECONDITION,
                ACTOR_INDICATOR,
                original.Implication + 1,
                original.Weight
            );
            var flawed_weight_copy = new Rule<int>
            (
                PRECONDITION,
                ACTOR_INDICATOR,
                original.Implication,
                original.Weight + 1
            );

            Assert.AreNotEqual(original, null);
            Assert.AreNotEqual(original, "incompatible type");
            Assert.AreNotEqual(original, flawed_precondition_copy);
            Assert.AreNotEqual(original, flawed_actor_indicator_copy);
            Assert.AreNotEqual(original, flawed_implication_copy);
            Assert.AreNotEqual(original, flawed_weight_copy);

            Assert.AreEqual(original, original);
            Assert.AreEqual(original, good_copy);
        }
    }
}
