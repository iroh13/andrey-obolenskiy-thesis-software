﻿using Dialog.Agency.Scene;
using Dialog.Communication.Actors;
using Dialog.Communication.Management;
using Dialog.Communication.Packets;
using doctor_patient_scenario.Agency.Scene;
using doctor_patient_scenario.Simulation.Actor_Controllers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CulturalModel.CulturalProject;
using doctor_patient_scenario.Communication;
using doctor_patient_scenario.Simulation;
using UnityEngine;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

/// <summary>
/// This behavior manages the virtual session.
/// </summary>
public sealed class ScenarioDirectorDoctor
    : SingletonMonoBehaviour<ScenarioDirectorDoctor>
{
    public SpeechBubble therapistBubble;
    public SpeechBubble firstPatientBubble;
    public SpeechBubble secondPatientBubble;

    /// <summary>
    /// Gets the doctor actor controller.
    /// </summary>
    public TherapistController TherapistController
    {
        get { return _scenario.TherapistController; }
    }

    void Awake()
    {
        InitializeSingleton();
        var doctorParams =
            new CulturalActorParams(Application.dataPath + "/DoctorCulturalActorParams.xml", "Doctor");
        var doctorCulture =
            new Culture(Application.dataPath + "/CulturalValues.xml", doctorParams.cultureName);
        var patientAParams =
            new CulturalActorParams(Application.dataPath + "/DoctorCulturalActorParams.xml", "Mark");
        var patientACulture =
            new Culture(Application.dataPath + "/CulturalValues.xml", patientAParams.cultureName);
        var patientBParams =
            new CulturalActorParams(Application.dataPath + "/DoctorCulturalActorParams.xml", "Anna");
        var patientBCulture =
            new Culture(Application.dataPath + "/CulturalValues.xml", patientBParams.cultureName);

        var session = new Session
        (
            new CulturalVirtualHuman(doctorCulture, doctorParams),
            new CulturalVirtualHuman(patientACulture, patientAParams),
            new CulturalVirtualHuman(patientBCulture, patientBParams)
        );

        _scenario = new Scenario(session);

        var eavesdropper = new Eavesdropper();
        var subscription = _scenario.CommunicationManager.Engage(eavesdropper);
        subscription.Include<Speech>(
            (manager, packets) => OnPerceiveSpeech(packets));
        subscription.Include<MoveEvent>(
            (manager, packets) => OnPerceiveMoveEvent(packets));

        _speech_bubble_for_actor.Add(session.PatientA, firstPatientBubble);
        _speech_bubble_for_actor.Add(session.PatientB, secondPatientBubble);
        _speech_bubble_for_actor.Add(session.Therapist, therapistBubble);

        foreach (var actor in session.Actors)
        {
            _expecting_new_phrase_for_actor.Add(actor, true);
        }
    }

    void Start()
    {
        foreach (var bubble in SpeechBubbles)
        {
            bubble.ClearDisplay();
        }
    }

    void Update()
    {
        if (TimeControl.IsPaused) { return; }
        if (_scenario.HasEnded())
        {
            KeyboardInputHandlerDoctor.Instance.IsEnabled = false;

            PauseMenu.Instance.Title = "THE END";
            PauseMenu.Instance.IsResumeButtonActive = false;

            TimeControl.Pause();

            StartCoroutine(OnScenarioEnd());
            
            return;
        }

        _scenario.Step(Time.deltaTime);

        foreach (var speech in _perceived_speech)
        {
            if (speech.Text.Trim() == "") continue;
            var speaker = speech.Speaker;
            SpeechBubble bubble = _speech_bubble_for_actor[speaker];
            
            if (_expecting_new_phrase_for_actor[speaker])
            {
                bubble.ClearDisplay();
                bubble.AddToDisplay(speech.Text);
                
                // We will expect a new phrase once we observe the dialog move
                // event associated with the current one, but for now we have 
                // no such expectation:
                _expecting_new_phrase_for_actor[speaker] = false;
            }
            else
            {
                bubble.AddToDisplay(" " + speech.Text);
            }
        }
        foreach (var @event in _perceived_move_events)
        {
            DevDebug.Log("Dialog move realization event: " + @event);

            _expecting_new_phrase_for_actor[@event.Source] = true;
        }
    }

    /// <summary>
    /// A dummy actor so we can listen in on the conversation.
    /// </summary>
    private sealed class Eavesdropper
        : ManagedActor
    {
        public void Act(CommunicationManager.DataSubmission submission) { }
    }

    private IEnumerable<SpeechBubble> SpeechBubbles
    {
        get { return _speech_bubble_for_actor.Values; }
    }
    
    private void OnPerceiveSpeech(IEnumerable<DataPacket<Speech>> packets)
    {
        _perceived_speech = 
            packets.Select(packet => packet.Payload).ToList();
    }
    private void 
        OnPerceiveMoveEvent(IEnumerable<DataPacket<MoveEvent>> packets)
    {
        _perceived_move_events = 
            packets.Select(packet => packet.Payload).ToList();
    }

    private IEnumerator OnScenarioEnd()
    {
        yield return new WaitForSeconds(1);

        PauseMenu.Instance.Show();
    }

    private Scenario _scenario;
    private IEnumerable<Speech> _perceived_speech;
    private IEnumerable<MoveEvent> _perceived_move_events;

    private readonly 
        Dictionary<Actor, SpeechBubble> 
        _speech_bubble_for_actor =
        new Dictionary<Actor, SpeechBubble>();
    private readonly
        Dictionary<Actor, bool>
        _expecting_new_phrase_for_actor =
        new Dictionary<Actor, bool>();
}
