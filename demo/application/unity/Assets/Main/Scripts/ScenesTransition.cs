﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesTransition : MonoBehaviour {

    public void DoctorSceneTransition()
    {
        
        SceneManager.LoadScene("DoctorScenario");
    }

    public void TeacherSceneTransition()
    {

        SceneManager.LoadScene("TeacherScenario");
    }

    public void Exit()
    {

        Application.Quit();
    }
}
