﻿using Dialog.Agency.Dialog_Moves;

namespace doctor_patient_scenario.Agency.Dialog_Moves
{
    public sealed class ScenarioDialogMoveKind
        : DialogMoveKind
    {
        /// <summary>
        /// Indicates a greeting from one actor to one or more other actors.
        /// </summary>
        public static readonly DialogMoveKind GreetingsDoctor_1 =
            new ScenarioDialogMoveKind("Good Morning!");

        public static readonly DialogMoveKind GreetingsDoctor_2 =
            new ScenarioDialogMoveKind("Hello.");

        public static readonly DialogMoveKind GreetingsPatients_1 =
            new ScenarioDialogMoveKind("Good morning, Doctor!");

        public static readonly DialogMoveKind GreetingsPatients_2 =
            new ScenarioDialogMoveKind("Good morning, Doctor! Thank you!");

        public static readonly DialogMoveKind GreetingsPatients_3 =
            new ScenarioDialogMoveKind("Hello Doctor. Thank you!");

        /// <summary>
        /// Indicates an acquaintance from one actor to one or more other actors.
        /// </summary>
        public static readonly DialogMoveKind AcquaintanceDoctor =
            new ScenarioDialogMoveKind("What is your name?");

        public static readonly DialogMoveKind AcquaintancePatient =
            new ScenarioDialogMoveKind("Name.");

        /// <summary>
        /// Indicates a problem definition from one actor to one or more other
        /// actors.
        /// </summary>
        public static readonly DialogMoveKind ProblemDoctor_1 =
            new ScenarioDialogMoveKind("How do you find yourself?");

        public static readonly DialogMoveKind ProblemDoctor_2 =
            new ScenarioDialogMoveKind("What is the problem?");

        public static readonly DialogMoveKind ProblemPatientA_1 =
            new ScenarioDialogMoveKind("I am worried about my leg, Doctor. It hurts.");

        public static readonly DialogMoveKind ProblemPatientB_1 =
            new ScenarioDialogMoveKind("Oh, everything is fine. I am here to support my boyfriend.");

        public static readonly DialogMoveKind ProblemPatientA_2 =
            new ScenarioDialogMoveKind("My leg hurts, Doctor.");

        public static readonly DialogMoveKind ProblemPatientB_2 =
            new ScenarioDialogMoveKind("I am just an accompanying. Mark's girlfriend.");

        /// <summary>
        /// Indicates the first subject discussion between actors.
        /// </summary>
        public static readonly DialogMoveKind SubjectDiscussionDoctor_1_1 =
            new ScenarioDialogMoveKind("Let’s collect some basic information about you first.");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_1_2 =
            new ScenarioDialogMoveKind("I need to get some basic information about you.");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_2_1 =
            new ScenarioDialogMoveKind("How old are you?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_2_2 =
            new ScenarioDialogMoveKind("What is your age?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_3 =
            new ScenarioDialogMoveKind("What is your height and weight?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_4_1 =
            new ScenarioDialogMoveKind("How exactly your leg hurts?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_4_2 =
            new ScenarioDialogMoveKind("What is wrong with your leg?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_5_1 =
            new ScenarioDialogMoveKind("For how long do you have this pain?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_5_2 =
            new ScenarioDialogMoveKind("For how long do you have this problem?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_6_1 =
            new ScenarioDialogMoveKind("What happened before the pain started?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_6_2 =
            new ScenarioDialogMoveKind("What happened before the pain started?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_7_1 =
            new ScenarioDialogMoveKind("Is your job related to physical activities?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_7_2 =
            new ScenarioDialogMoveKind("What is your occupation?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_8_1 =
            new ScenarioDialogMoveKind("I think now I see what the problem is with your knee.");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_8_2 =
            new ScenarioDialogMoveKind("Probably I already know what the problem is.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_1_1 =
            new ScenarioDialogMoveKind("Alright.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_1_2 =
            new ScenarioDialogMoveKind("My name is Mark, Doctor");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_2 =
            new ScenarioDialogMoveKind("I am 25.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_3_1 =
            new ScenarioDialogMoveKind("My height is about 180, and my weight… Anna, do you remember?");

        public static readonly DialogMoveKind SubjectDiscussionPatientB_3_1 =
            new ScenarioDialogMoveKind("His weight is almost 80 kilos. Got it quite recently. Completely gave up going to the gym.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_3_2 =
            new ScenarioDialogMoveKind("My height is about 180, and my weight… Hmm… I think it is almost 80 kilos.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_3_3 =
            new ScenarioDialogMoveKind("My height is about 180, and my weigh is almost 80 kilos.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_4_1 =
            new ScenarioDialogMoveKind("It hurts in the knee, especially when I go up the stairs.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_4_2 =
            new ScenarioDialogMoveKind("Yes, of course. Sorry sir. My knee hurts. Especially when I go up the stairs.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_5_1 =
            new ScenarioDialogMoveKind("Almost two weeks, Doctor.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_5_2 =
            new ScenarioDialogMoveKind("Two weeks.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_6_1 =
            new ScenarioDialogMoveKind("Anna and I went for a mountain skiing. I did not fall, did not hit my leg or anything. Just once I made quite a sharp turn around the tree. Then, suddenly something crackled and I started to feel pain in my knee… And… And afterwards I could not go up the stairs. Since that moment it hurts constantly.");

        public static readonly DialogMoveKind SubjectDiscussionPatientB_6_1 =
            new ScenarioDialogMoveKind("Yes, I can confirm. He did not fell or hit anything with his leg. But after that sharp turn his face was full of agony!");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_6_2 =
            new ScenarioDialogMoveKind("Anna and I went for a mountain skiing. I did not fall, did not hit my leg or anything. Just once I made quite a sharp turn around the tree. Then, suddenly something crackled and I started to feel pain in my knee… And… And afterwards I could not go up the stairs. Since that moment it hurts constantly.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_6_3 =
            new ScenarioDialogMoveKind("We went for a mountain skiing, and when I made a sharp turn, I felt that something crackled in my knee. Since that moment it hurts constantly.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_7_1 =
            new ScenarioDialogMoveKind("No, not really. I am an IT specialist in an insurance company. System administrator.");

        public static readonly DialogMoveKind SubjectDiscussionPatientB_7_1 =
            new ScenarioDialogMoveKind("Spends all days in front of the computer. No time for sports.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_7_2 =
            new ScenarioDialogMoveKind("Not really. I am an IT specialist, system administrator.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_7_3 =
            new ScenarioDialogMoveKind("System administrator.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_8_1 =
            new ScenarioDialogMoveKind("Oh, that sounds great!");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_8_2 =
            new ScenarioDialogMoveKind("Thank you, Doctor! Finally some good news…");

        /// <summary>
        /// Indicates first non-subject discussion between actors.
        /// </summary>
        public static readonly DialogMoveKind NonSubjectDiscussionDoctor_1 =
            new ScenarioDialogMoveKind("But tell me first. Where exactly have you been skiing?");

        public static readonly DialogMoveKind NonSubjectDiscussionDoctor_2 =
            new ScenarioDialogMoveKind("It is a very nice place…");

        public static readonly DialogMoveKind NonSubjectDiscussionDoctor_3 =
            new ScenarioDialogMoveKind("Böglerhof restaurant is still in place?");

        public static readonly DialogMoveKind NonSubjectDiscussionDoctor_4 =
            new ScenarioDialogMoveKind("Duftner family still run the place?");

        public static readonly DialogMoveKind NonSubjectDiscussionDoctor_5 =
            new ScenarioDialogMoveKind("Just a flood of warm memories…");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientA_1 =
            new ScenarioDialogMoveKind("We have been to a place called Alpbach, in Austria. It is a really nice ski resort for beginners.");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientB_1 =
            new ScenarioDialogMoveKind("And we are the beginners, it was our second time.");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientB_2 =
            new ScenarioDialogMoveKind("A lovely place indeed. So you have been there?");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientA_3 =
            new ScenarioDialogMoveKind("I guess. We have not visited it unfortunately.");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientB_4 =
            new ScenarioDialogMoveKind("We are really sorry, but we do not know.");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientA_5 =
            new ScenarioDialogMoveKind("We can imagine!");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientB_5 =
            new ScenarioDialogMoveKind("We will try to get there during our next mountain skiing trip… And once my leg is fixed.");

        /// <summary>
        /// Indicates the second subject discussion between actors.
        /// </summary>
        public static readonly DialogMoveKind SubjectDiscussionDoctor_9_1 =
            new ScenarioDialogMoveKind("Now I need a bit more details to be sure. Could you help us?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_9_2 =
            new ScenarioDialogMoveKind("Now I need more details to be sure.");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_10_1 =
            new ScenarioDialogMoveKind("Has Mark ever had problems with his knees in the past?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_10_2 =
            new ScenarioDialogMoveKind("Have you ever had problems with your knees in the past?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_11 =
            new ScenarioDialogMoveKind("Allergies? Intolerance to any medications?");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_12_1 =
            new ScenarioDialogMoveKind("I think your meniscus in a knee joint got injured.");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_12_2 =
            new ScenarioDialogMoveKind("I think your meniscus in a knee joint got injured.");

        public static readonly DialogMoveKind SubjectDiscussionDoctor_13 =
            new ScenarioDialogMoveKind("I recommend you to wear an orthosis on your knee joint.");

        public static readonly DialogMoveKind SubjectDiscussionPatientB_9_1 =
            new ScenarioDialogMoveKind("Sure! I am glad to supply you with any information I can.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_9_2 =
            new ScenarioDialogMoveKind("Of course! Anything you need.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_10_1 =
            new ScenarioDialogMoveKind("I do not remember any knee injuries in the past.");

        public static readonly DialogMoveKind SubjectDiscussionPatientB_10_1 =
            new ScenarioDialogMoveKind("No, I do not think so. At least as far as I am concerned, since we are together.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_10_2 =
            new ScenarioDialogMoveKind("No, as long as I remember, everything was fine, Doctor.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_11_1 =
            new ScenarioDialogMoveKind("Nothing that pops up in my mind.");

        public static readonly DialogMoveKind SubjectDiscussionPatientB_11_1 =
            new ScenarioDialogMoveKind("I am pretty sure he has no allergies or intolerances.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_11_2 =
            new ScenarioDialogMoveKind("No, no allergies and no intolerances.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_12_1 =
            new ScenarioDialogMoveKind("Thank you! You cannot imagine what a relief it is!");

        public static readonly DialogMoveKind SubjectDiscussionPatientB_12_1 =
            new ScenarioDialogMoveKind("We are very grateful!");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_12_2 =
            new ScenarioDialogMoveKind("Thank you. I have already made an appointment for an MRI. It is on next Tuesday.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_12_3 =
            new ScenarioDialogMoveKind("Thank you, Doctor! I will make an appointment for an MRI as soon as possible!");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_13_1 =
            new ScenarioDialogMoveKind("I will follow your recommendations. Thank you.");

        public static readonly DialogMoveKind SubjectDiscussionPatientA_13_2 =
            new ScenarioDialogMoveKind("Again, thank you Sir! I will certainly follow all your recommendations!");

        /// <summary>
        /// Indicates second non-subject discussion between actors.
        /// </summary>
        public static readonly DialogMoveKind NonSubjectDiscussionDoctor_6 =
            new ScenarioDialogMoveKind("Good.");

        public static readonly DialogMoveKind NonSubjectDiscussionDoctor_7 =
            new ScenarioDialogMoveKind("What bothers you?");

        public static readonly DialogMoveKind NonSubjectDiscussionDoctor_8 =
            new ScenarioDialogMoveKind("What kind of hereditary disease is it?");

        public static readonly DialogMoveKind NonSubjectDiscussionDoctor_9 =
            new ScenarioDialogMoveKind("The likelihood to appear is negligible.");

        public static readonly DialogMoveKind NonSubjectDiscussionDoctor_10 =
            new ScenarioDialogMoveKind("Males are more likely to be color blind than females.");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientB_6 =
            new ScenarioDialogMoveKind("Doctor, I wanted to ask you something.");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientB_7 =
            new ScenarioDialogMoveKind("My question is related to hereditary diseases.");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientB_8 =
            new ScenarioDialogMoveKind("My mother has color blindness in a low degree. What are the chances that I will become color blind as well?");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientB_9 =
            new ScenarioDialogMoveKind("Good to know! Thank you.");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientA_10 =
            new ScenarioDialogMoveKind("This X chromosome… But thank you! It is really good to know.");

        public static readonly DialogMoveKind NonSubjectDiscussionPatientB_10 =
            new ScenarioDialogMoveKind("Great!");

        /// <summary>
        /// Indicates goodbyes from one actor to one or more others.
        /// </summary>
        public static readonly DialogMoveKind GoodbyeDoctor_1_1 =
            new ScenarioDialogMoveKind("Come to visit me once you have the MRI results.");

        public static readonly DialogMoveKind GoodbyeDoctor_1_2 =
            new ScenarioDialogMoveKind("See you next time with MRI results.");

        public static readonly DialogMoveKind GoodbyeDoctor_2_1 =
            new ScenarioDialogMoveKind("Goodbye.");

        public static readonly DialogMoveKind GoodbyeDoctor_2_2 =
            new ScenarioDialogMoveKind("Goodbye.");

        public static readonly DialogMoveKind GoodbyePatientA_1_1 =
            new ScenarioDialogMoveKind("Of course!");

        public static readonly DialogMoveKind GoodbyePatientB_1_1 =
            new ScenarioDialogMoveKind("I will make sure he does not forget to make an appointment.");

        public static readonly DialogMoveKind GoodbyePatientA_1_2 =
            new ScenarioDialogMoveKind("Of course, Doctor! I will not forget to bring it with me.");

        public static readonly DialogMoveKind GoodbyePatientA_2_1 =
            new ScenarioDialogMoveKind("Goodbye. Have a good day!");

        public static readonly DialogMoveKind GoodbyePatientB_2_1 =
            new ScenarioDialogMoveKind("Goodbye.");

        public static readonly DialogMoveKind GoodbyePatientA_2_2 =
            new ScenarioDialogMoveKind("Goodbye, Doctor. Thank you for your time!");

        public static readonly DialogMoveKind GoodbyePatientB_2_2 =
            new ScenarioDialogMoveKind("Goodbye, Doctor. Thank you.");

        /// <summary>
        /// Indicates an empty line.
        /// </summary>
        public static readonly DialogMoveKind EmptyLine =
            new ScenarioDialogMoveKind("...");

        /// <summary>
        /// Indicates the closing of the session.
        /// </summary>
        public static readonly DialogMoveKind SessionClosing =
            new ScenarioDialogMoveKind("End the session");

        /// <summary>
        /// Indicates an acknowledgement.
        /// </summary>
        public static readonly DialogMoveKind AcknowledgementFirstPatient =
            new ScenarioDialogMoveKind("Ok.");

        public static readonly DialogMoveKind AcknowledgementSecondPatient =
            new ScenarioDialogMoveKind("Alright");

        /// <summary>
        /// Creates a new dialog move kind.
        /// </summary>
        /// <param name="label">The label associated with this kind.</param>
        /// <remarks>
        /// <paramref name="label"/> must not be blank.
        /// </remarks>
        private ScenarioDialogMoveKind(string label)
            : base(label)
        { }
    }
}
