﻿using Common.Collections.Extensions;
using Common.Functional.Options;
using Common.Functional.Options.Extensions;
using Common.Randomization.Distributions;
using Common.Time;
using Common.Validation;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Dialog_Moves.Extensions;
using Dialog.Agency.Modules.Action;
using Dialog.SPB.State;
using System;
using System.Collections.Generic;
using System.Linq;
using CulturalModel.CulturalProject;
using doctor_patient_scenario.Agency.Dialog_Moves;
using doctor_patient_scenario.Communication;

namespace doctor_patient_scenario.Agency.Modules.Action
{
    /// <summary>
    /// This class is an implementation of the 
    /// <see cref="ActionRealizationModule"/>.
    /// </summary>
    public sealed class ARModule
        : ActionRealizationModule
    {
        /// <summary>
        /// Creates a new module with the specified output rate.
        /// </summary>
        /// <param name="output_rate">
        /// The time it takes to output a single character of text.
        /// </param>
        /// <remarks>
        /// <paramref name="output_rate"/> must be >= 0.
        /// </remarks>
        public ARModule(float output_rate = 0.05f)
        {
            Require.IsAtLeast(output_rate, 0);

            OutputSpeechRate = output_rate;
        }

        /// <summary>
        /// The time it takes to output a single character.
        /// </summary>
        public float OutputSpeechRate { get; private set; }

        /// <summary>
        /// Gets the actor in whose name this module outputs speech.
        /// </summary>
        public CulturalVirtualHuman Owner { get; private set; }
        /// <summary>
        /// Gets this module's time reference point.
        /// </summary>
        public Clock Clock { get; private set; }

        /// <summary>
        /// Gets the move pending realization (if there is any).
        /// </summary>
        public Option<DialogMove<CulturalVirtualHuman>> PendingMove
        {
            get;
            private set;
        }
            = Option.CreateNone<DialogMove<CulturalVirtualHuman>>();

        /// <summary>
        /// Gets this module's output speech, if it has any.
        /// </summary>
        public Option<Speech> OutputSpeech
        {
            get;
            private set;
        }
            = Option.CreateNone<Speech>();

        /// <summary>
        /// Gets this module's output move, if it has any.
        /// </summary>
        public Option<DialogMove<CulturalVirtualHuman>> OutputMove
        {
            get;
            private set;
        }
            = Option.CreateNone<DialogMove<CulturalVirtualHuman>>();

        /// <summary>
        /// Gets the collection of data types required by this module to be 
        /// available through the information state.
        /// </summary>
        /// <param name="types">The collection of types.</param>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public override
            void GetRequiredStateComponents(ICollection<Type> types)
        {
            types.Add(typeof(SocialContext));
            types.Add(typeof(Clock));
        }
        /// <summary>
        /// Sets up this module for operation.
        /// </summary>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public override void Setup()
        {
            base.Setup();

            Owner = (CulturalVirtualHuman)State.Get<SocialContext>().Self;

            Clock = State.Get<Clock>();
            _action_delay_timer = new Timer(Clock);
            _speech_timer = new Timer(Clock);
        }

        /// <summary>
        /// Realizes the specified dialogue move.
        /// </summary>
        /// <param name="move">The dialogue move to realize.</param>
        /// <returns>
        /// Information regarding the status of the realization.
        /// </returns>
        /// <remarks>
        /// <paramref name="move"/> must not be null.
        /// </remarks>
        public override ActionRealizationStatus RealizeMove(DialogMove move)
        {
            Require.IsNotNull(move);

            OutputSpeech = Option.CreateNone<Speech>();
            OutputMove = Option.CreateNone<DialogMove<CulturalVirtualHuman>>();

            if (move.IsIdle())
            {
               PendingMove = Option.CreateNone<DialogMove<CulturalVirtualHuman>>();

                return ActionRealizationStatus.Complete;
            }

            var target_move = move.Cast<CulturalVirtualHuman>();
            if (!PendingMove.Contains(target_move))
            {
                return BeginRealization(target_move);
            }
            else
            {
                return ResumePendingRealization();
            }
        }

        /// <summary>
        /// Gets the word pending realization (only applicable if there is some
        /// move pending realization).
        /// </summary>
        public Option<string> PendingWord
        {
            get
            {
                if (_pending_words.Count > 0)
                {
                    return Option.CreateSome(_pending_words.Peek());
                }
                else
                {
                    return Option.CreateNone<string>();
                }
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ARModule)}(" +
                   $"{nameof(State)} = {State})";
        }

        /// <summary>
        /// Begin a new move realization.
        /// </summary>
        /// <param name="target_move">The move to realize.</param>
        /// <returns>
        /// Information regarding the status of the realization.
        /// </returns>
        private ActionRealizationStatus
            BeginRealization(DialogMove<CulturalVirtualHuman> target_move)
        {
            _pending_words.Clear();
            _action_delay_timer.Reset();

            var text = ExpressThroughText(target_move);
            var words = text.Split(' ');
            foreach (var word in words)
            {
                _pending_words.Enqueue(word);
            }

            PendingMove = Option.CreateSome(target_move);
            SetRandomActionDelay();

            return ActionRealizationStatus.InProgress;
        }
        /// <summary>
        /// Resumes the currently pending move realization.
        /// </summary>
        /// <returns>
        /// Information regarding the status of the realization.
        /// </returns>
        private ActionRealizationStatus ResumePendingRealization()
        {
            if (_action_delay_timer.Update())
            {
                _action_delay_timer.Reset();

                OutputWord();
            }
            else if (_speech_timer.Update())
            {
                _speech_timer.Reset();

                if (PendingWord.IsSome())
                {
                    OutputWord();
                }
                else
                {
                    OutputMove = PendingMove;
                    PendingMove = Option.CreateNone<DialogMove<CulturalVirtualHuman>>();

                    return ActionRealizationStatus.Complete;
                }
            }
            return ActionRealizationStatus.InProgress;
        }
        /// <summary>
        /// Outputs the currently pending word.
        /// </summary>
        private void OutputWord()
        {
            string word = _pending_words.Dequeue();
            var speech = new Speech(speaker: Owner, text: word);
            OutputSpeech = Option.CreateSome(speech);

            float output_duration = OutputSpeechRate * word.Length;
            _speech_timer.Set(output_duration);
            _speech_timer.Start();
        }

        /// <summary>
        /// Produces a string of speech text representing realization of the 
        /// specified dialogue move.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string ExpressThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            if (move.IsIdle()) return "";

            //Greetings 

            if (move.Kind == ScenarioDialogMoveKind.GreetingsDoctor_1)
            {
                return ExpressGreetingsDoctor_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GreetingsDoctor_2)
            {
                return ExpressGreetingsDoctor_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GreetingsPatients_1)
            {
                return ExpressGreetingsPatients_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GreetingsPatients_2)
            {
                return ExpressGreetingsPatients_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GreetingsPatients_3)
            {
                return ExpressGreetingsPatients_3ThroughText(move);
            }

            //Acquaintance

            else if (move.Kind == ScenarioDialogMoveKind.AcquaintanceDoctor)
            {
                return ExpressAcquaintanceDoctorThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.AcquaintancePatient)
            {
                return ExpressAcquaintancePatientThroughText(move);
            }

            //Problem

            else if (move.Kind == ScenarioDialogMoveKind.ProblemDoctor_1)
            {
                return ExpressProblemDoctor_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.ProblemDoctor_2)
            {
                return ExpressProblemDoctor_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.ProblemPatientA_1)
            {
                return ExpressProblemPatientA_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.ProblemPatientB_1)
            {
                return ExpressProblemPatientB_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.ProblemPatientA_2)
            {
                return ExpressProblemPatientA_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.ProblemPatientB_2)
            {
                return ExpressProblemPatientB_2ThroughText(move);
            }

            //First Subject Discussion

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_1_1)
            {
                return ExpressSubjectDiscussionDoctor_1_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_1_2)
            {
                return ExpressSubjectDiscussionDoctor_1_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_2_1)
            {
                return ExpressSubjectDiscussionDoctor_2_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_2_2)
            {
                return ExpressSubjectDiscussionDoctor_2_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_3)
            {
                return ExpressSubjectDiscussionDoctor_3ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_4_1)
            {
                return ExpressSubjectDiscussionDoctor_4_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_4_2)
            {
                return ExpressSubjectDiscussionDoctor_4_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_5_1)
            {
                return ExpressSubjectDiscussionDoctor_5_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_5_2)
            {
                return ExpressSubjectDiscussionDoctor_5_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_6_1)
            {
                return ExpressSubjectDiscussionDoctor_6_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_6_2)
            {
                return ExpressSubjectDiscussionDoctor_6_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_7_1)
            {
                return ExpressSubjectDiscussionDoctor_7_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_7_2)
            {
                return ExpressSubjectDiscussionDoctor_7_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_8_1)
            {
                return ExpressSubjectDiscussionDoctor_8_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_8_2)
            {
                return ExpressSubjectDiscussionDoctor_8_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_1_1)
            {
                return ExpressSubjectDiscussionPatientA_1_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_1_2)
            {
                return ExpressSubjectDiscussionPatientA_1_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_2)
            {
                return ExpressSubjectDiscussionPatientA_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_3_1)
            {
                return ExpressSubjectDiscussionPatientA_3_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientB_3_1)
            {
                return ExpressSubjectDiscussionPatientB_3_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_3_2)
            {
                return ExpressSubjectDiscussionPatientA_3_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_3_3)
            {
                return ExpressSubjectDiscussionPatientA_3_3ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_4_1)
            {
                return ExpressSubjectDiscussionPatientA_4_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_4_2)
            {
                return ExpressSubjectDiscussionPatientA_4_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_5_1)
            {
                return ExpressSubjectDiscussionPatientA_5_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_5_2)
            {
                return ExpressSubjectDiscussionPatientA_5_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_6_1)
            {
                return ExpressSubjectDiscussionPatientA_6_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientB_6_1)
            {
                return ExpressSubjectDiscussionPatientB_6_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_6_2)
            {
                return ExpressSubjectDiscussionPatientA_6_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_6_3)
            {
                return ExpressSubjectDiscussionPatientA_6_3ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_7_1)
            {
                return ExpressSubjectDiscussionPatientA_7_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientB_7_1)
            {
                return ExpressSubjectDiscussionPatientB_7_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_7_2)
            {
                return ExpressSubjectDiscussionPatientA_7_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_7_3)
            {
                return ExpressSubjectDiscussionPatientA_7_3ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_8_1)
            {
                return ExpressSubjectDiscussionPatientA_8_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_8_2)
            {
                return ExpressSubjectDiscussionPatientA_8_2ThroughText(move);
            }

            //First Non-Subject Discussion

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionDoctor_1)
            {
                return ExpressNonSubjectDiscussionDoctor_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionDoctor_2)
            {
                return ExpressNonSubjectDiscussionDoctor_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionDoctor_3)
            {
                return ExpressNonSubjectDiscussionDoctor_3ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionDoctor_4)
            {
                return ExpressNonSubjectDiscussionDoctor_4ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionDoctor_5)
            {
                return ExpressNonSubjectDiscussionDoctor_5ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientA_1)
            {
                return ExpressNonSubjectDiscussionPatientA_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientB_1)
            {
                return ExpressNonSubjectDiscussionPatientB_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientB_2)
            {
                return ExpressNonSubjectDiscussionPatientB_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientA_3)
            {
                return ExpressNonSubjectDiscussionPatientA_3ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientB_4)
            {
                return ExpressNonSubjectDiscussionPatientB_4ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientA_5)
            {
                return ExpressNonSubjectDiscussionPatientA_5ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientB_5)
            {
                return ExpressNonSubjectDiscussionPatientB_5ThroughText(move);
            }

            //Second Subject Discussion

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_9_1)
            {
                return ExpressSubjectDiscussionDoctor_9_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_9_2)
            {
                return ExpressSubjectDiscussionDoctor_9_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_10_1)
            {
                return ExpressSubjectDiscussionDoctor_10_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_10_2)
            {
                return ExpressSubjectDiscussionDoctor_10_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_11)
            {
                return ExpressSubjectDiscussionDoctor_11ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_12_1)
            {
                return ExpressSubjectDiscussionDoctor_12_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_12_2)
            {
                return ExpressSubjectDiscussionDoctor_12_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionDoctor_13)
            {
                return ExpressSubjectDiscussionDoctor_13ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientB_9_1)
            {
                return ExpressSubjectDiscussionPatientB_9_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_9_2)
            {
                return ExpressSubjectDiscussionPatientA_9_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_10_1)
            {
                return ExpressSubjectDiscussionPatientA_10_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientB_10_1)
            {
                return ExpressSubjectDiscussionPatientB_10_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_10_2)
            {
                return ExpressSubjectDiscussionPatientA_10_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_11_1)
            {
                return ExpressSubjectDiscussionPatientA_11_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientB_11_1)
            {
                return ExpressSubjectDiscussionPatientB_11_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_11_2)
            {
                return ExpressSubjectDiscussionPatientA_11_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_12_1)
            {
                return ExpressSubjectDiscussionPatientA_12_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientB_12_1)
            {
                return ExpressSubjectDiscussionPatientB_12_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_12_2)
            {
                return ExpressSubjectDiscussionPatientA_12_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_12_3)
            {
                return ExpressSubjectDiscussionPatientA_12_3ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_13_1)
            {
                return ExpressSubjectDiscussionPatientA_13_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionPatientA_13_2)
            {
                return ExpressSubjectDiscussionPatientA_13_2ThroughText(move);
            }

            //Second Non-Subject Discussion

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionDoctor_6)
            {
                return ExpressNonSubjectDiscussionDoctor_6ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionDoctor_7)
            {
                return ExpressNonSubjectDiscussionDoctor_7ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionDoctor_8)
            {
                return ExpressNonSubjectDiscussionDoctor_8ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionDoctor_9)
            {
                return ExpressNonSubjectDiscussionDoctor_9ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionDoctor_10)
            {
                return ExpressNonSubjectDiscussionDoctor_10ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientB_6)
            {
                return ExpressNonSubjectDiscussionPatientB_6ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientB_7)
            {
                return ExpressNonSubjectDiscussionPatientB_7ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientB_8)
            {
                return ExpressNonSubjectDiscussionPatientB_8ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientB_9)
            {
                return ExpressNonSubjectDiscussionPatientB_9ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientA_10)
            {
                return ExpressNonSubjectDiscussionPatientA_10ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.NonSubjectDiscussionPatientB_10)
            {
                return ExpressNonSubjectDiscussionPatientB_10ThroughText(move);
            }

            //Goodbyes

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeDoctor_1_1)
            {
                return ExpressGoodbyeDoctor_1_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeDoctor_1_2)
            {
                return ExpressGoodbyeDoctor_1_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeDoctor_2_1)
            {
                return ExpressGoodbyeDoctor_2_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeDoctor_2_2)
            {
                return ExpressGoodbyeDoctor_2_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyePatientA_1_1)
            {
                return ExpressGoodbyePatientA_1_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyePatientB_1_1)
            {
                return ExpressGoodbyePatientB_1_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyePatientA_1_2)
            {
                return ExpressGoodbyePatientA_1_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyePatientA_2_1)
            {
                return ExpressGoodbyePatientA_2_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyePatientB_2_1)
            {
                return ExpressGoodbyePatientB_2_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyePatientA_2_2)
            {
                return ExpressGoodbyePatientA_2_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyePatientB_2_2)
            {
                return ExpressGoodbyePatientB_2_2ThroughText(move);
            }

            //Other

            else if (move.Kind == ScenarioDialogMoveKind.EmptyLine)
            {
                return ExpressEmptyLineThroughText(move);
            }
            else if (move.Kind == ScenarioDialogMoveKind.SessionClosing)
            {
                return ExpressSessionClosingThroughText(move);
            }
            else if (move.Kind == ScenarioDialogMoveKind.AcknowledgementFirstPatient)
            {
                return ExpressAcknowledgementFirstPatientThroughText(move);
            }
            else if (move.Kind == ScenarioDialogMoveKind.AcknowledgementSecondPatient)
            {
                return ExpressAcknowledgementSecondPatientThroughText(move);
            }
            else
            {
                throw new ArgumentException
                (
                    "Move is of an unknown kind and cannot be expressed " +
                    "through text."
                );
            }
        }

        /// <summary>
        /// Produces a string of speech text representing realization of
        /// greetings.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressGreetingsDoctor_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.AreSame(move.Kind, ScenarioDialogMoveKind.GreetingsDoctor_1);

            if (move.Targets.IsEmpty())
            {
                return "Good morning! Please, make yourself comfortable.";
            }
            else
            {
                var names = move.TargetEnumeration.Select(actor => actor.Name).ToArray();
                return $"Good morning, {string.Join(", ", names)}.";
            }
        }

        private string
            ExpressGreetingsDoctor_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.AreSame(move.Kind, ScenarioDialogMoveKind.GreetingsDoctor_2);

            if (move.Targets.IsEmpty())
            {
                return "Hello. Please, take a seat.";
            }
            else
            {
                var names = move.TargetEnumeration.Select(actor => actor.Name).ToArray();
                return $"Hello, {string.Join(", ", names)}!";
            }
        }

        private string
            ExpressGreetingsPatients_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.AreSame(move.Kind, ScenarioDialogMoveKind.GreetingsPatients_1);

            if (move.Targets.IsEmpty())
            {
                return "Good morning!";
            }
            else
            {
                var names = move.TargetEnumeration.Select(actor => actor.Name).ToArray();
                return $"Good morning, {string.Join(", ", names)}!";
            }
        }

        private string
            ExpressGreetingsPatients_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.AreSame(move.Kind, ScenarioDialogMoveKind.GreetingsPatients_2);

            if (move.Targets.IsEmpty())
            {
                return "Good morning! Thank you!";
            }
            else
            {
                var names = move.TargetEnumeration.Select(actor => actor.Name).ToArray();
                return $"Good morning, {string.Join(", ", names)}! Thank you!";
            }
        }

        private string
            ExpressGreetingsPatients_3ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.AreSame(move.Kind, ScenarioDialogMoveKind.GreetingsPatients_3);

            if (move.Targets.IsEmpty())
            {
                return "Hello. Thank you!";
            }
            else
            {
                var names = move.TargetEnumeration.Select(actor => actor.Name).ToArray();
                return $"Hello {string.Join(", ", names)}. Thank you!";
            }
        }

        /// <summary>
        /// Produces a string of speech text representing realization of a
        /// acquaintance between actors.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressAcquaintanceDoctorThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            if (move.Targets.IsEmpty())
            {
                return "What is your name?";
            }
            else
            {
                var names = move.TargetEnumeration.Select(actor => actor.Name).ToArray();
                return $"What is your name, {string.Join(", ", names)}?";
            }
        }

        private string
            ExpressAcquaintancePatientThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"{move.Target.GetValue().Name}.";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of an
        /// problem definition between actors.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressProblemDoctor_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "How do you find yourself?";
        }

        private string
            ExpressProblemDoctor_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "What is the problem?";
        }

        private string
            ExpressProblemPatientA_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"I am worried about my leg, {move.Target.GetValue().Name}. " +
                   $"It hurts.";
        }

        private string
            ExpressProblemPatientB_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Oh, everything is fine. I am here to support my " +
                   "boyfriend.";
        }

        private string
            ExpressProblemPatientA_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"My leg hurts, {move.Target.GetValue().Name}.";
        }

        private string
            ExpressProblemPatientB_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return $"I am just an accompanying.";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of the
        /// first subject discussion between actors.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressSubjectDiscussionDoctor_1_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Ok. Let’s collect some basic information about you first, " +
                   $"{move.Target.GetValue().Name}.";
        }

        private string
            ExpressSubjectDiscussionDoctor_1_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Now I need to get some basic information about you " +
                   "first. What is your name again?";
        }

        private string
            ExpressSubjectDiscussionDoctor_2_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "How old are you?";
        }

        private string
            ExpressSubjectDiscussionDoctor_2_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "What is your age?";
        }

        private string
            ExpressSubjectDiscussionDoctor_3ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "What is your height and weight?";
        }

        private string
            ExpressSubjectDiscussionDoctor_4_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Good. So how your leg worries you? How exactly it hurts?";
        }

        private string
            ExpressSubjectDiscussionDoctor_4_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "So what is wrong with your leg? Can you describe it? " +
                   "Otherwise, I cannot help you.";
        }

        private string
            ExpressSubjectDiscussionDoctor_5_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Ok, I see. And for how long do you have this pain?";
        }

        private string
            ExpressSubjectDiscussionDoctor_5_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "For how long do you have this problem?";
        }

        private string
            ExpressSubjectDiscussionDoctor_6_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Oh, that is quite a while. And what happened before the " +
                   "pain started?";
        }

        private string
            ExpressSubjectDiscussionDoctor_6_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "What happened before the pain started?";
        }

        private string
            ExpressSubjectDiscussionDoctor_7_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Yes, I can imagine… And is your job related to physical " +
                   "activities?";
        }

        private string
            ExpressSubjectDiscussionDoctor_7_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Got it. And what is your occupation?";
        }

        private string
            ExpressSubjectDiscussionDoctor_8_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Ok. I think now I see what the problem is with your " +
                   $"knee, {move.Target.GetValue().Name}.";
        }

        private string
            ExpressSubjectDiscussionDoctor_8_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Probably I already know what the problem is.";
        }

        private string
            ExpressSubjectDiscussionPatientA_1_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Alright.";
        }

        private string
            ExpressSubjectDiscussionPatientA_1_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"My name is {move.Target.GetValue().Name}, Doctor.";
        }

        private string
            ExpressSubjectDiscussionPatientA_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I am 25.";
        }

        private string
            ExpressSubjectDiscussionPatientA_3_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"My height is about 180, and my weight… " +
                   $"{move.Target.GetValue().Name}, do you remember?";
        }

        private string
            ExpressSubjectDiscussionPatientB_3_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "His weight is almost 80 kilos. Got it quite recently. " +
                   "Completely gave up going to the gym.";
        }

        private string
            ExpressSubjectDiscussionPatientA_3_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "My height is about 180, and my weight… Hmm… I think " +
                   "it is almost 80 kilos.";
        }

        private string
            ExpressSubjectDiscussionPatientA_3_3ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "My height is about 180, and my weigh is almost 80 kilos.";
        }

        private string
            ExpressSubjectDiscussionPatientA_4_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "It hurts in the knee, especially when I go up the stairs.";
        }

        private string
            ExpressSubjectDiscussionPatientA_4_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Yes, of course. Sorry Sir. My knee hurts. Especially " +
                   "when I go up the stairs.";
        }

        private string
            ExpressSubjectDiscussionPatientA_5_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Almost two weeks, Doctor.";
        }

        private string
            ExpressSubjectDiscussionPatientA_5_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Two weeks.";
        }

        private string
            ExpressSubjectDiscussionPatientA_6_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"{move.Target.GetValue().Name} and I went for a mountain " +
                   $"skiing. I did not fall, did not hit my leg or anything. " +
                   $"Just once I made quite a sharp turn around the tree. " +
                   $"Then, suddenly something crackled and I started to feel " +
                   $"pain in my knee… And… And afterwards I could not go up " +
                   $"the stairs. Since that moment it hurts constantly.";
        }

        private string
            ExpressSubjectDiscussionPatientB_6_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Yes, I can confirm. He did not fell or hit anything " +
                   "with his leg. But after that sharp turn his face was " +
                   "full of agony!";
        }

        private string
            ExpressSubjectDiscussionPatientA_6_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"{move.Target.GetValue().Name} and I went for a mountain " +
                   $"skiing. I did not fall, did not hit my leg or anything. " +
                   $"Just once I made quite a sharp turn around the tree. " +
                   $"Then, suddenly something crackled and I started to feel " +
                   $"pain in my knee… And… And afterwards I could not go up " +
                   $"the stairs. Since that moment it hurts constantly.";
        }

        private string
            ExpressSubjectDiscussionPatientA_6_3ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "We went for a mountain skiing, and when I made a sharp " +
                   "turn, I felt that something crackled in my knee. Since " +
                   "that moment it hurts constantly.";
        }

        private string
            ExpressSubjectDiscussionPatientA_7_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "No, not really. I am an IT specialist in an insurance " +
                   "company. System administrator. ";
        }

        private string
            ExpressSubjectDiscussionPatientB_7_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Spends all days in front of the computer. No time for " +
                   "sports.";
        }

        private string
            ExpressSubjectDiscussionPatientA_7_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Not really. I am an IT specialist. System administrator.";
        }

        private string
            ExpressSubjectDiscussionPatientA_7_3ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "System administrator.";
        }

        private string
            ExpressSubjectDiscussionPatientA_8_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Oh, that sounds great!";
        }

        private string
            ExpressSubjectDiscussionPatientA_8_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Thank you, Doctor! Finally some good news…";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of the
        /// first non-subject discussion between actors.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressNonSubjectDiscussionDoctor_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "But tell me first. Where exactly have you been skiing? " +
                   "I am asking because I really like mountain skiing!";
        }

        private string
            ExpressNonSubjectDiscussionDoctor_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Oh… It is a very nice place… I remember it. The " +
                   "nature was wonderful there. ";
        }

        private string
            ExpressNonSubjectDiscussionDoctor_3ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Yes. They had some really good ski slopes there. Tell " +
                   "me, Böglerhof restaurant is still in place?";
        }

        private string
            ExpressNonSubjectDiscussionDoctor_4ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "A very good place with a large assortment of good wine. " +
                   "I just was wondering whether Duftner family still run " +
                   "the place?";
        }

        private string
            ExpressNonSubjectDiscussionDoctor_5ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Just a flood of warm memories…";
        }

        private string
            ExpressNonSubjectDiscussionPatientA_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "We have been to a place called Alpbach, in Austria. It " +
                   "is a really nice ski resort for beginners.";
        }

        private string
            ExpressNonSubjectDiscussionPatientB_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "And we are the beginners, it was our second time.";
        }

        private string
            ExpressNonSubjectDiscussionPatientB_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "A lovely place indeed. So you have been there?";
        }

        private string
            ExpressNonSubjectDiscussionPatientA_3ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I guess. Unfortunately, we have not visited it.";
        }

        private string
            ExpressNonSubjectDiscussionPatientB_4ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "We are really sorry, but we do not know.";
        }

        private string
            ExpressNonSubjectDiscussionPatientB_5ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "We can imagine!";
        }

        private string
            ExpressNonSubjectDiscussionPatientA_5ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "We will try to get there during our next mountain " +
                   "skiing trip… And once my leg is fixed.";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of the
        /// second subject discussion between actors.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressSubjectDiscussionDoctor_9_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Alright. Now I need a bit more details to be sure. " +
                   $"{move.Target.GetValue().Name}, maybe you could help us " +
                   $"with it?";
        }

        private string
            ExpressSubjectDiscussionDoctor_9_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Ok. Now I need more details to be sure. ";
        }

        private string
            ExpressSubjectDiscussionDoctor_10_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Good. Has Mark ever had problems with his knees in " +
                   "the past?";
        }

        private string
            ExpressSubjectDiscussionDoctor_10_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Have you ever had problems with your knees in the past?";
        }

        private string
            ExpressSubjectDiscussionDoctor_11ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Do you have any allergic reactions to anything? " +
                   "Intolerance to any medications?";
        }

        private string
            ExpressSubjectDiscussionDoctor_12_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Ok. So I think your meniscus in a knee joint got " +
                   "injured. First of all, I prescribe you to take two " +
                   "pills of paracetamol per day to get rid of the pain. " +
                   "It is necessary to make an MRI of the knee joint as " +
                   "soon as possible, so I will make an appointment for " +
                   "you next Tuesday. Plus, it is good to plan an " +
                   "arthroscopy – a minimally invasive intervention, and " +
                   "your meniscus will be good.";
        }

        private string
            ExpressSubjectDiscussionDoctor_12_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Ok. I think your meniscus in a knee joint got injured. It " +
                   "is necessary to make an MRI of the knee joint as soon as " +
                   "possible and plan an arthroscopy – a minimally invasive " +
                   "intervention, and your meniscus will be good.";
        }

        private string
            ExpressSubjectDiscussionDoctor_13ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I also recommend you wearing an orthosis on your knee " +
                   "joint and using some anti-inflammatory ointments. You " +
                   "can find the names of those in the medical prescription " +
                   "that I will provide.";
        }

        private string
            ExpressSubjectDiscussionPatientB_9_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Sure! I am glad to supply you with any information I " +
                   "can.";
        }

        private string
            ExpressSubjectDiscussionPatientA_9_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Of course! Anything you need.";
        }

        private string
            ExpressSubjectDiscussionPatientA_10_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I do not remember any knee injuries in the past.";
        }

        private string
            ExpressSubjectDiscussionPatientB_10_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "No, I do not think so. At least as far as I am " +
                   "concerned, since we are together.";
        }

        private string
            ExpressSubjectDiscussionPatientA_10_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"No, as long as I remember, everything was fine, " +
                   $"{move.Target.GetValue().Name}.";
        }

        private string
            ExpressSubjectDiscussionPatientA_11_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Nothing that pops up in my mind.";
        }

        private string
            ExpressSubjectDiscussionPatientB_11_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I am pretty sure he has no allergies or intolerances.";
        }

        private string
            ExpressSubjectDiscussionPatientA_11_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "No, no allergies and no intolerances.";
        }

        private string
            ExpressSubjectDiscussionPatientA_12_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Thank you! You cannot imagine what a relief it is!";
        }

        private string
            ExpressSubjectDiscussionPatientB_12_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "We are very grateful!";
        }

        private string
            ExpressSubjectDiscussionPatientA_12_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Thank you. Next Tuesday suits me.";
        }

        private string
            ExpressSubjectDiscussionPatientA_12_3ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Thank you, {move.Target.GetValue().Name}! I will make " +
                   $"an appointment for an MRI as soon as possible!";
        }

        private string
            ExpressSubjectDiscussionPatientA_13_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I will follow your recommendations. Thank you.";
        }

        private string
            ExpressSubjectDiscussionPatientA_13_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Again, thank you Sir! I will certainly follow all " +
                   "your recommendations!";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of the
        /// second non-subject discussion between actors.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressNonSubjectDiscussionDoctor_6ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Good.";
        }

        private string
            ExpressNonSubjectDiscussionDoctor_7ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Sure. What bothers you?";
        }

        private string
            ExpressNonSubjectDiscussionDoctor_8ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Ok. What kind of hereditary disease is it?";
        }

        private string
            ExpressNonSubjectDiscussionDoctor_9ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Oh. Do not be scared. If you have not had " +
                   "manifestations of this disease in your childhood, the " +
                   "likelihood to appear in your current age is negligible. " +
                   "At least based on genetics.";
        }

        private string
            ExpressNonSubjectDiscussionDoctor_10ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Also, Males are more likely to be color blind than " +
                   "females, as the genes responsible for the most common " +
                   "forms of color blindness are on the X chromosome. So " +
                   "there is nothing to worry about.";
        }

        private string
            ExpressNonSubjectDiscussionPatientB_6ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"{move.Target.GetValue().Name}, I wanted to ask you " +
                   $"something.";
        }

        private string
            ExpressNonSubjectDiscussionPatientB_7ThroughText(DialogMove<CulturalVirtualHuman> move)
        {

            return "My question is related to hereditary diseases.";
        }

        private string
            ExpressNonSubjectDiscussionPatientB_8ThroughText(DialogMove<CulturalVirtualHuman> move)
        {

            return "My mother has color blindness in a low degree. What " +
                   "are the chances that I will become color blind as well?";
        }

        private string
            ExpressNonSubjectDiscussionPatientB_9ThroughText(DialogMove<CulturalVirtualHuman> move)
        {

            return "Good to know! Thank you.";
        }

        private string
            ExpressNonSubjectDiscussionPatientA_10ThroughText(DialogMove<CulturalVirtualHuman> move)
        {

            return "This X chromosome… But thank you! It is really good to " +
                   "know.";
        }

        private string
            ExpressNonSubjectDiscussionPatientB_10ThroughText(DialogMove<CulturalVirtualHuman> move)
        {

            return "Great!";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of
        /// goodbyes.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressGoodbyeDoctor_1_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Alright. Come to visit me once you have the MRI results. " +
                   "And do not forget to make an appointment first!";
        }

        private string
            ExpressGoodbyeDoctor_1_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Ok. See you next time with MRI results.";
        }

        private string
            ExpressGoodbyeDoctor_2_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {

            if (move.Targets.IsEmpty())
            {
                return "Goodbye.";
            }
            else
            {
                var names = move.TargetEnumeration.Select(actor => actor.Name).ToArray();
                return $"Goodbye {string.Join(", ", names)}.";
            }
        }

        private string
            ExpressGoodbyeDoctor_2_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Goodbye {move.Target.GetValue().Name}. I hope you " +
                   $"will recover soon!";
        }

        private string
            ExpressGoodbyePatientA_1_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Of course!";
        }

        private string
            ExpressGoodbyePatientB_1_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I will make sure he does not forget to make an " +
                   "appointment.";
        }

        private string
            ExpressGoodbyePatientA_1_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Of course, {move.Target.GetValue().Name}! I will not " +
                   $"forget to bring it with me!";
        }

        private string
            ExpressGoodbyePatientA_2_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Goodbye. Thank you and have a good day!";
        }

        private string
            ExpressGoodbyePatientB_2_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Goodbye.";
        }

        private string
            ExpressGoodbyePatientA_2_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Goodbye, {move.Target.GetValue().Name}! Thank you for " +
                   $"your time!";
        }

        private string
            ExpressGoodbyePatientB_2_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Goodbye, {move.Target.GetValue().Name}. Thank you!";
        }

        /// <summary>
        /// Produces an empty line of speech.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressEmptyLineThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "...";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of a
        /// session's closing remarks.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string ExpressSessionClosingThroughText
        (
            DialogMove<CulturalVirtualHuman> move
        )
        {
            Require.AreSame(move.Kind, ScenarioDialogMoveKind.SessionClosing);

            return
                "Well, I hope I helped you with what I could! I hope we " +
                "will not see each other soon.";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of an
        /// acknowledgement.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string ExpressAcknowledgementFirstPatientThroughText
        (
            DialogMove<CulturalVirtualHuman> move
        )
        {
            Require.AreSame(move.Kind, ScenarioDialogMoveKind.AcknowledgementFirstPatient);

            return "Ok.";
        }

        private string ExpressAcknowledgementSecondPatientThroughText
        (
            DialogMove<CulturalVirtualHuman> move
        )
        {
            Require.AreSame(move.Kind, ScenarioDialogMoveKind.AcknowledgementSecondPatient);

            return "Alright.";
        }

        /// <summary>
        /// Sets the action delay timer to a random value in order to mimic
        /// human reaction time.
        /// </summary>
        private void SetRandomActionDelay()
        {
            float duration = (float)UniformDistribution.Sample(1.0, 1.5);
            _action_delay_timer.Set(duration);
            _action_delay_timer.Start();
        }

        private readonly Queue<string> _pending_words = new Queue<string>();

        private Timer _action_delay_timer;
        private Timer _speech_timer;

    }
}
