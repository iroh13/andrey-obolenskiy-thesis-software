﻿using System;
using Common.Validation;
using Dialog.Agency.Modules.Perception;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace doctor_patient_scenario.Agency.Modules.Perception
{
    /// <summary>
    /// This class is an implementation of the 
    /// <see cref="RecentActivityPerceptionModule"/> for the scenario's 
    /// virtual humans.
    /// </summary>
    public sealed class RAPModule
        : RecentActivityPerceptionModule
    {
        /// <summary>
        /// Perceives realized dialogue moves.
        /// </summary>
        /// <param name="data">
        /// This iteration's dialogue move events.
        /// </param>
        /// <remarks>
        /// <paramref name="data"/> must not be null.
        /// </remarks>
        public void PerceiveDialogMoves
        (
            IEnumerable<DialogMoveRealizationEvent> data
        )
        {
            Require.IsNotNull(data);

            _recent_events = data.ToList();
        }

        /// <summary>
        /// Fills in the report with recently perceived dialogue move
        /// realization events.
        /// </summary>
        /// <param name="report">The report to fill in.</param>
        /// <remarks>
        /// <paramref name="report"/> must not be null.
        /// </remarks>
        protected override void ComposeActivityReport
        (
            RecentActivityReport report
        )
        {
            Require.IsNotNull(report);

            foreach (var @event in _recent_events)
            {
                report.Add(@event);
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(RAPModule)}(" +
                   $"{nameof(State)} = {State})";
        }

        private IEnumerable<DialogMoveRealizationEvent> _recent_events;
    }
}
