﻿using Common.Functional.Options;
using Common.Functional.Options.Extensions;
using Common.Validation;
using Dialog.Agency.System;
using Dialog.SPB.Modules;
using Dialog.SPB.State;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CulturalModel.CulturalProject;
using doctor_patient_scenario.Agency.Modules.Delibiration;
using doctor_patient_scenario.Agency.Modules.Perception;
using doctor_patient_scenario.Agency.Modules.Action;
using doctor_patient_scenario.Agency.State;
using doctor_patient_scenario.Communication;
using doctor_patient_scenario.Social_Practices;
using DataSubmission = Dialog.Communication.Management.CommunicationManager.DataSubmission;
using DataSubscription = Dialog.Communication.Management.CommunicationManager.DataSubscription;
using MoveEvent = Dialog.Agency.Modules.Perception.DialogMoveRealizationEvent;

namespace doctor_patient_scenario.Simulation.Actor_Controllers
{
    /// <summary>
    /// This class represents the AI controlling a patient actor.
    /// </summary>
    public sealed class PatientController
    {
        /// <summary>
        /// Creates a new controller for the specified actor.
        /// </summary>
        /// <param name="patient">The actor to control.</param>
        /// <param name="scenario">
        /// The scenario the patient is part of.
        /// </param>
        /// <remarks>
        /// <paramref name="patient"/> must not be null.
        /// <paramref name="patient"/> must belong to 
        /// <paramref name="scenario"/>
        /// <paramref name="scenario"/> must not be null.
        /// </remarks>
        public PatientController(CulturalVirtualHuman patient, Scenario scenario)
        {
            Require.IsNotNull(patient);
            Require.IsNotNull(scenario);
            Require.IsTrue
            (
                patient == scenario.Session.PatientA ||
                patient == scenario.Session.PatientB
            );

            Patient = patient;
            Scenario = scenario;

            _data_subscription = Scenario.CommunicationManager.Engage(Patient);
            _data_subscription.Include<Speech>((manager, packets) =>
            {
                OnPerceiveSpeechData
                (
                    packets.Select(packet => packet.Payload)
                );
            });
            _data_subscription.Include<MoveEvent>((manager, packets) =>
            {
                OnPerceiveMoveEventData
                (
                    packets.Select(packet => packet.Payload)
                );
            });

            Patient.Action =
                Option.CreateSome<Action<DataSubmission>>(Act);

            _rap_module = new RAPModule();
            _cap_module = new CAPModule();
            _ar_module = new ARModule();
            var modules = new ModuleBundle.Builder()
                .WithRecentActivityPerceptionBy(_rap_module)
                .WithCurrentActivityPerceptionBy(_cap_module)
                .WithStateUpdateBy(new SUModule())
                .WithActionSelectionBy(new SPBASModule())
                .WithActionTimingBy(new SPBATModule())
                .WithActionRealizationBy(_ar_module)
                .Build();
            var state = ScenarioInformationState.Create
            (
                Scenario.Clock,
                new SocialContext
                (
                    Patient,
                    ScenarioSocialPracticeProgram.Create(Scenario.Session)
                )
            );
            AgencySystem = new AgencySystem(state, modules);
        }

        /// <summary>
        /// Gets the scenario the controlled actor is part of.
        /// </summary>
        public Scenario Scenario { get; private set; }
        /// <summary>
        /// Gets the controlled actor.
        /// </summary>
        public CulturalVirtualHuman Patient { get; private set; }
        /// <summary>
        /// Gets the actor's agency system.
        /// </summary>
        public AgencySystem AgencySystem { get; private set; }

        /// <summary>
        /// This handles perceived <see cref="Speech"/> data delivered by the
        /// scenario's communication manager.
        /// </summary>
        /// <param name="data">The received speech data.</param>
        private void OnPerceiveSpeechData(IEnumerable<Speech> data)
        {
            _cap_module.PerceiveSpeech(data);
        }
        /// <summary>
        /// This handles perceived <see cref="MoveEvent"/> data delivered by 
        /// the scenario's communication manager.
        /// </summary>
        /// <param name="data">The received speech data.</param>
        private void OnPerceiveMoveEventData(IEnumerable<MoveEvent> data)
        {
            _rap_module.PerceiveDialogMoves(data);
        }

        /// <summary>
        /// Performs action in the name of the controlled actor.
        /// </summary>
        /// <param name="submission">
        /// The object through which to submit output behavior.
        /// </param>
        /// <remarks>
        /// <paramref name="submission"/> must not be null.
        /// </remarks>
        private void Act(DataSubmission submission)
        {
            AgencySystem.Step();

            _ar_module.OutputSpeech.IfIsSome(speech =>
            {
                submission.Add(speech);
            });
            File.AppendAllText("Logfile.txt", $"{DateTime.Now} move added: {_ar_module.OutputMove} {Environment.NewLine}");
            _ar_module.OutputMove.IfIsSome(move =>
            {               
                submission.Add(new MoveEvent(Patient, move));
            });
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(PatientController)}(" +
                   $"{nameof(Patient)} = {Patient})";
        }

        private readonly DataSubscription _data_subscription;

        private readonly RAPModule _rap_module;
        private readonly CAPModule _cap_module;
        private readonly ARModule _ar_module;
    }
}
