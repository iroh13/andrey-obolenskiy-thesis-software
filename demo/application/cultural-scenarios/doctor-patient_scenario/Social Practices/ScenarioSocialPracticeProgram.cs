﻿using Common.Validation;
using Dialog.SPB.Program;
using CulturalModel.CulturalProject;
using dialog_culture.Cultural_Norms.Status_Norms;
using doctor_patient_scenario.Agency.Scene;
using Dialog.Agency.Dialog_Moves;
using static Dialog.SPB.Program.NLI.ProgramBuildUtilities;
using static dialog_culture.Program.Natural_Language_Interface.ProgramBuildUtilitiesCultural;
using static doctor_patient_scenario.Agency.Dialog_Moves.ScenarioDialogMoveKind;

namespace doctor_patient_scenario.Social_Practices
{
    /// <summary>
    /// This class is responsible for instantiating social practice programs
    /// for the scenario.
    /// </summary>
    public static class ScenarioSocialPracticeProgram
    {
        /// <summary>
        /// Creates a new program for the doctor-patient scenario.
        /// </summary>
        /// <param name="session">
        /// The session for which to make the program.
        /// </param>
        /// <returns>
        /// A new social practice program.
        /// </returns>
        /// <remarks>
        /// <paramref name="session"/> must not be null.
        /// </remarks>
        public static Node Create(Session session)
        {
            Require.IsNotNull(session);

            CulturalVirtualHuman therapist = session.Therapist;
            CulturalVirtualHuman patientA = session.PatientA;
            CulturalVirtualHuman patientB = session.PatientB;

            return

                // ----- BEGIN PRACTICE DESCRIPTION ------ //

                ExpectSkippingSequence
                (
                    "Doctor-Patients Session",

                    //Greetings

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: patientA),
                        ExpectEvent(GreetingsDoctor_1, source: therapist),
                        ExpectEvent(GreetingsDoctor_2, source: therapist)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: therapist),
                        ExpectCultural
                        (
                            new StatusLowToHighNorm(source: patientA, target: therapist),
                            ExpectEvent(GreetingsPatients_2, source: patientA, target: therapist),
                            ExpectEvent(GreetingsPatients_1, source: patientA, target: therapist)
                        ),
                        ExpectCultural
                        (
                            new StatusLowToHighNorm(source: patientA, target: therapist),
                            ExpectEvent(GreetingsPatients_3, source: patientA, target: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: therapist),
                        ExpectCultural
                        (
                            new StatusLowToHighNorm(source: patientB, target: therapist),
                            ExpectEvent(GreetingsPatients_2, source: patientB),
                            ExpectEvent(GreetingsPatients_1, source: patientB)
                        ),
                        ExpectCultural
                        (
                            new StatusLowToHighNorm(source: patientB, target: therapist),
                            ExpectEvent(GreetingsPatients_3, source: patientB)
                        )
                    ),

                    //Problem Definition                  
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(ProblemDoctor_1, source: therapist),
                        ExpectEvent(ProblemDoctor_2, source: therapist)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectEvent(ProblemPatientA_1, source: patientA, target: therapist),
                        ExpectEvent(ProblemPatientA_2, source: patientA, target: therapist)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectEvent(ProblemPatientB_1, source: patientB),
                        ExpectEvent(ProblemPatientB_2, source: patientB)
                    ),

                    //Acquaintance

                    ExpectEvent(AcquaintanceDoctor, source: therapist),
                    ExpectEvent(AcquaintancePatient, source: patientA, target: patientA),
                    ExpectEvent(AcquaintancePatient, source: patientB, target: patientB),

                    //First Subject Discussion

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(SubjectDiscussionDoctor_1_1, source: therapist, target: patientA),
                        ExpectEvent(SubjectDiscussionDoctor_1_2, source: therapist, target: patientA)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectEvent(SubjectDiscussionPatientA_1_1, source: patientA),
                        ExpectEvent(SubjectDiscussionPatientA_1_2, source: patientA, target: patientA)
                    ),

                    ExpectAll
                    (
                        "Questions 1",

                        ExpectSequence
                        (
                            "Question A",

                            ExpectCultural
                            (
                                new NonchalanceHighNorm(source: therapist, target: therapist),
                                ExpectEvent(SubjectDiscussionDoctor_2_1, source: therapist, target: patientA),
                                ExpectEvent(SubjectDiscussionDoctor_2_2, source: therapist, target: patientA)
                            ),
                            ExpectEvent(SubjectDiscussionPatientA_2, source: patientA)

                        ),
                        ExpectSkippingSequence
                        (
                            "Question B",

                            ExpectEvent(SubjectDiscussionDoctor_3, source: therapist, target: patientA),
                            ExpectCultural
                            (
                                new NonchalanceHighNorm(source: patientA, target: patientA),
                                ExpectCultural
                                (
                                    new ShortTermOrientationNorm(source: patientA, target: patientA),
                                    ExpectEvent(SubjectDiscussionPatientA_3_1, source: patientA, target: patientB),
                                    ExpectEvent(SubjectDiscussionPatientA_3_2, source: patientA)
                                ),
                                ExpectCultural
                                (
                                    new LongTermOrientationNorm(source: patientA, target: patientA),
                                    ExpectEvent(SubjectDiscussionPatientA_3_3, source: patientA),
                                    ExpectEvent(EmptyLine, source: patientA)
                                )
                            ),
                            ExpectCultural
                            (
                                new NonchalanceHighNorm(source: patientB, target: patientB),
                                ExpectCultural
                                (
                                    new ShortTermOrientationNorm(source: patientB, target: patientB),
                                    ExpectEvent(SubjectDiscussionPatientB_3_1, source: patientB)
                                )
                            )
                        )
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(SubjectDiscussionDoctor_4_1, source: therapist, target: patientA),
                        ExpectEvent(SubjectDiscussionDoctor_4_2, source: therapist, target: patientA)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectEvent(SubjectDiscussionPatientA_4_1, source: patientA),
                        ExpectEvent(SubjectDiscussionPatientA_4_2, source: patientA)
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(SubjectDiscussionDoctor_5_1, source: therapist, target: patientA),
                        ExpectEvent(SubjectDiscussionDoctor_5_2, source: therapist, target: patientA)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectEvent(SubjectDiscussionPatientA_5_1, source: patientA, target: therapist),
                        ExpectEvent(SubjectDiscussionPatientA_5_2, source: patientA)
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(SubjectDiscussionDoctor_6_1, source: therapist, target: patientA),
                        ExpectEvent(SubjectDiscussionDoctor_6_2, source: therapist, target: patientA)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientA, target: patientA),
                            ExpectEvent(SubjectDiscussionPatientA_6_1, source: patientA, target: patientB),
                            ExpectEvent(SubjectDiscussionPatientA_6_2, source: patientA, target: patientB)
                        ),
                        ExpectCultural
                        (
                            new LongTermOrientationNorm(source: patientA, target: patientA),
                            ExpectEvent(SubjectDiscussionPatientA_6_3, source: patientA),
                            ExpectEvent(EmptyLine, source: patientA)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientB, target: patientB),
                            ExpectEvent(SubjectDiscussionPatientB_6_1, source: patientB)
                        )
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(SubjectDiscussionDoctor_7_1, source: therapist, target: patientA),
                        ExpectEvent(SubjectDiscussionDoctor_7_2, source: therapist, target: patientA)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientA, target: patientA),
                            ExpectEvent(SubjectDiscussionPatientA_7_1, source: patientA),
                            ExpectEvent(SubjectDiscussionPatientA_7_2, source: patientA)
                        ),
                        ExpectCultural
                        (
                            new LongTermOrientationNorm(source: patientA, target: patientA),
                            ExpectEvent(SubjectDiscussionPatientA_7_3, source: patientA),
                            ExpectEvent(EmptyLine, source: patientA)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientB, target: patientB),
                            ExpectEvent(SubjectDiscussionPatientB_7_1, source: patientB)
                        )
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(SubjectDiscussionDoctor_8_1, source: therapist, target: patientA),
                        ExpectEvent(SubjectDiscussionDoctor_8_2, source: therapist, target: patientA)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectEvent(SubjectDiscussionPatientA_8_1, source: patientA),
                        ExpectEvent(SubjectDiscussionPatientA_8_2, source: patientA)
                    ),            

                    //First Non-Subject Discussion

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: therapist, target: therapist),
                            ExpectEvent(NonSubjectDiscussionDoctor_1, source: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientA, target: patientA),
                            ExpectEvent(NonSubjectDiscussionPatientA_1, source: patientA)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientB, target: patientB),
                            ExpectEvent(NonSubjectDiscussionPatientB_1, source: patientB)
                        )
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: therapist, target: therapist),
                            ExpectEvent(NonSubjectDiscussionDoctor_2, source: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientB, target: patientB),
                            ExpectEvent(NonSubjectDiscussionPatientB_2, source: patientB)
                        )
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: therapist, target: therapist),
                            ExpectEvent(NonSubjectDiscussionDoctor_3, source: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientA, target: patientA),
                            ExpectEvent(NonSubjectDiscussionPatientA_3, source: patientA)
                        )
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: therapist, target: therapist),
                            ExpectEvent(NonSubjectDiscussionDoctor_4, source: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientB, target: patientB),
                            ExpectEvent(NonSubjectDiscussionPatientB_4, source: patientB)
                        )
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: therapist, target: therapist),
                            ExpectEvent(NonSubjectDiscussionDoctor_5, source: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientB, target: patientB),
                            ExpectEvent(NonSubjectDiscussionPatientB_5, source: patientB)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientA, target: patientA),
                            ExpectEvent(NonSubjectDiscussionPatientA_5, source: patientA)
                        )
                    ),

                    //Second Subject Discussion

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(SubjectDiscussionDoctor_9_1, source: therapist, target: patientB),
                        ExpectEvent(SubjectDiscussionDoctor_9_2, source: therapist, target: patientA)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectEvent(SubjectDiscussionPatientB_9_1, source: patientB),
                        ExpectEvent(SubjectDiscussionPatientA_9_2, source: patientA)
                    ),

                    ExpectAll
                    (
                        "Questions 2",

                        ExpectSkippingSequence
                        (
                            "Question C",

                            ExpectCultural
                            (
                                new NonchalanceHighNorm(source: therapist, target: therapist),
                                ExpectEvent(SubjectDiscussionDoctor_10_1, source: therapist, target: patientB),
                                ExpectEvent(SubjectDiscussionDoctor_10_2, source: therapist, target: patientA)
                            ),
                            ExpectCultural
                            (
                                new NonchalanceHighNorm(source: patientB, target: patientB),
                                ExpectEvent(SubjectDiscussionPatientB_10_1, source: patientB)
                            ),
                            ExpectCultural
                            (
                                new NonchalanceHighNorm(source: patientA, target: patientA),
                                ExpectEvent(SubjectDiscussionPatientA_10_1, source: patientA),
                                ExpectEvent(SubjectDiscussionPatientA_10_2, source: patientA, target: therapist)
                            )
                        ),
                        ExpectSkippingSequence
                        (
                            "Question D",

                            ExpectEvent(SubjectDiscussionDoctor_11, source: therapist, target: patientA),
                            ExpectCultural
                            (
                                new NonchalanceHighNorm(source: patientA, target: patientA),
                                ExpectEvent(SubjectDiscussionPatientA_11_1, source: patientA),
                                ExpectEvent(SubjectDiscussionPatientA_11_2, source: patientA)
                            ),
                            ExpectCultural
                            (
                                new NonchalanceHighNorm(source: patientB, target: patientB),
                                ExpectEvent(SubjectDiscussionPatientB_11_1, source: patientB)
                            )
                        )
                    ),
               

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(SubjectDiscussionDoctor_12_1, source: therapist, target: patientA),
                        ExpectEvent(SubjectDiscussionDoctor_12_2, source: therapist, target: patientA)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectCultural
                        (
                            new StatusLowToHighNorm(source: patientA, target: therapist), 
                            ExpectEvent(SubjectDiscussionPatientA_12_1, source: patientA),
                            ExpectEvent(SubjectDiscussionPatientA_12_2, source: patientA)
                        ),
                        ExpectCultural
                        (
                            new StatusLowToHighNorm(source: patientA, target: therapist), 
                            ExpectEvent(SubjectDiscussionPatientA_12_3, source: patientA, target: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new StatusLowToHighNorm(source: patientB, target: therapist),
                            ExpectEvent(SubjectDiscussionPatientB_12_1, source: patientB)
                        )
                    ),

                    ExpectEvent(SubjectDiscussionDoctor_13, source: therapist, target: patientA),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectEvent(SubjectDiscussionPatientA_13_1, source: patientA),
                        ExpectEvent(SubjectDiscussionPatientA_13_2, source: patientA)
                    ),

                    //Second Non-Subject Discussion

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: therapist, target: therapist),
                            ExpectEvent(NonSubjectDiscussionDoctor_6, source: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientB, target: patientB),
                            ExpectEvent(NonSubjectDiscussionPatientB_6, source: patientB, target: therapist)
                        )
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: therapist, target: therapist),
                            ExpectEvent(NonSubjectDiscussionDoctor_7, source: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientB, target: patientB),
                            ExpectEvent(NonSubjectDiscussionPatientB_7, source: patientB)
                        )
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: therapist, target: therapist),
                            ExpectEvent(NonSubjectDiscussionDoctor_8, source: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientB, target: patientB),
                            ExpectEvent(NonSubjectDiscussionPatientB_8, source: patientB)
                        )
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: therapist, target: therapist),
                            ExpectEvent(NonSubjectDiscussionDoctor_9, source: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientB, target: patientB),
                            ExpectEvent(NonSubjectDiscussionPatientB_9, source: patientB)
                        )
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: therapist, target: therapist),
                            ExpectEvent(NonSubjectDiscussionDoctor_10, source: therapist)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientB, target: patientB),
                            ExpectEvent(NonSubjectDiscussionPatientB_10, source: patientB)
                        )
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectCultural
                        (
                            new ShortTermOrientationNorm(source: patientA, target: patientA),
                            ExpectEvent(NonSubjectDiscussionPatientA_10, source: patientA)
                        )
                    ),

                    //Goodbyes

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(GoodbyeDoctor_1_1, source: therapist),
                        ExpectEvent(GoodbyeDoctor_1_2, source: therapist)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectEvent(GoodbyePatientA_1_1, source: patientA),
                        ExpectEvent(GoodbyePatientA_1_2, source: patientA, target: therapist)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectEvent(GoodbyePatientB_1_1, source: patientB)
                    ),

                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(GoodbyeDoctor_2_1, source: therapist, target: patientB),
                        ExpectEvent(GoodbyeDoctor_2_1, source: therapist)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientB, target: patientB),
                        ExpectEvent(GoodbyePatientB_2_1, source: patientB),
                        ExpectEvent(GoodbyePatientB_2_2, source: patientB, target: therapist)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: therapist, target: therapist),
                        ExpectEvent(GoodbyeDoctor_2_2, source: therapist, target: patientA)
                    ),
                    ExpectCultural
                    (
                        new NonchalanceHighNorm(source: patientA, target: patientA),
                        ExpectEvent(GoodbyePatientA_2_1, source: patientA),
                        ExpectEvent(GoodbyePatientA_2_2, source: patientA, target: therapist)
                    )
                );

        } 
    }
}
