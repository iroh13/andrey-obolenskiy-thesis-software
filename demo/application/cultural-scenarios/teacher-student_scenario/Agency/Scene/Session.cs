﻿using Common.Collections;
using Common.Collections.Interfaces;
using Common.Collections.Views;
using Common.Validation;
using CulturalModel.CulturalProject;
using teacher_student_scenario.Simulation.Actor_Controllers;

namespace teacher_student_scenario.Agency.Scene
{
    /// <summary>
    /// The session class represents a couples-therapy session involving one
    /// therapist and a pair of patients.
    /// </summary>
    public class Session
    {
        /// <summary>
        /// Creates a new session using the specified actors.
        /// </summary>
        /// <param name="therapist">The therapist.</param>
        /// <param name="couple">The patient couple.</param>
        /// <remarks>
        /// <paramref name="therapist"/> must not be null.
        /// The patients of <paramref name="couple"/> must be in a relationship
        /// with each other.
        /// The <paramref name="couple"/> must contain two distinct patients.
        /// </remarks>
        public Session(CulturalVirtualHuman therapist, CulturalVirtualHuman patientA, CulturalVirtualHuman patientB)
        {
            Require.IsNotNull(therapist);
            Require.AreNotEqual(patientA, patientB);

            Therapist = therapist;
            PatientA = patientA;
            PatientB = patientB;
            Actors = new CollectionView<CulturalVirtualHuman>
            (
                new CulturalVirtualHuman[3]
                {
                    Therapist,
                    PatientA,
                    PatientB
                }
            );
        }

        /// <summary>
        /// Gets the therapist.
        /// </summary>
        public CulturalVirtualHuman Therapist { get; private set; }

        /// <summary>
        /// Gets the first patient.
        /// </summary>
        public CulturalVirtualHuman PatientA { get; private set; }

        /// <summary>
        /// Gets the second patient.
        /// </summary>
        public CulturalVirtualHuman PatientB { get; private set; }

        /// <summary>
        /// Gets a collection of all the actors.
        /// </summary>
        public ImmutableCollection<CulturalVirtualHuman> Actors { get; private set; }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(Session)}(" +
                   $"{nameof(Therapist)} = {Therapist}, " +
                   $"{nameof(PatientA)} = {PatientA}), " +
                   $"{nameof(PatientB)} = {PatientB})";
        }
    }
}
