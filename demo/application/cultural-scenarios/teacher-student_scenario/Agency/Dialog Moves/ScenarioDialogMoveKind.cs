﻿using Dialog.Agency.Dialog_Moves;

namespace teacher_student_scenario.Agency.Dialog_Moves
{
    public sealed class ScenarioDialogMoveKind
        : DialogMoveKind
    {
        /// <summary>
        /// Indicates a greeting from one actor to one or more others.
        /// </summary>
        public static readonly DialogMoveKind GreetingsTeacher_1 =
            new ScenarioDialogMoveKind("Good morning! How can I help you?");

        public static readonly DialogMoveKind GreetingsTeacher_2 =
            new ScenarioDialogMoveKind("Hello. What is the matter?");

        public static readonly DialogMoveKind GreetingsStudentB_1 =
            new ScenarioDialogMoveKind("Good morning, Teacher! Can we take a little time from you?");

        public static readonly DialogMoveKind GreetingsStudentB_2 =
            new ScenarioDialogMoveKind("Hello, Teacher. Could you spare some time for us?");

        /// <summary>
        /// Indicates a problem definition from one actor to one or more other
        /// actors.
        /// </summary>
        public static readonly DialogMoveKind ProblemTeacher_1 =
            new ScenarioDialogMoveKind("What exactly would you like to know?");

        public static readonly DialogMoveKind ProblemTeacher_2 =
            new ScenarioDialogMoveKind("What exactly you wanted to know?");

        public static readonly DialogMoveKind ProblemStudentA_1 =
            new ScenarioDialogMoveKind("Hi! I am Mark.");

        public static readonly DialogMoveKind ProblemStudentB_1 =
            new ScenarioDialogMoveKind("I am Anna. We wanted to ask you several questions about the upcoming exam.");

        public static readonly DialogMoveKind ProblemStudentB_2 =
            new ScenarioDialogMoveKind("Thank you! I am Anna and this is Mark. We would like to know something about the upcoming exam.");

        /// <summary>
        /// Indicates the first subject discussion between actors.
        /// </summary>
        public static readonly DialogMoveKind SubjectDiscussionTeacher_1_1 =
            new ScenarioDialogMoveKind("The exam will take exactly two hours.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_1_2 =
            new ScenarioDialogMoveKind("The exam will take exactly two hours.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_2_1 =
            new ScenarioDialogMoveKind("Around 20-25 questions in total.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_2_2 =
            new ScenarioDialogMoveKind("There will be 25 questions.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_3_1 =
            new ScenarioDialogMoveKind("Only the topics I provided throughout the course.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_3_2 =
            new ScenarioDialogMoveKind("You should know all the material that I provided throughout the course.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_4_1 =
            new ScenarioDialogMoveKind("You will not be allowed to use anything.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_4_2 =
            new ScenarioDialogMoveKind("You will not be allowed to use anything.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_5_1 =
            new ScenarioDialogMoveKind("No problem! I am always happy to help.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_5_2 =
            new ScenarioDialogMoveKind("No problem.");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_1_1 =
            new ScenarioDialogMoveKind("The exam is scheduled for October 13, if I remember correctly. But how long will it take?");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_1_2 =
            new ScenarioDialogMoveKind("How much time will we have for the exam?");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_2_1 =
            new ScenarioDialogMoveKind("And how many questions will there actually be in total?");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_2_2 =
            new ScenarioDialogMoveKind("Also, could you tell us how many questions will there be in total?");

        public static readonly DialogMoveKind SubjectDiscussionStudentA_3_1 =
            new ScenarioDialogMoveKind("Oh, it is doable. What topics will be included in the exam?");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_3_2 =
            new ScenarioDialogMoveKind("Oh, that is quite a lot! Could you please tell us what topics will be included in the exam?");

        public static readonly DialogMoveKind SubjectDiscussionStudentA_4_1 =
            new ScenarioDialogMoveKind("Great!");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_4_1 =
            new ScenarioDialogMoveKind("And also will we be allowed to use any additional materials during the exam? Like, lectures notes or the papers you suggested throughout the course?");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_4_2 =
            new ScenarioDialogMoveKind("Ok. But since we do not know which topics will be there, will we be allowed to use any additional materials during the exam? Like, lectures note?");

        public static readonly DialogMoveKind SubjectDiscussionStudentA_5_1 =
            new ScenarioDialogMoveKind("Thank you!");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_5_1 =
            new ScenarioDialogMoveKind("Thank you, Teacher!");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_5_2 =
            new ScenarioDialogMoveKind("Ok, we got it. Thank you anyway!");

        /// <summary>
        /// Indicates the second subject discussion between actors.
        /// </summary>
        public static readonly DialogMoveKind SubjectDiscussionTeacher_6_1 =
            new ScenarioDialogMoveKind("Sure.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_6_2 =
            new ScenarioDialogMoveKind("What else do you want to discuss?");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_7_1 =
            new ScenarioDialogMoveKind("I am afraid both of you are doing poorly this term.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_7_2 =
            new ScenarioDialogMoveKind("You clearly do not understand what I am looking for.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_8_1 =
            new ScenarioDialogMoveKind("I just wish you could have presented it more effectively.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_8_2 =
            new ScenarioDialogMoveKind("Yes, I want to see it clear and efficient.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_9_1 =
            new ScenarioDialogMoveKind("Your reader is coming to this cold.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_9_2 =
            new ScenarioDialogMoveKind("You forgot the most important thing here.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_10_1 =
            new ScenarioDialogMoveKind("Exactly. This is very important for this type of work.");

        public static readonly DialogMoveKind SubjectDiscussionTeacher_10_2 =
            new ScenarioDialogMoveKind("You can rewrite your essays and submit before the next Friday.");

        public static readonly DialogMoveKind SubjectDiscussionStudentA_6_1 =
            new ScenarioDialogMoveKind("Teacher, I also wanted to discuss my essay.");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_6_1 =
            new ScenarioDialogMoveKind("Oh, yeah, me too!");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_6_2 =
            new ScenarioDialogMoveKind("Teacher, we also wondered if you had a few minutes more to discuss our essays.");

        public static readonly DialogMoveKind SubjectDiscussionStudentA_7_1 =
            new ScenarioDialogMoveKind("Thank you. Teacher, you gave me the lowest passing grade on my last essay. That is the second time in a row I receive such a low grade. I guess I just do not understand what it is you are looking for.");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_7_2 =
            new ScenarioDialogMoveKind("Thank you. Teacher, you gave me the lowest passing grade on my last essay. That is the second time in a row I receive such a low grade. I guess I just do not understand what it is you are looking for.");


        public static readonly DialogMoveKind SubjectDiscussionStudentA_8_1 =
            new ScenarioDialogMoveKind("I do not think so! But I thought I had done it right this time. I understand that you want to see a clear, efficient development of the thesis and all that…");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_8_2 =
            new ScenarioDialogMoveKind("We thought we had done it right this time. We understand that you want to see a clear, efficient development of the thesis and all that…");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_9_1 =
            new ScenarioDialogMoveKind("What exactly do you mean?");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_9_2 =
            new ScenarioDialogMoveKind("But what exactly do you mean? We did everything you asked for… At least how we understood the task.");

        public static readonly DialogMoveKind SubjectDiscussionStudentA_10_1 =
            new ScenarioDialogMoveKind("So we should keep in mind that our target reader does not know anything about this topic?");

        public static readonly DialogMoveKind SubjectDiscussionStudentB_10_2 =
            new ScenarioDialogMoveKind("I guess we got it now…");

        /// <summary>
        /// Indicates goodbyes from one actor to one or more others.
        /// </summary>
        public static readonly DialogMoveKind GoodbyeTeacher_1_1 =
            new ScenarioDialogMoveKind("I am glad I could help. You are welcome!");

        public static readonly DialogMoveKind GoodbyeTeacher_1_2 =
            new ScenarioDialogMoveKind("No problem. You are welcome.");

        public static readonly DialogMoveKind GoodbyeTeacher_2 =
            new ScenarioDialogMoveKind("Goodbye.");

        public static readonly DialogMoveKind GoodbyeStudentA_1_1 =
            new ScenarioDialogMoveKind("Great! Thank you for your time, Teacher!");

        public static readonly DialogMoveKind GoodbyeStudentB_1_1 =
            new ScenarioDialogMoveKind("Thank you, Teacher!");

        public static readonly DialogMoveKind GoodbyeStudentB_1_2 =
            new ScenarioDialogMoveKind("Thank you for your time, Teacher! You helped us a lot!");

        public static readonly DialogMoveKind GoodbyeStudentA_2 =
            new ScenarioDialogMoveKind("Goodbye, Teacher! Thank you again!");

        public static readonly DialogMoveKind GoodbyeStudentB_2 =
            new ScenarioDialogMoveKind("Goodbye, Teacher!");

        /// <summary>
        /// Indicates an empty line.
        /// </summary>
        public static readonly DialogMoveKind EmptyLine =
            new ScenarioDialogMoveKind(".");

        /// <summary>
        /// Creates a new dialog move kind.
        /// </summary>
        /// <param name="label">The label associated with this kind.</param>
        /// <remarks>
        /// <paramref name="label"/> must not be blank.
        /// </remarks>
        private ScenarioDialogMoveKind(string label)
            : base(label)
        { }
    }
}
