﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Collections.Extensions;
using Common.Functional.Options;
using Common.Functional.Options.Extensions;
using Common.Randomization.Distributions;
using Common.Time;
using Common.Validation;
using CulturalModel.CulturalProject;
using teacher_student_scenario.Communication;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Dialog_Moves.Extensions;
using Dialog.Agency.Modules.Action;
using Dialog.SPB.State;
using teacher_student_scenario.Agency.Dialog_Moves;

namespace teacher_student_scenario.Agency.Modules.Action
{
    /// <summary>
    /// This class is an implementation of the 
    /// <see cref="ActionRealizationModule"/>.
    /// </summary>
    public sealed class ARModule
        : ActionRealizationModule
    {
        /// <summary>
        /// Creates a new module with the specified output rate.
        /// </summary>
        /// <param name="output_rate">
        /// The time it takes to output a single character of text.
        /// </param>
        /// <remarks>
        /// <paramref name="output_rate"/> must be >= 0.
        /// </remarks>
        public ARModule(float output_rate = 0.05f)
        {
            Require.IsAtLeast(output_rate, 0);

            OutputSpeechRate = output_rate;
        }

        /// <summary>
        /// The time it takes to output a single character.
        /// </summary>
        public float OutputSpeechRate { get; private set; }

        /// <summary>
        /// Gets the actor in whose name this module outputs speech.
        /// </summary>
        public CulturalVirtualHuman Owner { get; private set; }
        /// <summary>
        /// Gets this module's time reference point.
        /// </summary>
        public Clock Clock { get; private set; }

        /// <summary>
        /// Gets the move pending realization (if there is any).
        /// </summary>
        public Option<DialogMove<CulturalVirtualHuman>> PendingMove
        {
            get;
            private set;
        }
            = Option.CreateNone<DialogMove<CulturalVirtualHuman>>();

        /// <summary>
        /// Gets this module's output speech, if it has any.
        /// </summary>
        public Option<Speech> OutputSpeech
        {
            get;
            private set;
        }
            = Option.CreateNone<Speech>();

        /// <summary>
        /// Gets this module's output move, if it has any.
        /// </summary>
        public Option<DialogMove<CulturalVirtualHuman>> OutputMove
        {
            get;
            private set;
        }
            = Option.CreateNone<DialogMove<CulturalVirtualHuman>>();

        /// <summary>
        /// Gets the collection of data types required by this module to be 
        /// available through the information state.
        /// </summary>
        /// <param name="types">The collection of types.</param>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public override
            void GetRequiredStateComponents(ICollection<Type> types)
        {
            types.Add(typeof(SocialContext));
            types.Add(typeof(Clock));
        }
        /// <summary>
        /// Sets up this module for operation.
        /// </summary>
        /// <remarks>
        /// This is called once during module initialization.
        /// </remarks>
        public override void Setup()
        {
            base.Setup();

            Owner = (CulturalVirtualHuman)State.Get<SocialContext>().Self;

            Clock = State.Get<Clock>();
            _action_delay_timer = new Timer(Clock);
            _speech_timer = new Timer(Clock);
        }

        /// <summary>
        /// Realizes the specified dialogue move.
        /// </summary>
        /// <param name="move">The dialogue move to realize.</param>
        /// <returns>
        /// Information regarding the status of the realization.
        /// </returns>
        /// <remarks>
        /// <paramref name="move"/> must not be null.
        /// </remarks>
        public override ActionRealizationStatus RealizeMove(DialogMove move)
        {
            Require.IsNotNull(move);

            OutputSpeech = Option.CreateNone<Speech>();
            OutputMove = Option.CreateNone<DialogMove<CulturalVirtualHuman>>();

            if (move.IsIdle())
            {
                PendingMove = Option.CreateNone<DialogMove<CulturalVirtualHuman>>();

                return ActionRealizationStatus.Complete;
            }

            var target_move = move.Cast<CulturalVirtualHuman>();
            if (!PendingMove.Contains(target_move))
            {
                return BeginRealization(target_move);
            }
            else
            {
                return ResumePendingRealization();
            }
        }

        /// <summary>
        /// Gets the word pending realization (only applicable if there is some
        /// move pending realization).
        /// </summary>
        public Option<string> PendingWord
        {
            get
            {
                if (_pending_words.Count > 0)
                {
                    return Option.CreateSome(_pending_words.Peek());
                }
                else
                {
                    return Option.CreateNone<string>();
                }
            }
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ARModule)}(" +
                   $"{nameof(State)} = {State})";
        }

        /// <summary>
        /// Begin a new move realization.
        /// </summary>
        /// <param name="target_move">The move to realize.</param>
        /// <returns>
        /// Information regarding the status of the realization.
        /// </returns>
        private ActionRealizationStatus
            BeginRealization(DialogMove<CulturalVirtualHuman> target_move)
        {
            _pending_words.Clear();
            _action_delay_timer.Reset();

            var text = ExpressThroughText(target_move);
            var words = text.Split(' ');
            foreach (var word in words)
            {
                _pending_words.Enqueue(word);
            }

            PendingMove = Option.CreateSome(target_move);
            SetRandomActionDelay();

            return ActionRealizationStatus.InProgress;
        }
        /// <summary>
        /// Resumes the currently pending move realization.
        /// </summary>
        /// <returns>
        /// Information regarding the status of the realization.
        /// </returns>
        private ActionRealizationStatus ResumePendingRealization()
        {
            if (_action_delay_timer.Update())
            {
                _action_delay_timer.Reset();

                OutputWord();
            }
            else if (_speech_timer.Update())
            {
                _speech_timer.Reset();

                if (PendingWord.IsSome())
                {
                    OutputWord();
                }
                else
                {
                    OutputMove = PendingMove;
                    PendingMove = Option.CreateNone<DialogMove<CulturalVirtualHuman>>();

                    return ActionRealizationStatus.Complete;
                }
            }
            return ActionRealizationStatus.InProgress;
        }
        /// <summary>
        /// Outputs the currently pending word.
        /// </summary>
        private void OutputWord()
        {
            string word = _pending_words.Dequeue();
            var speech = new Speech(speaker: Owner, text: word);
            OutputSpeech = Option.CreateSome(speech);

            float output_duration = OutputSpeechRate * word.Length;
            _speech_timer.Set(output_duration);
            _speech_timer.Start();
        }

        /// <summary>
        /// Produces a string of speech text representing realization of the 
        /// specified dialogue move.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string ExpressThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            if (move.IsIdle()) return "";

            //Greetings

            if (move.Kind == ScenarioDialogMoveKind.GreetingsTeacher_1)
            {
                return ExpressGreetingsTeacher_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GreetingsTeacher_2)
            {
                return ExpressGreetingsTeacher_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GreetingsStudentB_1)
            {
                return ExpressGreetingsStudentB_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GreetingsStudentB_2)
            {
                return ExpressGreetingsStudentB_2ThroughText(move);
            }

            //Problem Definition

            else if (move.Kind == ScenarioDialogMoveKind.ProblemTeacher_1)
            {
                return ExpressProblemTeacher_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.ProblemTeacher_2)
            {
                return ExpressProblemTeacher_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.ProblemStudentA_1)
            {
                return ExpressProblemStudentA_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.ProblemStudentB_1)
            {
                return ExpressProblemStudentB_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.ProblemStudentB_2)
            {
                return ExpressProblemStudentB_2ThroughText(move);
            }

            //First Subject Discussion

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_1_1)
            {
                return ExpressSubjectDiscussionTeacher_1_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_1_2)
            {
                return ExpressSubjectDiscussionTeacher_1_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_2_1)
            {
                return ExpressSubjectDiscussionTeacher_2_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_2_2)
            {
                return ExpressSubjectDiscussionTeacher_2_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_3_1)
            {
                return ExpressSubjectDiscussionTeacher_3_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_3_2)
            {
                return ExpressSubjectDiscussionTeacher_3_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_4_1)
            {
                return ExpressSubjectDiscussionTeacher_4_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_4_2)
            {
                return ExpressSubjectDiscussionTeacher_4_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_5_1)
            {
                return ExpressSubjectDiscussionTeacher_5_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_5_2)
            {
                return ExpressSubjectDiscussionTeacher_5_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_1_1)
            {
                return ExpressSubjectDiscussionStudentB_1_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_1_2)
            {
                return ExpressSubjectDiscussionStudentB_1_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_2_1)
            {
                return ExpressSubjectDiscussionStudentB_2_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_2_2)
            {
                return ExpressSubjectDiscussionStudentB_2_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentA_3_1)
            {
                return ExpressSubjectDiscussionStudentA_3_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_3_2)
            {
                return ExpressSubjectDiscussionStudentB_3_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentA_4_1)
            {
                return ExpressSubjectDiscussionStudentA_4_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_4_1)
            {
                return ExpressSubjectDiscussionStudentB_4_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_4_2)
            {
                return ExpressSubjectDiscussionStudentB_4_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentA_5_1)
            {
                return ExpressSubjectDiscussionStudentA_5_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_5_1)
            {
                return ExpressSubjectDiscussionStudentB_5_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_5_2)
            {
                return ExpressSubjectDiscussionStudentB_5_2ThroughText(move);
            }

            //Second Subject Discussion
            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_6_1)
            {
                return ExpressSubjectDiscussionTeacher_6_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_6_2)
            {
                return ExpressSubjectDiscussionTeacher_6_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_7_1)
            {
                return ExpressSubjectDiscussionTeacher_7_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_7_2)
            {
                return ExpressSubjectDiscussionTeacher_7_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_8_1)
            {
                return ExpressSubjectDiscussionTeacher_8_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_8_2)
            {
                return ExpressSubjectDiscussionTeacher_8_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_9_1)
            {
                return ExpressSubjectDiscussionTeacher_9_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_9_2)
            {
                return ExpressSubjectDiscussionTeacher_9_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_10_1)
            {
                return ExpressSubjectDiscussionTeacher_10_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionTeacher_10_2)
            {
                return ExpressSubjectDiscussionTeacher_10_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentA_6_1)
            {
                return ExpressSubjectDiscussionStudentA_6_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_6_1)
            {
                return ExpressSubjectDiscussionStudentB_6_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_6_2)
            {
                return ExpressSubjectDiscussionStudentB_6_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentA_7_1)
            {
                return ExpressSubjectDiscussionStudentA_7_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_7_2)
            {
                return ExpressSubjectDiscussionStudentB_7_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentA_8_1)
            {
                return ExpressSubjectDiscussionStudentA_8_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_8_2)
            {
                return ExpressSubjectDiscussionStudentB_8_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_9_1)
            {
                return ExpressSubjectDiscussionStudentB_9_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_9_2)
            {
                return ExpressSubjectDiscussionStudentB_9_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentA_10_1)
            {
                return ExpressSubjectDiscussionStudentA_10_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.SubjectDiscussionStudentB_10_2)
            {
                return ExpressSubjectDiscussionStudentB_10_2ThroughText(move);
            }

            //Goodbyes
            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeTeacher_1_1)
            {
                return ExpressGoodbyeTeacher_1_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeTeacher_1_2)
            {
                return ExpressGoodbyeTeacher_1_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeTeacher_2)
            {
                return ExpressGoodbyeTeacher_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeStudentA_1_1)
            {
                return ExpressGoodbyeStudentA_1_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeStudentB_1_1)
            {
                return ExpressGoodbyeStudentB_1_1ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeStudentB_1_2)
            {
                return ExpressGoodbyeStudentB_1_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeStudentA_2)
            {
                return ExpressGoodbyeStudentA_2ThroughText(move);
            }

            else if (move.Kind == ScenarioDialogMoveKind.GoodbyeStudentB_2)
            {
                return ExpressGoodbyeStudentB_2ThroughText(move);
            }

            //Other

            else if (move.Kind == ScenarioDialogMoveKind.EmptyLine)
            {
                return ExpressEmptyLineThroughText(move);
            }
            else
            {
                throw new ArgumentException
                (
                    "Move is of an unknown kind and cannot be expressed " +
                    "through text."
                );
            }
        }

        /// <summary>
        /// Produces a string of speech text representing realization of a
        /// greetings.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressGreetingsTeacher_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Good morning! Sure, how can I help you?";
        }

        private string
            ExpressGreetingsTeacher_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Hello. Yes, I have some time. What is the matter?";
        }

        private string
            ExpressGreetingsStudentB_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Good morning, {move.Target.GetValue().Name}. Can we " +
                   $"take a little of your time?";
        }

        private string
            ExpressGreetingsStudentB_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Hello, {move.Target.GetValue().Name}. Could you spare " +
                   $"some time for us?";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of an
        /// problem definition between actors.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressProblemTeacher_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Yes, I remember you. What exactly would you like to know?";
        }

        private string
            ExpressProblemTeacher_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Ok. What exactly you wanted to know?";
        }

        private string
            ExpressProblemStudentA_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Hi! I am {move.Target.GetValue().Name}.";
        }

        private string
            ExpressProblemStudentB_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"I am {move.Target.GetValue().Name}. We wanted to ask " +
                   $"you several questions about the upcoming exam.";
        }

        private string
            ExpressProblemStudentB_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Thank you! I am {move.Target.GetValue().Name} and " +
                   $"this is Mark. We would like to know something about " +
                   $"the upcoming exam.";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of the
        /// first subject discussion between actors.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressSubjectDiscussionTeacher_1_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Yes, you are absolutely right! October 13. The exam " +
                   "will take exactly two hours.";
        }

        private string
            ExpressSubjectDiscussionTeacher_1_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "As usual, the exam will take exactly two hours.";
        }

        private string
            ExpressSubjectDiscussionTeacher_2_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I still do not know the exact number yet… But I think " +
                   "there will be around 20-25 questions in total.";
        }

        private string
            ExpressSubjectDiscussionTeacher_2_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "There will be 25 questions.";
        }

        private string
            ExpressSubjectDiscussionTeacher_3_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Basically, only the topics I provided throughout the " +
                   "course.";
        }

        private string
            ExpressSubjectDiscussionTeacher_3_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I cannot answer you this question. You should know all " +
                   "the material that I provided throughout the course.";
        }

        private string
            ExpressSubjectDiscussionTeacher_4_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "No, actually you will not be allowed to use anything. " +
                   "You know, it is not an open book exam. So just make " +
                   "sure you will not forget to bring with you a couple of " +
                   "pens. Draft sheets will be provided.";
        }

        private string
            ExpressSubjectDiscussionTeacher_4_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Of course not. It is not an open book exam. Knowledge " +
                   "should be in your head, not on the desk.";
        }

        private string
            ExpressSubjectDiscussionTeacher_5_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "No problem! I am always happy to help.";
        }

        private string
            ExpressSubjectDiscussionTeacher_5_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "No problem.";
        }

        private string
            ExpressSubjectDiscussionStudentB_1_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "The exam is scheduled for October 13, if I remember " +
                   "correctly. But how long will it take?";
        }

        private string
            ExpressSubjectDiscussionStudentB_1_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "How much time will we have for the exam?";
        }

        private string
            ExpressSubjectDiscussionStudentB_2_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "And how many questions will there actually be in total?";
        }

        private string
            ExpressSubjectDiscussionStudentB_2_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Also, could you tell us how many questions will there " +
                   "be in total?";
        }

        private string
            ExpressSubjectDiscussionStudentA_3_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Oh, it is doable. And What topics will be included in " +
                   "the exam?";
        }

        private string
            ExpressSubjectDiscussionStudentB_3_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Oh, that is quite a lot! Could you please tell us what " +
                   "topics will be included in the exam?";
        }

        private string
            ExpressSubjectDiscussionStudentA_4_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Cool!";
        }

        private string
            ExpressSubjectDiscussionStudentB_4_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "And also will we be allowed to use any additional " +
                   "materials during the exam? Like, lectures notes or the " +
                   "papers you suggested throughout the course?";
        }

        private string
            ExpressSubjectDiscussionStudentB_4_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "But since we do not know which topics will be there, " +
                   "will we be allowed to use any additional materials " +
                   "during the exam? Like, lectures note?";
        }

        private string
            ExpressSubjectDiscussionStudentA_5_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Thank you!";
        }

        private string
            ExpressSubjectDiscussionStudentB_5_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Ok, we got it. Thank you, {move.Target.GetValue().Name}!";
        }

        private string
            ExpressSubjectDiscussionStudentB_5_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Ok, we got it. Thank you anyway!";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of the
        /// second subject discussion between actors.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressSubjectDiscussionTeacher_6_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Sure.";
        }

        private string
            ExpressSubjectDiscussionTeacher_6_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Ok. What do you want to discuss?";
        }

        private string
            ExpressSubjectDiscussionTeacher_7_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Well, you are not alone, " +
                   $"{move.Target.GetValue().Name}. I am afraid Anna is " +
                   $"doing poorly this term too. Now, I have tried to " +
                   $"explain what I want you all to do several times now, " +
                   $"but nobody seems to get it. I guess I am a failure as " +
                   $"a teacher.";
        }

        private string
            ExpressSubjectDiscussionTeacher_7_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "You guess it right. You clearly do not understand " +
                   "what I am looking for. And you are not alone. Most of " +
                   "the class is doing poorly this term, including you " +
                   "Mark. I explained it to all of you several times, " +
                   "but it turned out to be pointless.";
        }

        private string
            ExpressSubjectDiscussionTeacher_8_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Actually, {move.Target.GetValue().Name}, you have a " +
                   $"good essay. I like it. I just wish you could have " +
                   $"presented it more effectively and more clear for the " +
                   $"reader. The same holds for you, Anna.";
        }

        private string
            ExpressSubjectDiscussionTeacher_8_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Yes, I want to see it clear and efficient. And most " +
                   "of you failed on that matter.";
        }

        private string
            ExpressSubjectDiscussionTeacher_9_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Your essays seem short and direct. But you must " +
                   "remember that when you wrote this, you had been reading " +
                   "and researching your topic beforehand - I hope - but " +
                   "the reader is coming to this cold.";
        }

        private string
            ExpressSubjectDiscussionTeacher_9_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "You forgot the most important thing here. Your reader " +
                   "does not know anything about this topic. You should " +
                   "always keep in mind that the reader is coming to this " +
                   "cold.";
        }

        private string
            ExpressSubjectDiscussionTeacher_10_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Exactly. This is very important for this type of work. " +
                   "I really like both of your essays, so if you want to " +
                   "raise your grade, you can rewrite it and submit before " +
                   "the next Friday.";
        }

        private string
            ExpressSubjectDiscussionTeacher_10_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I hope so. If you want to try to raise your grade, " +
                   "you can rewrite your essays and submit before the " +
                   "next Friday.";
        }

        private string
            ExpressSubjectDiscussionStudentA_6_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"{move.Target.GetValue().Name}, I also wanted to " +
                   $"discuss my essay.";
        }

        private string
            ExpressSubjectDiscussionStudentB_6_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Oh, yeah, me too!";
        }

        private string
            ExpressSubjectDiscussionStudentB_6_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"{move.Target.GetValue().Name}, we also wondered if " +
                   $"you have a few minutes more to discuss our essays.";
        }

        private string
            ExpressSubjectDiscussionStudentA_7_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Thank you. {move.Target.GetValue().Name}, you gave me " +
                   $"the lowest passing grade on my last essay. That is the " +
                   $"second time in a row I receive such a low grade. I " +
                   $"guess I just do not understand what it is you are " +
                   $"looking for.";
        }

        private string
            ExpressSubjectDiscussionStudentB_7_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Thank you! {move.Target.GetValue().Name}, you gave us " +
                   $"the lowest passing grade on our last essays. That is " +
                   $"the second time in a row we receive such a low grade. " +
                   $"I guess we just do not understand what it is you are " +
                   $"looking for.";
        }

        private string
            ExpressSubjectDiscussionStudentA_8_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I do not think so! But I thought I had done it right " +
                   "this time. I understand that you want to see a clear, " +
                   "efficient development of the thesis and all that…";
        }

        private string
            ExpressSubjectDiscussionStudentB_8_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "We thought we had done it right this time. We " +
                   "understand that you want to see a clear, efficient " +
                   "development of the thesis and all that…";
        }

        private string
            ExpressSubjectDiscussionStudentB_9_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "What exactly do you mean?";
        }

        private string
            ExpressSubjectDiscussionStudentB_9_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "But what exactly do you mean? We did everything you " +
                   "asked for… At least how we understood the task.";
        }

        private string
            ExpressSubjectDiscussionStudentA_10_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "So we should keep in mind that our target reader does " +
                   "not know anything about this topic?";
        }

        private string
            ExpressSubjectDiscussionStudentB_10_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I guess we got it now…";
        }

        /// <summary>
        /// Produces a string of speech text representing realization of
        /// goodbyes.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressGoodbyeTeacher_1_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "I am glad I could help. You are welcome!";
        }

        private string
            ExpressGoodbyeTeacher_1_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "No problem. You are welcome.";
        }

        private string
            ExpressGoodbyeTeacher_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {

            if (move.Targets.IsEmpty())
            {
                return "Goodbye.";
            }
            else
            {
                var names = move.TargetEnumeration.Select(actor => actor.Name).ToArray();
                return $"Goodbye {string.Join(", ", names)}.";
            }
        }

        private string
            ExpressGoodbyeStudentA_1_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Great! Thank you for your time, " +
                   $"{move.Target.GetValue().Name}!";
        }

        private string
            ExpressGoodbyeStudentB_1_1ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return "Thank you!";
        }

        private string
            ExpressGoodbyeStudentB_1_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Great! Thank you for your time, " +
                   $"{move.Target.GetValue().Name}. You helped us a lot!";
        }

        private string
            ExpressGoodbyeStudentA_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            Require.IsTrue(move.Target.IsSome());

            return $"Goodbye {move.Target.GetValue().Name}! Thank you again!";
        }

        private string
            ExpressGoodbyeStudentB_2ThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            if (move.Targets.IsEmpty())
            {
                return "Goodbye.";
            }
            else
            {
                var names = move.TargetEnumeration.Select(actor => actor.Name).ToArray();
                return $"Goodbye {string.Join(", ", names)}.";
            }
        }

        /// <summary>
        /// Produces an empty line of speech.
        /// </summary>
        /// <param name="move">The move to express in text.</param>
        /// <returns>A string.</returns>
        private string
            ExpressEmptyLineThroughText(DialogMove<CulturalVirtualHuman> move)
        {
            return ".";
        }

        /// <summary>
        /// Sets the action delay timer to a random value in order to mimic
        /// human reaction time.
        /// </summary>
        private void SetRandomActionDelay()
        {
            float duration = (float)UniformDistribution.Sample(1.0, 1.5);
            _action_delay_timer.Set(duration);
            _action_delay_timer.Start();
        }

        private readonly Queue<string> _pending_words = new Queue<string>();

        private Timer _action_delay_timer;
        private Timer _speech_timer;
    }
}
