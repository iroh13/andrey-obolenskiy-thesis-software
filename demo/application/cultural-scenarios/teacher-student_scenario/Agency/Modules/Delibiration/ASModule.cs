﻿using Common.Validation;
using Dialog.Agency.Dialog_Moves;
using Dialog.Agency.Modules.Deliberation;

namespace teacher_student_scenario.Agency.Modules.Delibiration
{
    /// <summary>
    /// This class is an implementation of the 
    /// <see cref="ActionSelectionModule"/>, where the target move is 
    /// externally set.
    /// </summary>
    public sealed class ASModule
        : ActionSelectionModule
    {
        /// <summary>
        /// Gets/sets the dialogue move to select.
        /// </summary>
        /// <remarks>
        /// <see cref="TargetMove"/> must not be set to null.
        /// </remarks>
        public DialogMove TargetMove { get; set; } = IdleMove.Instance;

        /// <summary>
        /// Selects a target dialogue move for the system to perform. This 
        /// selects the move specified by <see cref="TargetMove"/>.
        /// </summary>
        /// <returns>The move specified by <see cref="TargetMove"/>.</returns>
        /// <remarks>
        /// <see cref="TargetMove"/> must not be null.
        /// </remarks>
        public override DialogMove SelectMove()
        {
            Require.IsNotNull(TargetMove);

            return TargetMove;
        }

        /// <summary>
        /// Returns a string that represents this instance.
        /// </summary>
        /// <returns>
        /// A human-readable string.
        /// </returns>
        public override string ToString()
        {
            return $"{nameof(ASModule)}(" +
                   $"{nameof(State)} = {State})";
        }
    }
}
