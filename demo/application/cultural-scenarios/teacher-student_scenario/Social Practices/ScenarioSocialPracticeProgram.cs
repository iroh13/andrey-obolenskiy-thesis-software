﻿using Common.Validation;
using Dialog.SPB.Program;
using CulturalModel.CulturalProject;
using dialog_culture.Cultural_Norms.Status_Norms;
using teacher_student_scenario.Agency.Scene;
using static Dialog.SPB.Program.NLI.ProgramBuildUtilities;
using static dialog_culture.Program.Natural_Language_Interface.ProgramBuildUtilitiesCultural;
using static teacher_student_scenario.Agency.Dialog_Moves.ScenarioDialogMoveKind;

namespace teacher_student_scenario.Social_Practices
{
    /// <summary>
    /// This class is responsible for instantiating social practice programs
    /// for the scenario.
    /// </summary>
    public static class ScenarioSocialPracticeProgram
    {
        /// <summary>
        /// Creates a new program for the doctor-patient scenario.
        /// </summary>
        /// <param name="session">
        /// The session for which to make the program.
        /// </param>
        /// <returns>
        /// A new social practice program.
        /// </returns>
        /// <remarks>
        /// <paramref name="session"/> must not be null.
        /// </remarks>
        public static Node Create(Session session)
        {
            Require.IsNotNull(session);

            CulturalVirtualHuman teacher = session.Therapist;
            CulturalVirtualHuman studentA = session.PatientA;
            CulturalVirtualHuman studentB = session.PatientB;

            return

                // ----- BEGIN PRACTICE DESCRIPTION ------ //

                ExpectSkippingSequence
                (
                    "Teacher-Students Session",

                    //Greetings

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectEvent(GreetingsStudentB_2, source: studentB, target: teacher),
                        ExpectEvent(GreetingsStudentB_1, source: studentB, target: teacher)
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentA),
                        ExpectEvent(GreetingsTeacher_2, source: teacher),
                        ExpectEvent(GreetingsTeacher_1, source: teacher)
                    ),

                    //Problem Definition

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentA, target: teacher),
                        ExpectCultural
                        (
                            new OutGroupNorm(source: studentA, target: studentB),
                            ExpectEvent(ProblemStudentA_1, source: studentA)
                        ),
                        ExpectEvent(ProblemStudentA_1, source: studentA, target: studentA)
                    ),
                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(ProblemStudentB_2, source: studentB, target: studentB)
                        ),
                        ExpectEvent(ProblemStudentB_1, source: studentB, target: studentB)
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentA),
                        ExpectEvent(ProblemTeacher_2, source: teacher),
                        ExpectEvent(ProblemTeacher_1, source: teacher)
                    ),

                    //First Subject Discussion

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(SubjectDiscussionStudentB_1_2, source: studentB)
                        ),
                        ExpectEvent(SubjectDiscussionStudentB_1_1, source: studentB)
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_1_2, source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_1_1, source: teacher, target: studentB)
                    ),

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(SubjectDiscussionStudentB_2_2, source: studentB)
                        ),
                        ExpectEvent(SubjectDiscussionStudentB_2_1, source: studentB)
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_2_2, source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_2_1, source: teacher, target: studentB)
                    ),

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentA, target: teacher),
                        ExpectCultural
                        (
                            new OutGroupNorm(source: studentA, target: studentB),
                            ExpectEvent(SubjectDiscussionStudentA_3_1, source: studentA)
                        ),
                        ExpectEvent(SubjectDiscussionStudentA_3_1, source: studentA)
                    ),
                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(SubjectDiscussionStudentB_3_2, source: studentB)
                        )
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_3_2, source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_3_1, source: teacher, target: studentA)
                    ),

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentA, target: teacher),
                        ExpectCultural
                        (
                            new OutGroupNorm(source: studentA, target: studentB),
                            ExpectEvent(SubjectDiscussionStudentA_4_1, source: studentA)
                        ),
                        ExpectEvent(SubjectDiscussionStudentA_4_1, source: studentA)
                    ),
                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(SubjectDiscussionStudentB_4_2, source: studentB)
                        ),
                        ExpectEvent(SubjectDiscussionStudentB_4_1, source: studentB)
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_4_2, source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_4_1, source: teacher, target: studentB)
                    ),

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(SubjectDiscussionStudentB_5_2, source: studentB)
                        ),
                        ExpectEvent(SubjectDiscussionStudentB_5_1, source: studentB, target: teacher)
                    ),
                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentA, target: teacher),
                        ExpectCultural
                        (
                            new OutGroupNorm(source: studentA, target: studentB),
                            ExpectEvent(SubjectDiscussionStudentA_5_1, source: studentA)
                        ),
                        ExpectEvent(SubjectDiscussionStudentA_5_1, source: studentA)
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_5_2, source: teacher),
                        ExpectEvent(SubjectDiscussionTeacher_5_1, source: teacher)
                    ),

                    //Second Subject Discussion

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentA, target: teacher),
                        ExpectCultural
                        (
                            new OutGroupNorm(source: studentA, target: studentB),
                            ExpectEvent(SubjectDiscussionStudentA_6_1, source: studentA)
                        ),
                        ExpectEvent(SubjectDiscussionStudentA_6_1, source: studentA, target: teacher)
                    ),
                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(SubjectDiscussionStudentB_6_2, source: studentB, target: teacher)
                        ),
                        ExpectEvent(SubjectDiscussionStudentB_6_1, source: studentB)
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_6_2, source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_6_1, source: teacher, target: studentA)
                    ),

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentA, target: teacher),
                        ExpectCultural
                        (
                            new OutGroupNorm(source: studentA, target: studentB),
                            ExpectEvent(SubjectDiscussionStudentA_7_1, source: studentA)
                        ),
                        ExpectEvent(SubjectDiscussionStudentA_7_1, source: studentA, target: teacher)
                    ),
                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(SubjectDiscussionStudentB_7_2, source: studentB, target: teacher)
                        )
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_7_2, source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_7_1, source: teacher, target: studentA)
                    ),

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentA, target: teacher),
                        ExpectCultural
                        (
                            new OutGroupNorm(source: studentA, target: studentB),
                            ExpectEvent(SubjectDiscussionStudentA_8_1, source: studentA)
                        ),
                        ExpectEvent(SubjectDiscussionStudentA_8_1, source: studentA)
                    ),
                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(SubjectDiscussionStudentB_8_2, source: studentB)
                        )
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_8_2, source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_8_1, source: teacher, target: studentA)
                    ),

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(SubjectDiscussionStudentB_9_2, source: studentB)
                        ),
                        ExpectEvent(SubjectDiscussionStudentB_9_1, source: studentB)
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_9_2, source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_9_1, source: teacher, target: studentB)
                    ),

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentA, target: teacher),
                        ExpectCultural
                        (
                            new OutGroupNorm(source: studentA, target: studentB),
                            ExpectEvent(SubjectDiscussionStudentA_10_1, source: studentA)
                        ),
                        ExpectEvent(SubjectDiscussionStudentA_10_1, source: studentA)
                    ),
                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(SubjectDiscussionStudentB_10_2, source: studentB)
                        )
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_10_2, source: teacher, target: studentB),
                        ExpectEvent(SubjectDiscussionTeacher_10_1, source: teacher, target: studentA)
                    ),

                    //Goodbyes

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentA, target: teacher),
                        ExpectCultural
                        (
                            new OutGroupNorm(source: studentA, target: studentB),
                            ExpectEvent(GoodbyeStudentA_1_1, source: studentA, target: teacher)
                        ),
                        ExpectEvent(GoodbyeStudentA_1_1, source: studentA, target: teacher)
                    ),
                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(GoodbyeStudentB_1_2, source: studentB, target: teacher)
                        ),
                        ExpectEvent(GoodbyeStudentB_1_1, source: studentB)
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(GoodbyeTeacher_1_2, source: teacher, target: studentB),
                        ExpectEvent(GoodbyeTeacher_1_1, source: teacher)
                    ),

                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentA, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentA, target: studentB),
                            ExpectEvent(GoodbyeStudentB_2, source: studentA, target: teacher)
                        ),
                        ExpectEvent(GoodbyeStudentB_2, source: studentA, target: teacher)
                    ),
                    ExpectCultural
                    (
                        new StatusLowToHighNorm(source: studentB, target: teacher),
                        ExpectCultural
                        (
                            new InGroupNorm(source: studentB, target: studentA),
                            ExpectEvent(GoodbyeStudentA_2, source: studentB, target: teacher)
                        ),
                        ExpectEvent(GoodbyeStudentB_2, source: studentB, target: teacher)
                    ),
                    ExpectCultural
                    (
                        new StatusHighToLowNorm(source: teacher, target: studentB),
                        ExpectEvent(GoodbyeTeacher_2, source: teacher),
                        ExpectAll
                        (
                            "Goodbyes",
                            ExpectEvent(GoodbyeTeacher_2, source: teacher, target: studentA),
                            ExpectEvent(GoodbyeTeacher_2, source: teacher, target: studentB)
                        )       
                    )
                );
        }
    }
}
