﻿using teacher_student_scenario.Communication;
using Dialog.Agency.Modules.Perception;
using Dialog.Communication.Management;

namespace teacher_student_scenario.Communication
{
    /// <summary>
    /// This class is responsible for instantiating communication manager for
    /// the scenario.
    /// </summary>
    public static class ScenarioCommunicationManager
    {
        /// <summary>
        /// Creates a new communication manager for the doctor-patient
        /// scenario.
        /// </summary>
        /// <returns>
        /// A new communication manager supporting two channels: one for 
        /// realized dialogue move events, and the other for string data.
        /// </returns>
        public static CommunicationManager Create()
        {
            return new CommunicationManager.Builder()
                .Support<DialogMoveRealizationEvent>()
                .Support<Speech>()
                .Build();
        }
    }
}
