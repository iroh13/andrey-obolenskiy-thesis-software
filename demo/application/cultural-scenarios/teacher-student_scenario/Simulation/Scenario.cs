﻿using Common.Collections;
using Common.Time;
using Common.Validation;
using teacher_student_scenario.Agency.Scene;
using teacher_student_scenario.Simulation.Actor_Controllers;
using Dialog.Communication.Management;
using Dialog.SPB.State;
using teacher_student_scenario.Communication;

namespace teacher_student_scenario.Simulation
{
    /// <summary>
    /// This class represents a couples therapy simulation scenario, with the
    /// patient couple being driven by AI, and the therapist being controlled
    /// by a human player.
    /// </summary>
    public sealed class Scenario
    {
        /// <summary>
        /// Creates a new scenario for the specified session.
        /// </summary>
        /// <param name="session">The session parameters.</param>
        /// <remarks>
        /// <paramref name="session"/> must not be null.
        /// </remarks>
        public Scenario(Session session)
        {
            Require.IsNotNull(session);

            Session = session;
            _clock = new VariableIncrementClock();

            CommunicationManager = ScenarioCommunicationManager.Create();

            TherapistController =
                new TherapistController(Session.Therapist, this);
            _patientA_controller = new PatientController(Session.PatientA, this);
            _patientB_controller = new PatientController(Session.PatientB, this);
        }

        /// <summary>
        /// Gets the simulated session details.
        /// </summary>
        public Session Session { get; private set; }
        /// <summary>
        /// Gets the scenario's time reference.
        /// </summary>
        public Clock Clock { get { return _clock; } }
        /// <summary>
        /// Gets the scenario's communication manager.
        /// </summary>
        public CommunicationManager CommunicationManager { get; private set; }

        /// <summary>
        /// Gets the therapist actor controller.
        /// </summary>
        public TherapistController TherapistController { get; private set; }

        /// <summary>
        /// Indicates whether the scenario has come to an end.
        /// </summary>
        /// <returns>True if the scenario has ended, otherwise false.</returns>
        public bool HasEnded()
        {
            return
                TherapistController.AgencySystem
                    .State.Get<SocialContext>()
                    .Program.IsResolved;
        }
        /// <summary>
        /// Advances the simulation in time.
        /// </summary>
        /// <param name="dt">The time duration to advance by.</param>
        /// <remarks>
        /// <paramref name="dt"/> must be > 0.
        /// </remarks>
        public void Step(float dt)
        {
            Require.IsGreaterThan(dt, 0);

            _clock.Tick(dt);
            CommunicationManager.Update();
        }

        private PatientController _patientA_controller;
        private PatientController _patientB_controller;
        private VariableIncrementClock
            _clock = new VariableIncrementClock();
    }
}
